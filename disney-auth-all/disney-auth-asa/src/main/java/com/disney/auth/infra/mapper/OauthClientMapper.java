package com.disney.auth.infra.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.disney.auth.infra.entity.OauthClient;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * oauth客户端表 持久
 *
 * @author issavior
 */
@Repository
public interface OauthClientMapper extends BaseMapper<OauthClient> {

    List<OauthClient> page(OauthClient oauthclient);

}

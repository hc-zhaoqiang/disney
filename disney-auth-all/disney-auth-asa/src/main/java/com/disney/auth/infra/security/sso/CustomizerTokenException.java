package com.disney.auth.infra.security.sso;

import org.springframework.security.authentication.AuthenticationServiceException;

/**
 * @author issavior
 */
public class CustomizerTokenException extends AuthenticationServiceException {
    public CustomizerTokenException(String message, Throwable cause) {
        super(message, cause);
    }

    public CustomizerTokenException(String message) {
        super(message);
    }
}

package com.disney.auth.app.service;


import com.github.pagehelper.PageInfo;

/**
 * 用户表
 *
 * @author issavior
 */
public interface UserService {


    /**
     * 登录查询使用
     *
     * @param username 手机号/邮箱
     */
    LifeUser selectByUsername(String username);

    /**
     * 锁定用户几小时
     *
     * @param userId 用户ID
     * @param lockTime 锁定时间-单位：小时；
     */
    void lock(Long userId , Integer lockTime);

    PageInfo<LifeUserVO> page(LifeUserDTO lifeUser);

}

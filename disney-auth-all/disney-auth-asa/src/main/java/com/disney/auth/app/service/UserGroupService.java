package com.disney.auth.app.service;

import java.util.List;

/**
 * 用户组表
 *
 * @author issavior
 */
public interface UserGroupService {

    /**
     * 查找该用户所属的用户组
     */
    List<UserGroup> selectByUserId(Long userId);

}

package com.disney.auth.api.remote;

import org.springframework.stereotype.Service;

/**
 *
 * @author issavior
 */
@Service
public class AuthServerRemoteServiceImpl implements AuthServerRemoteService {

    /**
     * 根据jwtToken获取当前登录用户信息
     *
     * @param accessToken 前端传入的accessToken,
     */
    @Override
    public AuthServerApiRes<UserDetailVO> getUserByToken(String accessToken) {

        return null;
    }
}

package com.disney.auth.infra.security.sso;

import org.springframework.security.authentication.AccountStatusException;

/**
 * @author issavior
 */
public class ValidVerificationCodeException extends AccountStatusException {

    public ValidVerificationCodeException(String msg) {
        super(msg);
    }
    public ValidVerificationCodeException(String msg, Throwable cause) {
        super(msg, cause);
    }

}

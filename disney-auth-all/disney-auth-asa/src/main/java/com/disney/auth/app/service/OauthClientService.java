package com.disney.auth.app.service;


/**
 * oauth客户端表
 *
 * @author issavior
 */
public interface OauthClientService {


    OauthClientVO selectClientByClientId(String id);

    OauthClientVO clientByDomain(String domainName, String grantType);

}

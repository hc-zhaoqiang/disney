package com.disney.auth.app.service;

/**
 * dubbo接口公共集成接口
 *
 * @author issavior
 */
public interface RemoteCommonService {

    String getSystemRemoteNowDate();

}
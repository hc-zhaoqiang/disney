package com.disney.auth.test;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * @author issavior
 */
@RestController
@RequestMapping("/v1/test")
public class OauthServerTestCtrl {

    @Operation(summary = "获取当前时间")
    @PostMapping("/now-date")
    public Result<String> nowDate() {
        return Result.ok(JsonDateUtil.formatLocalDateTime(LocalDateTime.now()));
    }

}

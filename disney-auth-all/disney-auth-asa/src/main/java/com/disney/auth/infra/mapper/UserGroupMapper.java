package com.disney.auth.infra.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.disney.auth.infra.entity.UserGroup;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户组表 持久
 *
 * @author issavior
 */
@Repository
public interface UserGroupMapper extends BaseMapper<UserGroup> {

    List<UserGroup> page(UserGroup usergroup);

}

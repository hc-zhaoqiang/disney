package com.disney.auth.infra.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.disney.auth.infra.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户表 持久
 *
 * @author issavior
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    List<User> page(User lifeUser);

}

package com.disney.auth.app.service.impl;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户组表
 *
 * @author issavior
 */
@Service
public class UserGroupServiceImpl implements UserGroupService {

    @Autowired
    private UserGroupMapper mapper;


    /**
     * 查找该用户所属的用户组
     */
    @Override
    public List<UserGroup> selectByUserId(Long userId) {
        return mapper.selectList(Wrappers.lambdaQuery(UserGroup.class).eq(UserGroup::getUserId, userId));
    }
}

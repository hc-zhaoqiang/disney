package com.resort.order;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.order.api.OrderDetailService;
import com.resort.order.api.response.OrderDetailDTO;
import com.resort.order.convert.OrderConvert;
import com.resort.order.domain.gateway.OrderGateway;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@Service
public class OrderDetailServiceImpl implements OrderDetailService {

    @Resource
    private OrderGateway orderGateway;

    @Resource
    private OrderConvert orderConvert;

    @Override
    public DisneyResult<List<OrderDetailDTO>> orderDetails() {
        return orderConvert.toOrderDetailDTO(orderGateway.queryOrderDetails());
    }
}

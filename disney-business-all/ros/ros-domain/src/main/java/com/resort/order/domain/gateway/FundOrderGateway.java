package com.resort.order.domain.gateway;

import com.resort.order.domain.model.FundOrder;

/**
 * @author issavior
 */
public interface FundOrderGateway {
    FundOrder findByOrderId(Long orderId);

    void insert(FundOrder fundOrder);

    void updateStatuses(FundOrder fundOrder);
}

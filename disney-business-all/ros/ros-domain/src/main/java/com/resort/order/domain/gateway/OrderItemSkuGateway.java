package com.resort.order.domain.gateway;

import com.resort.order.domain.model.OrderItemSku;

public interface OrderItemSkuGateway {
    void createOrderItemSku(OrderItemSku orderItemSku);
}

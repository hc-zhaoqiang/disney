package com.resort.order.domain.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * 交易币种
 * @author issavior
 */
@Getter
public enum CurrencyType {

    Order_init(1, "订单初始化"),
    Order_Trading(2, "订单交易中"),
    Order_trade_success(3, "订单交易成功"),
    Order_trade_close(4, "订单交易关闭"),
    Order_trade_cancel(5, "订单交易取消"),

    UNKNOWN(-1,"未知")
    ;

    private final Integer code;
    private final String desc;

    CurrencyType(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static CurrencyType of(Integer code) {
        return Arrays.stream(CurrencyType.values())
                .filter(e -> Objects.equals(e.getCode(), code))
                .findFirst()
                .orElse(UNKNOWN);
    }
}

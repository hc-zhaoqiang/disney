package com.resort.order.domain.customer.gateway;

import com.resort.order.domain.customer.Customer;

public interface CustomerGateway {
    Customer getByById(String customerId);
}

package com.resort.order.domain.even.domain;

import com.disney.domain.sdk.even.DomainEven;
import com.resort.trade.domain.model.Order;
import lombok.Builder;
import lombok.Data;

/**
 * @author issavior
 */
@Data
@Builder
public class CreateOrderEven implements DomainEven {
    private Order order;

}

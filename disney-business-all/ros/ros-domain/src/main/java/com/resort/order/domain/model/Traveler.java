package com.resort.order.domain.model;

import lombok.Data;

import java.util.Date;

/**
 * @author 纵影
 * @date 2023/7/16 1:22 上午
 * @description
 */
@Data
public class Traveler {

    /**
     * ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 用户ID
     */
    private String userId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 姓
     */
    private String surnName;

    /**
     * 名
     */
    private String givenName;

    /**
     * 国际编码
     */
    private String mobilePreFix;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 证件类型
     */
    private Integer docType;

    /**
     * 证件id
     */
    private String docId;
    /**
     * 人群类型（1-Senior老人、2-Adult成人、3-Child儿童）
     */
    private Integer personCode;

    /**
     * 人群类型（1-Old老人、2-Adult成人、3-Child儿童）
     */
    private String personType;
    /**
     * 人群类型（1-Old老人、2-Adult成人、3-Child儿童）
     */
    private String personTypeName;

    /**
     * 乐观锁version
     */
    private Integer version;
    /**
     * 有效 1，无效-1
     */
    private Integer active;

}

package com.resort.order.domain.even.domain.listener;

import com.disney.domain.sdk.anno.DomainEvenListener;
import com.resort.trade.domain.even.domain.CancelOrderEven;
import com.resort.trade.domain.model.Order;
import com.resort.trade.domain.status.OrderStatus;
import org.springframework.context.event.EventListener;

/**
 * @author issavior
 */
@DomainEvenListener
public class CancelOrderEvenListener {

    @EventListener
    public void eventListener(CancelOrderEven even) {
        Order order = even.getOrder();
        order.setOrderStatus(OrderStatus.ORDER_TRADE_CANCEL);
    }
}

package com.resort.order.domain.customer.gateway;

import com.resort.order.domain.customer.Credit;

//Assume that the credit info is in another distributed Service
public interface CreditGateway {
    Credit getCredit(String customerId);
}

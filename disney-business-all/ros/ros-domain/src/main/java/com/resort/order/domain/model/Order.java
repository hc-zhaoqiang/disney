package com.resort.order.domain.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.disney.domain.sdk.common.Lazy;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.trade.domain.enums.CurrencyType;
import com.resort.trade.domain.status.OrderStatus;
import com.resort.trade.domain.status.PayStatus;
import com.resort.trade.domain.status.PerformanceStatus;
import com.resort.trade.domain.status.RefundStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * 订单
 *
 * @author issavior
 */
@Slf4j
@Getter
@Setter
public class Order {

    /**
     * 订单ID
     */
    private Long id;


    /**
     * 业务编码
     */
    private DomainBizType bizCodeEnum;

    /**
     * 订单状态
     */
    private OrderStatus orderStatus;

    /**
     * 支付状态
     */
    private PayStatus payStatus;

    /**
     * 支付币种
     */
    private CurrencyType currencyType;

    /**
     * 支付时间
     */
    private Date payTime;

    /**
     * 关单原因
     */
    private String closeReason;

    /**
     * 关单时间
     */
    private Date closeTime;

    /**
     * 退款状态
     */
    private RefundStatus refundStatus;

    /**
     * 退款金额
     */
    private Long refundFee;

    /**
     * 退款金额币种
     */
    private CurrencyType refundCurrencyType;

    /**
     * 交付状态
     */
    private PerformanceStatus performanceStatus;
    /**
     * 订单总金额
     */
    private Long totalFee;

    /**
     * 支付金额
     */
    private Long payFee;

    /**
     * 折扣金额
     */
    private Long discountFee;

    /**
     * 买家信息
     */
//    private Buyer buyer;

    /**
     * 联系人
     */
//    private Contact contact;
    /**
     * 关联的游客
     */
    private List<Traveler> travellers;

    /**
     * 指定的入住人
     */
//    private OccupantInfo occupant;

    /**
     * 成人数
     */
    private Integer adultNum;

    /**
     * 儿童数
     */
    private Integer childNum;

    /**
     * 游玩时间
     */
    private Date travelTime;

    /**
     * 服务开始时间
     */
    private Date serviceStartTime;

    /**
     * 服务结束时间
     */
    private Date serviceEndTime;

    /**
     * 角色类型。1-测试 2-普通
     */
    private Integer roleType;

    /**
     * 商品
     */
    @JSONField(serialize = false)
    private Lazy<List<OrderItem>> orderItemLazy;

    /**
     * 凭证信息
     */
//    @JSONField(serialize = false)
//    private Lazy<List<Voucher>> voucherListLazy;

    /**
     * 资金单
     */
    @JSONField(serialize = false)
    private Lazy<List<FundOrder>> fundOrderLisLazy;

    /**
     * 退款单
     */
    @JSONField(serialize = false)
    private Lazy<List<RefundOrder>> refundOrderListLazy;

    /**
     * 乐观锁
     */
    private Integer version;

    private Date gmtCreate;

    private Date gmtModified;

    private Map<String, String> attributes;

//    private List<Promotion> promotions;

    // todo
    public boolean isPaid() {
        return this.payStatus == PayStatus.PAID;
    }

    public boolean isNotPaid() {
        return !isPaid();
    }

    public boolean isEnd() {
        return this.orderStatus == OrderStatus.ORDER_TRADE_SUCCESS || this.orderStatus == OrderStatus.ORDER_TRADE_CLOSE || this.orderStatus == OrderStatus.ORDER_TRADE_CANCEL;
    }

    public boolean notRefund() {
        return this.refundStatus == RefundStatus.REFUND_ORDER_INIT;
    }

    public boolean isRefunding() {
        return this.refundStatus == RefundStatus.REFUNDING;
    }

    public boolean isRefundEnd() {
        return this.refundStatus == RefundStatus.REFUND_SUCCESS || this.refundStatus == RefundStatus.REFUND_FAIL;
    }

    public String getOutSkuIdByInnerSkuId(Long innerSkuId) {
        final List<OrderItem> orderItems = orderItemLazy.get();
        if (CollectionUtils.isEmpty(orderItems)) {
            return null;
        }
        for (OrderItem orderItem : orderItems) {
            final List<OrderItemSku> orderItemSkus = orderItem.getItemSkuListLazy().get();
            final OrderItemSku orderItemSku = orderItemSkus.stream().filter(s -> s.getSkuId().equals(innerSkuId)).findFirst().orElse(null);
            return Objects.nonNull(orderItemSku) ? orderItemSku.getOutSkuId() : null;
        }
        return null;
    }

}
package com.resort.order.domain.gateway;

import com.resort.order.domain.model.Order;

import java.util.List;

/**
 * @author issavior
 */
public interface OrderGateway {

     Order findById(Long orderId);

    /**
     * 创单
     * @param order
     */
    void createOrder(Order order);

    void updateStatuses(Order order);

    List<Order> queryOrderDetails();
}

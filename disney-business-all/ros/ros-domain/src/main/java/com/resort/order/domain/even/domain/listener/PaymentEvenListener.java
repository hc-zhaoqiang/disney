package com.resort.order.domain.even.domain.listener;

import com.disney.domain.sdk.anno.DomainEvenListener;
import com.resort.trade.domain.even.domain.PaymentEven;
import com.resort.trade.domain.model.FundOrder;
import com.resort.trade.domain.model.Order;
import org.springframework.context.event.EventListener;

/**
 * @author issavior
 */
@DomainEvenListener
public class PaymentEvenListener {

    @EventListener
    public void eventListener(PaymentEven even) {
        Order order = even.getOrder();
        FundOrder fundOrder = even.getFundOrder();

        order.setOrderStatus(OrderStatus.ORDER_TRADING);
        order.setPayStatus(PayStatus.PAID);
        order.setPerformanceStatus(PerformanceStatus.WAITING_VOUCHER);

        fundOrder.setStatus(FundStatus.FUND_ORDER_INIT);
    }
}

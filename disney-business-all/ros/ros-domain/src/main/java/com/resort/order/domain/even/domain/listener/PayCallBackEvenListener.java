package com.resort.order.domain.even.domain.listener;

import com.disney.domain.sdk.anno.DomainEvenListener;
import com.resort.trade.domain.even.domain.PayCallBackEven;
import com.resort.trade.domain.model.FundOrder;
import com.resort.trade.domain.model.Order;
import com.resort.trade.domain.status.FundStatus;
import com.resort.trade.domain.status.OrderStatus;
import com.resort.trade.domain.status.PayStatus;
import com.resort.trade.domain.status.PerformanceStatus;
import org.springframework.context.event.EventListener;

/**
 * @author issavior
 */
@DomainEvenListener
public class PayCallBackEvenListener {

    @EventListener
    public void eventListener(PayCallBackEven even) {
        Order order = even.getOrder();
        FundOrder fundOrder = even.getFundOrder();

        order.setPayStatus(PayStatus.PAID);
        // todo 定时任务扫表，过期且未核销需更改状态为用户未核销
        order.setPerformanceStatus(PerformanceStatus.VOUCHER_SENT);

        fundOrder.setStatus(FundStatus.PAY_SUCCESS);

        // todo 判断是否支付成功，失败则关闭
        if (!even.isSuccess()) {
            order.setOrderStatus(OrderStatus.ORDER_TRADE_CLOSE);
            fundOrder.setStatus(FundStatus.PAY_FAIL);

        }
    }
}

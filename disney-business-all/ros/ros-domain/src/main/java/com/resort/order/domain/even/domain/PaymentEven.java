package com.resort.order.domain.even.domain;

import com.disney.domain.sdk.even.DomainEven;
import com.resort.trade.domain.model.FundOrder;
import com.resort.trade.domain.model.Order;
import lombok.Builder;
import lombok.Data;

/**
 * @author issavior
 */
@Data
@Builder
public class PaymentEven implements DomainEven {
    private Order order;
    private FundOrder fundOrder;
}

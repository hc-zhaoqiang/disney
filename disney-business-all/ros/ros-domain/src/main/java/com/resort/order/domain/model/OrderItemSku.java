package com.resort.order.domain.model;

import com.alibaba.fastjson.JSONObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * 商品SKU
 * create by feiyang.mfy@alibaba-inc.com on 2023/7/3
 */
@Getter
@Setter
@ToString
public class OrderItemSku {

    /**
     * 主键
     */
    private Long id;

    /**
     * 商品表主键
     */
    private Long itemPid;

    /**
     * 商品ID
     */
    private Long itemId;

    /**
     * 订单ID
     */
    private Long orderId;

    /**
     * 商品类型
     */
//    private ItemTypeEnum itemType;

    /**
     * SKU类型
     */
    private String skuType;

    /**
     * 人群类型
     */
    private String personType;

    /**
     * 购买数量
     */
    private Integer quantity;

    /**
     * 价格
     */
    private Long price;

    /**
     * 标准价格
     */
    private Long standardPrice;

    /**
     * skuID
     */
    private Long skuId;

    /**
     * 关联的外部skuID
     */
    private String outSkuId;

    /**
     * 成人数
     */
    private Integer adultNum;

    /**
     * 儿童数
     */
    private Integer childNum;

    /**
     * 间夜数
     */
    private Integer nights;

    /**
     * sku名称
     */
    private String skuName;

    /**
     * sku子标题
     */
    private String skuSubName;

    /**
     * 出行日期
     */
    private String travelDate;

    /* skuType=HOTEL必传字段 */
    /**
     * 入住时间或有效期开始时间 格式：yyyy-MM-dd HH:mm:ss
     */
    private String checkInDate;

    /**
     * 离店时间或有效期结束时间 格式：yyyy-MM-dd HH:mm:ss
     */
    private String checkOutDate;

    /**
     * hotelGHALoyaltyCode
     */
    private String hotelGHALoyaltyCode;

    /**
     * 酒店编码
     */
    private String hotelCode;

    /**
     * 房价类型编码
     */
    private String ratePlanCode;

    /**
     * 房间类型编码
     */
    private String roomTypeCode;

    /**
     * 子渠道代码
     */
    private String sourceChannelCode;
    /* skuType=HOTEL必传字段 */

    /**
     * 酒景套餐子商品元素
     */
//    private Lazy<List<OrderSubItem>> orderSubItemsLazy;

    private Date gmtCreate;
    private Date gmtModified;
    private Integer version;

    /**
     * 扩展属性
     */
    private JSONObject attributes;

}
package com.resort.order.domain.status;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author issavior
 */
@Getter
public enum PerformanceStatus {

    PERFORMANCE_INIT(1, "履约单初始化"),
    WAITING_VOUCHER(2, "订单支付中，等待发送凭证"),
    VOUCHER_SENT(3, "订单支付成功，已发送凭证"),
    UNVERIFIED(4, "用户未核销"),
    VERIFIED(5, "用户已核销"),

    UNKNOWN(-1,"未知")
    ;

    private final Integer code;
    private final String desc;

    PerformanceStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static PerformanceStatus of(Integer code) {
        return Arrays.stream(PerformanceStatus.values())
                .filter(e -> Objects.equals(e.getCode(), code))
                .findFirst()
                .orElse(UNKNOWN);
    }
}

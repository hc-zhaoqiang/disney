package com.resort.order.domain.gateway;

import com.resort.order.domain.model.OrderItem;

/**
 * @author issavior
 */
public interface OrderItemGateway {
    void createOrderItem(OrderItem orderItem);
}

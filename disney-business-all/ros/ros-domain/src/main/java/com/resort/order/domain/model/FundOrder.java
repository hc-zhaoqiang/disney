package com.resort.order.domain.model;

import com.alibaba.fastjson.JSONObject;
import com.disney.domain.sdk.exception.BizException;
import com.resort.trade.domain.enums.CurrencyType;
import com.resort.trade.domain.status.FundStatus;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 资金单
 *
 * @author issavior
 */
@Getter
@Setter
public class FundOrder {

    /**
     * 资金单ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 销售单id
     */
    private Long orderId;

    /**
     * 退款单id
     */
    private Long refundOrderId;

    /**
     * 付款方名称
     */
    private String payerNick;

    /**
     * 付款方账号id
     */
    private String payerAccountId;

    /**
     * 付款方账号类型
     */
    private String payerAccountType;

    /**
     * 收款方账号id
     */
    private String receiverAccountId;

    /**
     * 收款方名称
     */
    private String receiverNick;

    /**
     * 收款方账号类型
     */
    private String receiverAccountType;

    /**
     * 外部支付订单号（支付退款流水号）
     */
    private String outPayOrderId;

    /**
     * 支付类型
     */
//    private PayType payType;

    /**
     * 资金单状态
     */
    private FundStatus status;

    /**
     * 资金单类型
     */
//    private FundType fundType;

    /**
     * 支付总金额
     */
    private Long payFee;

    /**
     * 支付时间
     */
    private Date payTime;

    /**
     * 回调时间
     */
    private Date receiveTime;

    /**
     * 币种
     */
    private CurrencyType currencyType;

    /**
     * 扩展属性
     */
    private JSONObject attributes = new JSONObject();

    public boolean isEnd() {
        return FundStatus.PAY_FAIL == status || FundStatus.PAY_SUCCESS == status;
    }

    public boolean isFailEnd() {
        return FundStatus.PAY_FAIL == status;
    }

    public boolean isSuccessEnd() {
        return FundStatus.PAY_SUCCESS == status;
    }

    /**
     * 获取支付单标题
     *
     * @return
     */
    public String getSubject() {
        Object subject = attributes.get("subject");
        if (subject == null) {
            return null;
        }
        return String.valueOf(subject);
    }

    /**
     * 设置支付单标题
     *
     * @param subject
     */
    public void setSubject(String subject) {
        attributes.put("subject", subject);
    }

    public void setExternalCreateErrMsg(String code, String msg) {
        attributes.put("externalCreateErr", String.format("code: %s, msg: %s", code, msg));
    }

    public void setExternalRefundErrMsg(String code, String msg) {
        attributes.put("externalRefundErr", String.format("code: %s, msg: %s", code, msg));
    }

    public void setExternalCancelErrMsg(String code, String msg) {
        attributes.put("externalCancelErr", String.format("code: %s, msg: %s", code, msg));
    }

    public void idempotent() {
        if (this.status == FundStatus.PAY_SUCCESS) {
            throw new BizException("资金单异常");
        }
    }
}

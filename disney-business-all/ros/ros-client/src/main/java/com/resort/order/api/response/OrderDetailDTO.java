package com.resort.order.api.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder
public class OrderDetailDTO implements Serializable {
}

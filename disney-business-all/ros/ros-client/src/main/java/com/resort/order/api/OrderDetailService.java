package com.resort.order.api;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.order.api.response.OrderDetailDTO;

import java.util.List;

/**
 * @author issavior
 */
public interface OrderDetailService {

    DisneyResult<List<OrderDetailDTO>> orderDetails();
}

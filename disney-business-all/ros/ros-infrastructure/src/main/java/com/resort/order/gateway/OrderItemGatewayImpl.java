package com.resort.order.gateway;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.order.convert.OrderItemConvert;
import com.resort.order.domain.gateway.OrderItemGateway;
import com.resort.order.domain.model.OrderItem;
import com.resort.order.mapper.RtsOrderItemMapper;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author issavior
 */
@DomainGateway
public class OrderItemGatewayImpl implements OrderItemGateway {

    @Resource
    private RtsOrderItemMapper rtsOrderItemMapper;

    @Resource
    private OrderItemConvert orderItemConvert;

    @Override
    public void createOrderItem(OrderItem orderItem) {

        orderItem.setGmtCreate(new Date());
        orderItem.setGmtModified(new Date());
        orderItem.setVersion(1);
        rtsOrderItemMapper.insert(orderItemConvert.toRtsOrderItem(orderItem));
    }


}

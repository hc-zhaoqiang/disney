package com.resort.order.pojo;

import java.util.Date;

public class RtsRefundOrder {
    private Long id;

    private Date createdTime;

    private Date updatedTime;

    private Long orderId;

    private String bizCode;

    private Short status;

    private Long amount;

    private Long totalRefundFee;

    private Long totalServiceFee;

    private Long outRefundId;

    private String goodsName;

    private Date refundGoodsTime;

    private Date refundTime;

    private String reason;

    private String desc;

    private Integer version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getBizCode() {
        return bizCode;
    }

    public void setBizCode(String bizCode) {
        this.bizCode = bizCode;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getTotalRefundFee() {
        return totalRefundFee;
    }

    public void setTotalRefundFee(Long totalRefundFee) {
        this.totalRefundFee = totalRefundFee;
    }

    public Long getTotalServiceFee() {
        return totalServiceFee;
    }

    public void setTotalServiceFee(Long totalServiceFee) {
        this.totalServiceFee = totalServiceFee;
    }

    public Long getOutRefundId() {
        return outRefundId;
    }

    public void setOutRefundId(Long outRefundId) {
        this.outRefundId = outRefundId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Date getRefundGoodsTime() {
        return refundGoodsTime;
    }

    public void setRefundGoodsTime(Date refundGoodsTime) {
        this.refundGoodsTime = refundGoodsTime;
    }

    public Date getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(Date refundTime) {
        this.refundTime = refundTime;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
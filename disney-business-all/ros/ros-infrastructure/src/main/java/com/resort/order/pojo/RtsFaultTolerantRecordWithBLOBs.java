package com.resort.order.pojo;

public class RtsFaultTolerantRecordWithBLOBs extends RtsFaultTolerantRecord {
    private String requestData;

    private String resultData;

    public String getRequestData() {
        return requestData;
    }

    public void setRequestData(String requestData) {
        this.requestData = requestData;
    }

    public String getResultData() {
        return resultData;
    }

    public void setResultData(String resultData) {
        this.resultData = resultData;
    }
}
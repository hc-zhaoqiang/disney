package com.resort.order.pojo;

public class RtsRefundOrderWithBLOBs extends RtsRefundOrder {
    private String orderLines;

    private String rule;

    private String attributes;

    public String getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(String orderLines) {
        this.orderLines = orderLines;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }
}
package com.resort.order.mapper;

import com.resort.order.pojo.RtsFundOrder;
import com.resort.order.pojo.RtsFundOrderExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RtsFundOrderMapper {
    long countByExample(RtsFundOrderExample example);

    int deleteByExample(RtsFundOrderExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RtsFundOrder record);

    int insertSelective(RtsFundOrder record);

    List<RtsFundOrder> selectByExample(RtsFundOrderExample example);

    RtsFundOrder selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RtsFundOrder record, @Param("example") RtsFundOrderExample example);

    int updateByExample(@Param("record") RtsFundOrder record, @Param("example") RtsFundOrderExample example);

    int updateByPrimaryKeySelective(RtsFundOrder record);

    int updateByPrimaryKey(RtsFundOrder record);
}
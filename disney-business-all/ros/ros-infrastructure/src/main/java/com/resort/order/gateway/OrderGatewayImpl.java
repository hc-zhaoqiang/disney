package com.resort.order.gateway;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.order.convert.OrderConvert;
import com.resort.order.domain.gateway.OrderGateway;
import com.resort.order.domain.model.Order;
import com.resort.order.domain.status.OrderStatus;
import com.resort.order.mapper.RtsOrderMapper;
import com.resort.order.pojo.RtsOrder;
import com.resort.order.pojo.RtsOrderExample;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class OrderGatewayImpl implements OrderGateway {

    @Resource
    private RtsOrderMapper rtsOrderMapper;

    @Resource
    private OrderConvert orderConvert;

    @Override
    public Order findById(Long orderId) {
        return null;
    }

    @Override
    public void createOrder(Order order) {

        order.setGmtCreate(new Date());
        order.setGmtModified(new Date());
        order.setVersion(1);
        rtsOrderMapper.insert(orderConvert.toRtsOrder(order));
    }

    @Override
    public void updateStatuses(Order order) {

    }

    @Override
    public List<Order> queryOrderDetails() {

        RtsOrderExample example = new RtsOrderExample();
        return orderConvert.toRtsOrders(rtsOrderMapper.selectByExample(example));
    }
}

package com.resort.order.gateway;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.order.domain.gateway.RefundOrderGateway;

/**
 * @author issavior
 */
@DomainGateway
public class RefundOrderGatewayImpl implements RefundOrderGateway {
}

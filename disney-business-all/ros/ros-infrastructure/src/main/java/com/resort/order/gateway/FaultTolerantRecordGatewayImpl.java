package com.resort.order.gateway;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.order.domain.gateway.FaultTolerantRecordGateway;

/**
 * @author issavior
 */
@DomainGateway
public class FaultTolerantRecordGatewayImpl implements FaultTolerantRecordGateway {
}

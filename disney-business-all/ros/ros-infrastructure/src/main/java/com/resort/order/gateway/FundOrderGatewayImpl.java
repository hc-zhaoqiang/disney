package com.resort.order.gateway;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.order.domain.gateway.FundOrderGateway;
import com.resort.order.domain.model.FundOrder;

/**
 * @author issavior
 */
@DomainGateway
public class FundOrderGatewayImpl implements FundOrderGateway {
    @Override
    public FundOrder findByOrderId(Long orderId) {
        return null;
    }

    @Override
    public void insert(FundOrder fundOrder) {

    }

    @Override
    public void updateStatuses(FundOrder fundOrder) {

    }
}

package com.resort.order.convert;

import com.disney.domain.sdk.anno.DomainConvert;
import com.disney.domain.sdk.common.DisneyResult;
import com.resort.order.api.response.OrderDetailDTO;
import com.resort.order.domain.model.Order;
import com.resort.order.pojo.RtsOrder;
import com.resort.order.pojo.RtsOrderWithBLOBs;

import java.util.List;

/**
 * @author issavior
 */
@DomainConvert
public class OrderConvert {
    public RtsOrderWithBLOBs toRtsOrder(Order order) {

        RtsOrderWithBLOBs rtsOrderWithBLOBs = new RtsOrderWithBLOBs();
        rtsOrderWithBLOBs.setBizCode(order.getBizCodeEnum().getCode());
        return rtsOrderWithBLOBs;
    }

    public List<Order> toRtsOrders(List<RtsOrder> rtsOrders) {
        return null;
    }

    public DisneyResult<List<OrderDetailDTO>> toOrderDetailDTO(List<Order> orders) {
        return null;
    }
}

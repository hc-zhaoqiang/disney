package com.resort.order.mapper;

import com.resort.order.pojo.RtsOrderItemSku;
import com.resort.order.pojo.RtsOrderItemSkuExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RtsOrderItemSkuMapper {
    long countByExample(RtsOrderItemSkuExample example);

    int deleteByExample(RtsOrderItemSkuExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RtsOrderItemSku record);

    int insertSelective(RtsOrderItemSku record);

    List<RtsOrderItemSku> selectByExampleWithBLOBs(RtsOrderItemSkuExample example);

    List<RtsOrderItemSku> selectByExample(RtsOrderItemSkuExample example);

    RtsOrderItemSku selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RtsOrderItemSku record, @Param("example") RtsOrderItemSkuExample example);

    int updateByExampleWithBLOBs(@Param("record") RtsOrderItemSku record, @Param("example") RtsOrderItemSkuExample example);

    int updateByExample(@Param("record") RtsOrderItemSku record, @Param("example") RtsOrderItemSkuExample example);

    int updateByPrimaryKeySelective(RtsOrderItemSku record);

    int updateByPrimaryKeyWithBLOBs(RtsOrderItemSku record);

    int updateByPrimaryKey(RtsOrderItemSku record);
}
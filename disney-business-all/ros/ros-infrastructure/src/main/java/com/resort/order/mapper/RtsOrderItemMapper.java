package com.resort.order.mapper;

import com.resort.order.pojo.RtsOrderItem;
import com.resort.order.pojo.RtsOrderItemExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RtsOrderItemMapper {
    long countByExample(RtsOrderItemExample example);

    int deleteByExample(RtsOrderItemExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RtsOrderItem record);

    int insertSelective(RtsOrderItem record);

    List<RtsOrderItem> selectByExampleWithBLOBs(RtsOrderItemExample example);

    List<RtsOrderItem> selectByExample(RtsOrderItemExample example);

    RtsOrderItem selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RtsOrderItem record, @Param("example") RtsOrderItemExample example);

    int updateByExampleWithBLOBs(@Param("record") RtsOrderItem record, @Param("example") RtsOrderItemExample example);

    int updateByExample(@Param("record") RtsOrderItem record, @Param("example") RtsOrderItemExample example);

    int updateByPrimaryKeySelective(RtsOrderItem record);

    int updateByPrimaryKeyWithBLOBs(RtsOrderItem record);

    int updateByPrimaryKey(RtsOrderItem record);
}
package com.resort.order.pojo;

public class RtsOrderWithBLOBs extends RtsOrder {
    private String travellers;

    private String occupant;

    public String getTravellers() {
        return travellers;
    }

    public void setTravellers(String travellers) {
        this.travellers = travellers;
    }

    public String getOccupant() {
        return occupant;
    }

    public void setOccupant(String occupant) {
        this.occupant = occupant;
    }
}
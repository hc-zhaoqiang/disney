package com.resort.order.gateway;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.order.convert.OrderItemSkuConvert;
import com.resort.order.domain.gateway.OrderItemSkuGateway;
import com.resort.order.domain.model.OrderItemSku;
import com.resort.order.mapper.RtsOrderItemSkuMapper;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author issavior
 */
@DomainGateway
public class OrderItemSkuGatewayImpl implements OrderItemSkuGateway {

    @Resource
    private RtsOrderItemSkuMapper rtsOrderItemSkuMapper;

    @Resource
    private OrderItemSkuConvert orderItemSkuConvert;

    @Override
    public void createOrderItemSku(OrderItemSku orderItemSku) {

        orderItemSku.setGmtCreate(new Date());
        orderItemSku.setGmtModified(new Date());
        orderItemSku.setVersion(1);
        rtsOrderItemSkuMapper.insert(orderItemSkuConvert.toRtsOrderItem(orderItemSku));
    }
}

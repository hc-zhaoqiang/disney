package com.resort.data.domain.customer.gateway;

import com.resort.data.domain.customer.Customer;

public interface CustomerGateway {
    Customer getByById(String customerId);
}

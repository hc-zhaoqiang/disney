package com.resort.car.domain.customer.gateway;

import com.resort.car.domain.customer.Customer;

public interface CustomerGateway {
    Customer getByById(String customerId);
}

package com.resort.car.domain.gateway;

import com.resort.car.domain.model.Car;

import java.util.List;

/**
 * @author issavior
 */
public interface CarGateway {

    List<Car> queryCarList();

    Void addCar(Car car);

    Void deleteCar(Long id);

    Void deleteCarList(List<Long> cars);
}

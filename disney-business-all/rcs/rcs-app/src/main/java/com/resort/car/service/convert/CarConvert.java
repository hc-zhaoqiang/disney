package com.resort.car.service.convert;

import com.resort.car.api.request.CarRequest;
import com.resort.car.api.request.PreOrderRequest;
import com.resort.car.api.response.CarDTO;
import com.resort.car.domain.model.Car;
import com.resort.trade.api.order.request.OrderValidateRequest;

import java.util.List;

/**
 * @author issavior
 */
public class CarConvert {
    public static List<CarDTO> fromCars(List<Car> cars) {
        return null;
    }

    public static Car toCar(CarRequest request) {
        return null;
    }

    public static OrderValidateRequest fromPreOrderRequest(PreOrderRequest request) {
        return null;
    }
}

package com.resort.car.service;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.car.api.CarService;
import com.resort.car.api.request.CarRequest;
import com.resort.car.api.request.PreOrderRequest;
import com.resort.car.api.response.CarDTO;
import com.resort.car.domain.gateway.CarGateway;
import com.resort.car.domain.model.Car;
import com.resort.trade.api.order.TradeService;
import com.resort.trade.api.order.response.OrderValidateDTO;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import com.resort.car.service.convert.CarConvert;

/**
 * @author issavior
 */
@Service
public class CarServiceImpl implements CarService {

    @Resource
    private CarGateway carGateway;

    @DubboReference
    private TradeService tradeService;

    @Override
    public DisneyResult<Void> preOrder(PreOrderRequest request) {
        // todo 校验是否过期或下架

        // todo 库存是否足够
        DisneyResult<OrderValidateDTO> result = tradeService.orderValidate(CarConvert.fromPreOrderRequest(request));
        if (result.isFail()) {
            return DisneyResult.failed("库存不足，请重新选择");
        }
        return DisneyResult.ok();
    }

    @Override
    public DisneyResult<List<CarDTO>> queryCarList() {

        return DisneyResult.ok(CarConvert.fromCars(carGateway.queryCarList()));
    }

    @Override
    public DisneyResult<Void> addCar(CarRequest request) {

        return DisneyResult.ok(carGateway.addCar(CarConvert.toCar(request)));

    }

    @Override
    public DisneyResult<Void> deleteCar(Long carId) {
        return DisneyResult.ok(carGateway.deleteCar(carId));

    }

    @Override
    public DisneyResult<Void> deleteCars(List<Long> cars) {
        return DisneyResult.ok(carGateway.deleteCarList(cars));

    }
}

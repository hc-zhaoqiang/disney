package com.resort.car.gateway;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.car.domain.gateway.CarGateway;
import com.resort.car.domain.model.Car;

import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class CarGatewayImpl implements CarGateway {
    @Override
    public List<Car> queryCarList() {
        return null;
    }

    @Override
    public Void addCar(Car car) {

        return null;
    }

    @Override
    public Void deleteCar(Long id) {
        return null;
    }

    @Override
    public Void deleteCarList(List<Long> cars) {
        return null;
    }

}

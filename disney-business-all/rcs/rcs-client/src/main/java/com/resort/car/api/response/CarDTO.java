package com.resort.car.api.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder
public class CarDTO implements Serializable {
}

package com.resort.car.api;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.car.api.request.CarRequest;
import com.resort.car.api.request.PreOrderRequest;
import com.resort.car.api.response.CarDTO;

import java.util.List;

/**
 * @author issavior
 */
public interface CarService {

    DisneyResult<Void> preOrder(PreOrderRequest request);

    DisneyResult<List<CarDTO>> queryCarList();

    DisneyResult<Void> addCar(CarRequest request);

    DisneyResult<Void> deleteCar(Long carId);

    DisneyResult<Void> deleteCars(List<Long> cars);
}

package com.resort.user.domain.customer.gateway;

import com.resort.user.domain.customer.Credit;

//Assume that the credit info is in another distributed Service
public interface CreditGateway {
    Credit getCredit(String customerId);
}

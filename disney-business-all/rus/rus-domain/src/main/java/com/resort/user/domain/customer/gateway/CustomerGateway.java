package com.resort.user.domain.customer.gateway;

import com.resort.user.domain.customer.Customer;

public interface CustomerGateway {
    Customer getByById(String customerId);
}

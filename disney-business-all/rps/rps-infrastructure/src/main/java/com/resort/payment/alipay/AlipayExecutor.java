package com.resort.payment.alipay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.AlipayRequest;
import com.alipay.api.AlipayResponse;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Executor
 *
 * @author issavior
 */
@Repository
public class AlipayExecutor {
    @Resource
    private AlipayClient alipayClient;

    public <T extends AlipayResponse> T execute(AlipayRequest<T> alipayRequest) {

        try {
            return alipayClient.execute(alipayRequest);
        } catch (AlipayApiException e) {
            throw new RuntimeException(e);
        }

    }
}

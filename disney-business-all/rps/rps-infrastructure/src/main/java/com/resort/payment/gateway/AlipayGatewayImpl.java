package com.resort.payment.gateway;

import com.alipay.api.request.*;
import com.alipay.api.response.*;
import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.payment.alipay.AlipayExecutor;
import com.resort.payment.domain.gateway.AlipayGateway;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author issavior
 */
@DomainGateway
public class AlipayGatewayImpl implements AlipayGateway {

    @Resource
    private AlipayExecutor alipayExecutor;

    @Override
    public AlipayTradeCreateResponse tradeCreate(AlipayTradeCreateRequest request) {
        return alipayExecutor.execute(request);
    }

    @Override
    public AlipayTradeQueryResponse tradeQuery(AlipayTradeQueryRequest request) {
        return alipayExecutor.execute(request);
    }

    @Override
    public AlipayTradeRefundResponse tradeRefund(AlipayTradeRefundRequest request) {
        return alipayExecutor.execute(request);
    }

    @Override
    public AlipayTradeCancelResponse tradeCancel(AlipayTradeCancelRequest request) {
        return alipayExecutor.execute(request);
    }

    @Override
    public AlipayTradeCloseResponse tradeClose(AlipayTradeCloseRequest request) {
        return alipayExecutor.execute(request);
    }

    @Override
    public AlipayTradeFastpayRefundQueryResponse refundQuery(AlipayTradeFastpayRefundQueryRequest request) {
        return alipayExecutor.execute(request);
    }

    @Override
    public boolean checkTrade(Map<String, String> params) {
        return true;
    }
}

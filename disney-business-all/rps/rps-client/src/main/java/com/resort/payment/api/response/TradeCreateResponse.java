package com.resort.payment.api.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder
public class TradeCreateResponse implements Serializable {

    private String tradeNo;
}

package com.resort.payment.api.request;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder
public class TradeCreateRequest implements Serializable {
    public String bizCode;

    private String orderId;
    private String payFee;
    private String buyerId;
    private String orderTimeOut;
    private String itemDesc;

}

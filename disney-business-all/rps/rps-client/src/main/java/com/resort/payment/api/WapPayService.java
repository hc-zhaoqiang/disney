package com.resort.payment.api;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.payment.api.request.TradeCreateRequest;
import com.resort.payment.api.response.TradeCreateResponse;

/**
 * @author issavior
 */
public interface WapPayService {

    DisneyResult<TradeCreateResponse> getTradeNo(TradeCreateRequest request);
}

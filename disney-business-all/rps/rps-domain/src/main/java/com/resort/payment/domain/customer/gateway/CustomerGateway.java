package com.resort.payment.domain.customer.gateway;

import com.resort.payment.domain.customer.Customer;

public interface CustomerGateway {
    Customer getByById(String customerId);
}

package com.resort.payment.domain.customer.gateway;

import com.resort.payment.domain.customer.Credit;

//Assume that the credit info is in another distributed Service
public interface CreditGateway {
    Credit getCredit(String customerId);
}

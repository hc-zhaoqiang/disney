package com.resort.payment.domain.gateway;

import com.alipay.api.request.*;
import com.alipay.api.response.*;

import java.util.Map;

/**
 * @author issavior
 */
public interface AlipayGateway {
    AlipayTradeCreateResponse tradeCreate(AlipayTradeCreateRequest request);

    AlipayTradeQueryResponse tradeQuery(AlipayTradeQueryRequest request);

    AlipayTradeRefundResponse tradeRefund(AlipayTradeRefundRequest request);

    AlipayTradeCancelResponse tradeCancel(AlipayTradeCancelRequest request);

    AlipayTradeCloseResponse tradeClose(AlipayTradeCloseRequest request);

    AlipayTradeFastpayRefundQueryResponse refundQuery(AlipayTradeFastpayRefundQueryRequest request);

    boolean checkTrade(Map<String, String> params);
}

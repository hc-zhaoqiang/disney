package com.resort.payment.apps.wechat;

import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.payment.api.request.TradeCreateRequest;
import com.resort.payment.api.response.TradeCreateResponse;
import com.resort.payment.apps.WapPayDomainBiz;
import com.disney.domain.sdk.anno.Biz;


/**
 * @author issavior
 */
@Biz(DomainBizType.WECHAT_PAY)
public class WechatWapPayDomainBiz extends WapPayDomainBiz {
    @Override
    public TradeCreateResponse getTradeNo(TradeCreateRequest request) {
        return null;
    }
}

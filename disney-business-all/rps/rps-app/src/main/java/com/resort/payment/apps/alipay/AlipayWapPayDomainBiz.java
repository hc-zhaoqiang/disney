package com.resort.payment.apps.alipay;

import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.payment.api.request.TradeCreateRequest;
import com.resort.payment.api.response.TradeCreateResponse;
import com.resort.payment.apps.WapPayDomainBiz;
import com.disney.domain.sdk.anno.Biz;
import com.resort.payment.domain.gateway.AlipayGateway;

import javax.annotation.Resource;
import com.resort.payment.apps.alipay.convert.AlipayConvert;


/**
 * @author issavior
 */
@Biz(DomainBizType.ALIPAY_PAY)
public class AlipayWapPayDomainBiz extends WapPayDomainBiz {

    @Resource
    private AlipayGateway alipayGateway;
    @Override
    public TradeCreateResponse getTradeNo(TradeCreateRequest request) {

        return AlipayConvert.toTradeCreateResponse(alipayGateway.tradeCreate(AlipayConvert.fromTradeCreateRequest(request)));
    }
}

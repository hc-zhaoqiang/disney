package com.resort.payment.apps;

import com.disney.domain.sdk.anno.AbsBiz;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.IDomainBiz;
import com.resort.payment.api.request.TradeCreateRequest;
import com.resort.payment.api.response.TradeCreateResponse;


/**
 * @author issavior
 */
@AbsBiz(DomainAbsBizType.PAY)
public abstract class WapPayDomainBiz implements IDomainBiz {

    public abstract TradeCreateResponse getTradeNo(TradeCreateRequest request);
}

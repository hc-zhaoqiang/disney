package com.resort.payment.service;

import com.disney.domain.sdk.common.DisneyResult;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.enums.DomainBizType;
import com.disney.domain.sdk.factory.DomainExecutor;
import com.disney.domain.sdk.factory.DomainRequest;
import com.resort.payment.api.WapPayService;
import com.resort.payment.api.request.TradeCreateRequest;
import com.resort.payment.api.response.TradeCreateResponse;
import com.resort.payment.apps.WapPayDomainBiz;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

/**
 * @author issavior
 */
@DubboService
public class WapPayServiceImpl implements WapPayService {

    private static final DomainExecutor<WapPayDomainBiz> WAP_PAY_EXECUTOR = new DomainExecutor<>(WapPayDomainBiz.class);

    @Override
    public DisneyResult<TradeCreateResponse> getTradeNo(TradeCreateRequest request) {


        DomainRequest domainRequest = DomainRequest.to()
                .code(DomainBizType.of(request.getBizCode()))
                .scenario(DomainAbsBizType.PAY)
                .end();

        return DisneyResult.ok(WAP_PAY_EXECUTOR.execFirst(domainRequest, biz -> biz.getTradeNo(request)));
    }
}

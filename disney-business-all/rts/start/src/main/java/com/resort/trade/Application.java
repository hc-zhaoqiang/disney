package com.resort.trade;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot Starter
 *
 * @author Frank Zhang
 */
@SpringBootApplication(scanBasePackages = {"com.resort.trade", "com.alibaba.cola"})
@EnableDubbo
//@MapperScan("com.resort.trade")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

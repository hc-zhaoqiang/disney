package com.resort.trade.domain.model;

import com.alibaba.fastjson.JSONObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

/**
 * 商品SKU
 * @author issavior
 */
@Getter
@Setter
@ToString
public class OrderItemSku {

    /**
     * 主键
     */
    private Long id;

    /**
     * 商品表主键
     */
    private Long itemPid;

    /**
     * 商品Id
     */
    private Long itemId;

    /**
     * 订单Id
     */
    private Long orderId;

    /**
     * SKU类型
     */
    private String skuType;

    /**
     * 购买数量
     */
    private Integer quantity;

    /**
     * 价格
     */
    private Long price;

    /**
     * 标准价格
     */
    private Long standardPrice;

    /**
     * skuID
     */
    private Long skuId;

    /**
     * 关联的外部skuID
     */
    private String outSkuId;

    /**
     * 成人数
     */
    private Integer adultNum;

    /**
     * 儿童数
     */
    private Integer childNum;

    /**
     * 间夜数
     */
    private Integer nights;

    /**
     * sku名称
     */
    private String skuName;

    /**
     * sku子标题
     */
    private String skuSubName;

    /**
     * 出行日期
     */
    private String travelDate;

    /**
     * 酒店编码
     */
    private String hotelCode;

    private Date createdTime;
    private Date updatedTime;
    private Integer version;

    /**
     * 扩展属性
     */
    private JSONObject attributes;

}
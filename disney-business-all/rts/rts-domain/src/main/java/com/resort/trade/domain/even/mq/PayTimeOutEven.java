package com.resort.trade.domain.even.mq;

import com.disney.domain.sdk.even.MQEven;
import com.disney.domain.sdk.mq.DomainMessage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author issavior
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PayTimeOutEven<T> implements MQEven {

    private DomainMessage<T> domainMessage;
}

package com.resort.trade.domain.even.domain.listener;

import com.disney.domain.sdk.anno.DomainEvenListener;
import com.resort.trade.domain.even.domain.CreateOrderEven;
import com.resort.trade.domain.model.Order;
import com.resort.trade.domain.status.OrderStatus;
import com.resort.trade.domain.status.PayStatus;
import com.resort.trade.domain.status.PerformanceStatus;
import com.resort.trade.domain.status.RefundStatus;
import org.springframework.context.event.EventListener;

/**
 * @author issavior
 */
@DomainEvenListener
public class CreateOrderListener {

    @EventListener
    public void eventListener(CreateOrderEven even) {
        Order order = even.getOrder();
        order.setOrderStatus(OrderStatus.ORDER_INIT);
        order.setPayStatus(PayStatus.TO_BE_PAID);
        order.setPerformanceStatus(PerformanceStatus.PERFORMANCE_INIT);
        order.setRefundStatus(RefundStatus.REFUND_ORDER_INIT);
    }
}

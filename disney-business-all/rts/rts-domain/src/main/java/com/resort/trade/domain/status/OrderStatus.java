package com.resort.trade.domain.status;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author issavior
 */
@Getter
public enum OrderStatus {

    ORDER_INIT(1, "订单初始化"),
    ORDER_TRADING(2, "订单交易中"),
    ORDER_TRADE_SUCCESS(3, "订单交易成功"),
    ORDER_TRADE_CLOSE(4, "订单交易关闭"),
    ORDER_TRADE_CANCEL(5, "订单交易取消"),

    UNKNOWN(-1,"未知")
    ;

    private final Integer code;
    private final String desc;

    OrderStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static OrderStatus of(Integer code) {
        return Arrays.stream(OrderStatus.values())
                .filter(e -> Objects.equals(e.getCode(), code))
                .findFirst()
                .orElse(UNKNOWN);
    }
}

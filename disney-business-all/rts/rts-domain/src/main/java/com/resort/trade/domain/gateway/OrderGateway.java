package com.resort.trade.domain.gateway;

import com.resort.trade.domain.model.Order;

/**
 * @author issavior
 */
public interface OrderGateway {

     Order findById(Long orderId);

    /**
     * 创单
     * @param order
     */
    void createOrder(Order order);

    void updateStatuses(Order order);
}

package com.resort.trade.domain.status;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author issavior
 */
@Getter
public enum RefundStatus {

    REFUND_ORDER_INIT(1, "退款单初始化"),
    REFUNDING(2, "退款中"),
    REFUND_SUCCESS(3, "退款成功"),
    REFUND_FAIL(4, "退款失败"),

    UNKNOWN(-1,"未知")
    ;

    private final Integer code;
    private final String desc;

    RefundStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static RefundStatus of(Integer code) {
        return Arrays.stream(RefundStatus.values())
                .filter(e -> Objects.equals(e.getCode(), code))
                .findFirst()
                .orElse(UNKNOWN);
    }
}

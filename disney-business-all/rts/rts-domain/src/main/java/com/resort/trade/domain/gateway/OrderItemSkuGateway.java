package com.resort.trade.domain.gateway;

import com.resort.trade.domain.model.OrderItemSku;

public interface OrderItemSkuGateway {
    void createOrderItemSku(OrderItemSku orderItemSku);
}

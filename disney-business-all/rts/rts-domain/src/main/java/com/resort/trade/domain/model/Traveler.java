package com.resort.trade.domain.model;

import lombok.Data;

import java.util.Date;


/**
 * @author issavior
 */
@Data
public class Traveler {

    /**
     * Id
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;
    /**
     * 用户Id
     */
    private String userId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 国际编码
     */
    private String mobilePreFix;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 证件类型
     */
    private Integer docType;

    /**
     * 证件id
     */
    private String docId;
    /**
     * 人群类型（1-Senior老人、2-Adult成人、3-Child儿童、4-残章人士）
     */
    private Integer personCode;

    /**
     * 人群类型（1-Old老人、2-Adult成人、3-Child儿童、4-残章人士）
     */
    private String personType;
    /**
     * 人群类型（1-Old老人、2-Adult成人、3-Child儿童、4-残章人士）
     */
    private String personTypeName;

    /**
     * 乐观锁version
     */
    private Integer version;
    /**
     * 有效 1，无效-1
     */
    private Integer active;

}

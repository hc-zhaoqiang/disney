package com.resort.trade.domain.gateway;

import com.resort.trade.domain.model.OrderItem;

/**
 * @author issavior
 */
public interface OrderItemGateway {
    void createOrderItem(OrderItem orderItem);
}

package com.resort.trade.domain.even.domain;

import com.disney.domain.sdk.even.DomainEven;
import com.resort.trade.domain.model.FundOrder;
import com.resort.trade.domain.model.Order;
import com.resort.trade.domain.model.RefundOrder;
import lombok.Builder;
import lombok.Data;

/**
 * @author issavior
 */
@Data
@Builder
public class RefundEven implements DomainEven {

    private Order order;
    private RefundOrder refundOrder;
}

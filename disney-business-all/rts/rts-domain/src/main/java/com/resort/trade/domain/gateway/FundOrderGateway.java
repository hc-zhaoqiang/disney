package com.resort.trade.domain.gateway;

import com.resort.trade.domain.model.FundOrder;

/**
 * @author issavior
 */
public interface FundOrderGateway {
    FundOrder findByOrderId(Long orderId);

    void insert(FundOrder fundOrder);

    void updateStatuses(FundOrder fundOrder);
}

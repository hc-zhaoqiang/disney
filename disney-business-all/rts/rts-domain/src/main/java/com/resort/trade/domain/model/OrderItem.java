package com.resort.trade.domain.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.disney.domain.sdk.common.Lazy;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 商品快照
 * @author issavior
 */
@Getter
@Setter
@ToString
public class OrderItem {

    /**
     * 订单商品表主键
     */
    private Long id;

    /**
     * 商品id
     */
    private Long itemId;

    /**
     * 关联的订单
     */
    private Long orderId;

    /**
     * 商品名称
     */
    private String itemName;

    /**
     * 商品子标题
     */
    private String itemSubName;

    /**
     * 外部商品id
     */
    private String outItemId;

    /**
     * 票型或房型
     */
    private String itemKind;

    /**
     * 封面图片，如券封面
     */
    private String coverImg;

    /**
     * 页卡图片
     */
    private String pageCardImg;

    /**
     * 是否需要实名制。
     */
    private Boolean needRealName;

    /**
     * 扩展属性
     */
    private Map<String, String> attributes;

    private Integer version;

    private Date gmtCreate;

    private Date gmtModified;

    /**
     * 关联的sku列表
     */
    private Lazy<List<OrderItemSku>> itemSkuListLazy;

    /**
     * 添加扩展属性
     *
     * @param key
     * @param value
     */
    @JSONField(serialize = false)
    public void addAttributes(String key, String value) {
        if (StringUtils.isBlank(key)) {
            return;
        }

        if (this.getAttributes() == null) {
            this.setAttributes(Maps.newHashMap());
        }

        this.getAttributes().put(key, value);
    }
}
package com.resort.trade.domain.status;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author issavior
 */
@Getter
public enum FundStatus {

    FUND_ORDER_INIT(1, "资金单初始化"),
    PAY_SUCCESS(2, "支付成功"),
    PAY_FAIL(3, "支付失败"),

    UNKNOWN(-1,"未知")
    ;

    private final Integer code;
    private final String desc;

    FundStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static FundStatus of(Integer code) {
        return Arrays.stream(FundStatus.values())
                .filter(e -> Objects.equals(e.getCode(), code))
                .findFirst()
                .orElse(UNKNOWN);
    }
}

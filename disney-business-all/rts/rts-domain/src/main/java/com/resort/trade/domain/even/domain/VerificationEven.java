package com.resort.trade.domain.even.domain;

import com.disney.domain.sdk.even.DomainEven;
import com.resort.trade.domain.model.Order;
import lombok.Builder;
import lombok.Data;

/**
 * @author issavior
 */
@Data
@Builder
public class VerificationEven implements DomainEven {
    private Order order;
}

package com.resort.trade.domain.mq;

import com.disney.domain.sdk.mq.BaseConsumer;
import com.disney.domain.sdk.mq.DomainMessage;
import com.resort.trade.domain.model.Order;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;

/**
 * todo @RocketMQMessageListener注解待重构 -> 优化思路：增加tag
 *
 * @author issavior
 */
@RocketMQMessageListener(consumerGroup = "GROUP_TEST", topic = "TOPIC_TEST")
public class PayTimeOutMQListener implements BaseConsumer<Order> {

    @Override
    public void onMessage(DomainMessage<Order> orderDomainMessage) {
        // todo 取消订单消息
    }
}

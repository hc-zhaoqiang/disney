package com.resort.trade.domain.even.domain.listener;

import com.disney.domain.sdk.anno.DomainEvenListener;
import com.resort.trade.domain.even.domain.RefundEven;
import com.resort.trade.domain.model.Order;
import com.resort.trade.domain.model.RefundOrder;
import com.resort.trade.domain.status.OrderStatus;
import com.resort.trade.domain.status.PerformanceStatus;
import com.resort.trade.domain.status.RefundStatus;
import org.springframework.context.event.EventListener;

/**
 * @author issavior
 */
@DomainEvenListener
public class RefundEvenListener {

    @EventListener
    public void eventListener(RefundEven even) {
        Order order = even.getOrder();
        RefundOrder refundOrder = even.getRefundOrder();

        order.setOrderStatus(OrderStatus.ORDER_TRADE_CLOSE);
        order.setPerformanceStatus(PerformanceStatus.VERIFIED);

        // todo 退款状态需另外处理
        refundOrder.setRefundStatus(RefundStatus.REFUND_SUCCESS);
    }
}

package com.resort.trade.domain.status;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author issavior
 */
@Getter
public enum PayStatus {

    TO_BE_PAID(1, "待支付"),
    PAID(2, "已支付"),

    UNKNOWN(-1,"未知")
    ;

    private final Integer code;
    private final String desc;

    PayStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static PayStatus of(Integer code) {
        return Arrays.stream(PayStatus.values())
                .filter(e -> Objects.equals(e.getCode(), code))
                .findFirst()
                .orElse(UNKNOWN);
    }
}

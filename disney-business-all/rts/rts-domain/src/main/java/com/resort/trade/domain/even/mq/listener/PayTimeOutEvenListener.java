package com.resort.trade.domain.even.mq.listener;

import com.disney.domain.sdk.mq.BaseProducer;
import com.disney.domain.sdk.anno.DomainEvenListener;
import com.disney.domain.sdk.anno.MQProducer;
import com.disney.domain.sdk.enums.MQContent;
import com.disney.domain.sdk.mq.DomainMessage;
import com.resort.trade.domain.even.mq.PayTimeOutEven;
import com.resort.trade.domain.model.Order;
import org.springframework.context.event.EventListener;

/**
 *
 *
 * @author issavior
 */
@DomainEvenListener
@MQProducer(topic = MQContent.Topic.UNKNOWN, tag = MQContent.Tag.UNKNOWN)
public class PayTimeOutEvenListener extends BaseProducer {

    @EventListener
    public void eventListener(PayTimeOutEven<Order> even) {
        DomainMessage<Order> domainMessage = even.getDomainMessage();
        this.syncSend(domainMessage);
    }
}

package com.resort.trade.domain.model;

import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.trade.domain.status.RefundStatus;
import lombok.Data;

import java.util.Date;
import java.util.Map;

/**
 * 退款单模型
 * @author issavior
 */
@Data
public class RefundOrder {

    /**
     * 退款单表主键
     */
    private Long refundOrderId;

    /**
     * 退款单创建时间
     */
    private Date createdTime;

    /**
     * 退款单修改时间
     */
    private Date updatedTime;

    /**
     * 销售单id
     */
    private Long orderId;

    /**
     * 业务类型
     */
    private DomainBizType bizCode;

    /**
     * 退款状态
     */
    private RefundStatus refundStatus;

    /**
     * 单价
     */
    private Long price;

    /**
     * 退款数量
     */
    private Long amount;

    /**
     * 总退款金额
     */
    private Long totalRefundFee;

    /**
     * 总退款服务费
     */
    private Long totalServiceFee;

    /**
     * 退款单号
     */
    private Long itemId;

    /**
     * 商品名称
     */
    private String itemName;
    /**
     * 退货时间
     */
    private Date refundGoodsTime;

    /**
     * 退款时间
     */
    private Date refundFundTime;

    /**
     * 退款原因
     */
    private String refundReason;

    /**
     * 退款说明
     */
    private String refundDesc;

    /**
     * 退款规则
     */
    private String refundRule;

    /**
     * 版本号
     */
    private Integer version;

    /**
     * 扩展字段
     */
    private Map<String, String> attributes;

    /**
     * 退款单是否已完结
     *
     * @return 结果
     */
    public boolean isRefundOrderEnd() {
        return this.refundStatus == RefundStatus.REFUND_FAIL || this.refundStatus == RefundStatus.REFUND_SUCCESS;
    }

}
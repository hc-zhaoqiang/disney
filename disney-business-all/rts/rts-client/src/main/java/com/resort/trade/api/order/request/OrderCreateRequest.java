package com.resort.trade.api.order.request;

import com.resort.goods.api.rpc.response.GoodsSubSubDTO;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder(builderMethodName = "to",buildMethodName = "end")
public class OrderCreateRequest implements Serializable {

    private String bizCode;

    private GoodsSubSubDTO goodsSubSubDTO;
}

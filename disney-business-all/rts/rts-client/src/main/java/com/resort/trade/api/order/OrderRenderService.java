package com.resort.trade.api.order;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.trade.api.order.request.OrderFillingRenderRequest;
import com.resort.trade.api.order.request.OrderSelectionRenderRequest;
import com.resort.trade.api.order.response.OrderFillingRenderDTO;
import com.resort.trade.api.order.response.OrderSelectionRenderDTO;


/**
 * @author issavior
 */
public interface OrderRenderService {
    DisneyResult<OrderFillingRenderDTO> fillingRender(OrderFillingRenderRequest request);

    DisneyResult<OrderSelectionRenderDTO> selectionRender(OrderSelectionRenderRequest request);
}

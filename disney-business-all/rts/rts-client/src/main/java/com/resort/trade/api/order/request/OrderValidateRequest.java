package com.resort.trade.api.order.request;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder(builderMethodName = "to",buildMethodName = "end")
public class OrderValidateRequest implements Serializable {

    private String bizCode;
}

package com.resort.trade.api.order.request;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder
public class OrderFillingRenderRequest implements Serializable {

    private String bizCode;
}

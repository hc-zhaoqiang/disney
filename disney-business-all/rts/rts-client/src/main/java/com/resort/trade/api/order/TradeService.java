package com.resort.trade.api.order;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.trade.api.order.request.OrderCancelRequest;
import com.resort.trade.api.order.request.OrderCreateRequest;
import com.resort.trade.api.order.request.OrderPaymentRequest;
import com.resort.trade.api.order.request.OrderValidateRequest;
import com.resort.trade.api.order.response.OrderCancelDTO;
import com.resort.trade.api.order.response.OrderCreateDTO;
import com.resort.trade.api.order.response.OrderPaymentDTO;
import com.resort.trade.api.order.response.OrderValidateDTO;

import java.util.Map;

/**
 * @author issavior
 */
public interface TradeService {

    DisneyResult<OrderValidateDTO> orderValidate(OrderValidateRequest orderValidateRequest);

    DisneyResult<OrderCreateDTO> orderCreate(OrderCreateRequest orderCreateRequest);

    DisneyResult<OrderPaymentDTO> orderPayment(OrderPaymentRequest orderPaymentRequest);

    DisneyResult<Void> orderPayCallBack(Map<String, String> params);

    DisneyResult<OrderCancelDTO> orderCancel(OrderCancelRequest orderPaymentRequest);


}

package com.resort.trade.api.payment.request;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder(builderMethodName = "to", buildMethodName = "end")
public class PaymentRenderRequest implements Serializable {

    private String bizCode;
}

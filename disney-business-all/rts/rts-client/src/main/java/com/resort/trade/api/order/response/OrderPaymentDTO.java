package com.resort.trade.api.order.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder
public class OrderPaymentDTO implements Serializable {

    private String tradeNo;
}

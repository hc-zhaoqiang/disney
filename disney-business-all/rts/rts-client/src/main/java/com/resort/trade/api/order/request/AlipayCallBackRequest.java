package com.resort.trade.api.order.request;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

/**
 * @author issavior
 */
@Data
@Builder
public class AlipayCallBackRequest {

    private Date notifyTime;

    private String tradeNo;

    private String outTradeNo;

    private String tradeStatus;

    private String body;
}

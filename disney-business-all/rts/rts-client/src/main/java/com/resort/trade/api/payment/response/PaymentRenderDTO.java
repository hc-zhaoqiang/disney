package com.resort.trade.api.payment.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder(builderMethodName = "to",buildMethodName = "end")
public class PaymentRenderDTO implements Serializable {
}

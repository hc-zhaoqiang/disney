package com.resort.trade.api.order.request;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder(builderMethodName = "to",buildMethodName = "end")
public class OrderCancelRequest implements Serializable {

    private String bizCode;
    private Long orderId;
}

package com.resort.trade.api.payment;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.trade.api.payment.request.PaymentRenderRequest;
import com.resort.trade.api.payment.response.PaymentRenderDTO;

/**
 * @author issavior
 */
public interface PaymentRenderService {

    public DisneyResult<PaymentRenderDTO> paymentRender(PaymentRenderRequest paymentRenderRequest);
}

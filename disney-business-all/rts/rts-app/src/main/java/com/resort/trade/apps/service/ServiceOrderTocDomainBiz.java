package com.resort.trade.apps.service;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.trade.apps.OrderTocDomainBiz;

/**
 * @author issavior
 */
@Biz(DomainBizType.SERVICE)
public class ServiceOrderTocDomainBiz extends OrderTocDomainBiz {


    @Override
    public Integer orderPayExpireMin() {
        return null;
    }
}

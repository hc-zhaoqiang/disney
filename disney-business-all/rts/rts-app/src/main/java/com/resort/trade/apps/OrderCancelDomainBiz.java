package com.resort.trade.apps;

import com.disney.domain.sdk.anno.AbsBiz;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.IDomainBiz;
import com.resort.trade.domain.model.FundOrder;

import java.util.Optional;


/**
 * @author issavior
 */
@AbsBiz(value = DomainAbsBizType.TRADE)
public abstract class OrderCancelDomainBiz implements IDomainBiz {

    public Boolean fundHandler(FundOrder fundOrder, Object dubbo) {

        // 资金单或者支付失败，直接return
        if (Optional.ofNullable(fundOrder).map(FundOrder::isFailEnd).orElse(true)) {
            return Boolean.TRUE;
        }
        // todo 已经支付就走退款
        if (fundOrder.isSuccessEnd()) {
            return Boolean.TRUE;
        }
        // todo 未支付，就调支付宝接口关闭交易，修改资金单状态
        // todo 关闭交易失败，就调撤销交易接口，修改资金单状态

        return Boolean.TRUE;

    }
}

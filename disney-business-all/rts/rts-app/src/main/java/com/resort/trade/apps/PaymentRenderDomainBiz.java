package com.resort.trade.apps;

import com.disney.domain.sdk.anno.AbsBiz;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.IDomainBiz;
import com.resort.trade.api.payment.request.PaymentRenderRequest;
import com.resort.trade.api.payment.response.PaymentRenderDTO;


/**
 * @author issavior
 */
@AbsBiz(value = DomainAbsBizType.TRADE)
public abstract class PaymentRenderDomainBiz implements IDomainBiz {

    public abstract PaymentRenderDTO paymentRender(PaymentRenderRequest paymentRenderRequest);
}

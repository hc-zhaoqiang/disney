package com.resort.trade.apps;

import com.disney.domain.sdk.anno.AbsBiz;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.IDomainBiz;


/**
 * @author issavior
 */
@AbsBiz(value = DomainAbsBizType.TRADE)
public abstract class OrderTocDomainBiz implements IDomainBiz {

   public abstract Integer orderPayExpireMin();

}

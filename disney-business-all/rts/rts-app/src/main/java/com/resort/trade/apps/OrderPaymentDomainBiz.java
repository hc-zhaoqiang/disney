package com.resort.trade.apps;

import com.disney.domain.sdk.anno.AbsBiz;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.IDomainBiz;
import com.resort.trade.domain.model.FundOrder;
import com.resort.trade.domain.model.Order;

import java.util.Date;


/**
 * @author issavior
 */
@AbsBiz(value = DomainAbsBizType.TRADE)
public abstract class OrderPaymentDomainBiz implements IDomainBiz {

    public FundOrder buildFundOrder(Order order,String tradeNo){
        FundOrder fundOrder = new FundOrder();
        fundOrder.setGmtCreate(new Date());
        fundOrder.setGmtModified(new Date());

        return fundOrder;
    }

}

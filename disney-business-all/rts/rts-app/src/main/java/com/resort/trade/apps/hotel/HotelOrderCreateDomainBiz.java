package com.resort.trade.apps.hotel;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.rpc.response.GoodsSubSubDTO;
import com.resort.goods.api.rpc.response.GoodsSubSubSubDTO;
import com.resort.trade.api.order.request.OrderCreateRequest;
import com.resort.trade.api.order.request.OrderRequest;
import com.resort.trade.domain.model.Order;
import com.resort.trade.domain.model.OrderItem;
import com.resort.trade.domain.model.OrderItemSku;
import com.resort.trade.apps.OrderCreateDomainBiz;

/**
 * @author issavior
 */
@Biz(DomainBizType.HOTEL)
public class HotelOrderCreateDomainBiz extends OrderCreateDomainBiz {


    @Override
    public Boolean checkOrder(OrderRequest orderRequest) {
        return null;
    }

    @Override
    public OrderRequest buildOrderCreateRequest(OrderCreateRequest orderCreateRequest) {
        return null;
    }

    @Override
    public void buildOrderCreateRequestCheck(OrderRequest orderRequest) {

    }

    @Override
    public Order buildOrder(OrderRequest orderRequest) {
        return null;
    }

    @Override
    public OrderItem buildOrderItem(GoodsSubSubDTO goodsSubSubSubDTO) {
        return null;
    }

    @Override
    public OrderItemSku buildOrderItemSku(GoodsSubSubSubDTO goodsSubSubSubDTO) {
        return null;
    }

    @Override
    public Boolean minusStock(OrderRequest orderRequest) {
        return null;
    }

    @Override
    public Boolean addStock(OrderRequest orderRequest) {
        return null;
    }
}

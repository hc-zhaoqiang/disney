package com.resort.trade.service.convert;

import com.resort.trade.api.order.request.OrderCreateRequest;
import com.resort.trade.api.order.request.OrderValidateRequest;

/**
 * @author issavior
 */
public class OrderRequestConvert {
    public static OrderValidateRequest toOrderValidateRequest(OrderCreateRequest orderCreateRequest) {
        return OrderValidateRequest.to()
                .bizCode(orderCreateRequest.getBizCode())
                .end();
    }
}

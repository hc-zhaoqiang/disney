package com.resort.trade.apps;

import com.disney.domain.sdk.anno.AbsBiz;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.IDomainBiz;
import com.resort.trade.api.order.request.OrderRequest;
import com.resort.trade.api.order.request.OrderValidateRequest;


/**
 * @author issavior
 */
@AbsBiz(value = DomainAbsBizType.TRADE)
public abstract class OrderValidateDomainBiz implements IDomainBiz {

    public abstract OrderRequest buildOrderValidateRequest(OrderValidateRequest orderValidateRequest);

    public abstract Boolean orderValidateRequestCheck(OrderRequest orderRequest);

    public abstract Boolean checkStock(OrderRequest orderRequest);

}

package com.resort.trade.service;

import com.disney.domain.sdk.common.DisneyResult;
import com.disney.domain.sdk.enums.DomainBizType;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.DomainExecutor;
import com.disney.domain.sdk.factory.DomainRequest;
import com.resort.trade.api.order.OrderRenderService;
import com.resort.trade.api.order.request.OrderFillingRenderRequest;
import com.resort.trade.api.order.request.OrderSelectionRenderRequest;
import com.resort.trade.api.order.response.OrderFillingRenderDTO;
import com.resort.trade.api.order.response.OrderSelectionRenderDTO;
import com.resort.trade.apps.OrderRenderDomainBiz;
import org.springframework.stereotype.Service;

/**
 * @author issavior
 */
@Service
public class OrderRenderServiceImpl implements OrderRenderService {

    private static final DomainExecutor<OrderRenderDomainBiz> EXECUTOR = new DomainExecutor<>(OrderRenderDomainBiz.class);

    @Override
    public DisneyResult<OrderFillingRenderDTO> fillingRender(OrderFillingRenderRequest request) {
        return DisneyResult.ok(EXECUTOR.execFirst(DomainRequest.to()
                        .code(DomainBizType.of(request.getBizCode()))
                        .scenario(DomainAbsBizType.TRADE)
                        .end(),
                biz -> biz.fillingRender(request)));
    }

    @Override
    public DisneyResult<OrderSelectionRenderDTO> selectionRender(OrderSelectionRenderRequest request) {
        return DisneyResult.ok(EXECUTOR.execFirst(DomainRequest.to()
                        .code(DomainBizType.of(request.getBizCode()))
                        .scenario(DomainAbsBizType.TRADE)
                        .end(),
                biz -> biz.selectionRender(request)));
    }
}

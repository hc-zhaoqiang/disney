package com.resort.trade.apps.pass;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.trade.api.order.request.OrderRequest;
import com.resort.trade.api.order.request.OrderValidateRequest;
import com.resort.trade.apps.OrderValidateDomainBiz;

/**
 * @author issavior
 */
@Biz(DomainBizType.ANNUAL_PASS)
public class AnnualPassOrderValidateDomainBiz extends OrderValidateDomainBiz {


    @Override
    public OrderRequest buildOrderValidateRequest(OrderValidateRequest orderValidateRequest) {
        return null;
    }

    @Override
    public Boolean orderValidateRequestCheck(OrderRequest orderRequest) {

        return null;
    }

    @Override
    public Boolean checkStock(OrderRequest orderRequest) {
        return null;
    }

}

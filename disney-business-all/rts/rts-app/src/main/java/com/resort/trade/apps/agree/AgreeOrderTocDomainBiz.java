package com.resort.trade.apps.agree;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.trade.apps.OrderTocDomainBiz;

/**
 * @author issavior
 */
@Biz(DomainBizType.AGREE)
public class AgreeOrderTocDomainBiz extends OrderTocDomainBiz {


    @Override
    public Integer orderPayExpireMin() {
        return null;
    }
}

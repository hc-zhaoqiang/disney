package com.resort.trade.apps.pass;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.trade.apps.OrderTocDomainBiz;

/**
 * @author issavior
 */
@Biz(DomainBizType.TICKET)
public class AnnualPassOrderTocDomainBiz extends OrderTocDomainBiz {


    @Override
    public Integer orderPayExpireMin() {
        return null;
    }
}

package com.resort.trade.apps.hotel;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.trade.api.order.request.OrderFillingRenderRequest;
import com.resort.trade.api.order.request.OrderSelectionRenderRequest;
import com.resort.trade.api.order.response.OrderFillingRenderDTO;
import com.resort.trade.api.order.response.OrderSelectionRenderDTO;
import com.resort.trade.apps.OrderRenderDomainBiz;

/**
 * @author issavior
 */
@Biz(DomainBizType.HOTEL)
public class HotelOrderRenderDomainBiz extends OrderRenderDomainBiz {

    @Override
    public OrderFillingRenderDTO fillingRender(OrderFillingRenderRequest request) {
        return null;
    }

    @Override
    public OrderSelectionRenderDTO selectionRender(OrderSelectionRenderRequest request) {
        return null;
    }

}

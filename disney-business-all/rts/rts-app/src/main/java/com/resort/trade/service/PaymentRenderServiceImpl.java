package com.resort.trade.service;

import com.disney.domain.sdk.common.DisneyResult;
import com.disney.domain.sdk.enums.DomainBizType;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.DomainExecutor;
import com.disney.domain.sdk.factory.DomainRequest;
import com.resort.trade.api.payment.PaymentRenderService;
import com.resort.trade.api.payment.request.PaymentRenderRequest;
import com.resort.trade.api.payment.response.PaymentRenderDTO;
import com.resort.trade.apps.PaymentRenderDomainBiz;
import org.springframework.stereotype.Service;

/**
 * @author issavior
 */
@Service
public class PaymentRenderServiceImpl implements PaymentRenderService {

    private static final DomainExecutor<PaymentRenderDomainBiz> EXECUTOR = new DomainExecutor<>(PaymentRenderDomainBiz.class);

    @Override
    public DisneyResult<PaymentRenderDTO> paymentRender(PaymentRenderRequest request) {
        return DisneyResult.ok(EXECUTOR.execFirst(DomainRequest.to()
                        .code(DomainBizType.of(request.getBizCode()))
                        .scenario(DomainAbsBizType.TRADE)
                        .end(),
                biz -> biz.paymentRender(request)));
    }
}

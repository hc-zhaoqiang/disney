package com.resort.trade.apps;

import com.disney.domain.sdk.anno.AbsBiz;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.IDomainBiz;
import com.disney.domain.sdk.publish.DomainPublisher;
import com.resort.goods.api.rpc.response.GoodsSubSubDTO;
import com.resort.goods.api.rpc.response.GoodsSubSubSubDTO;
import com.resort.trade.api.order.request.OrderCreateRequest;
import com.resort.trade.api.order.request.OrderRequest;
import com.resort.trade.domain.even.domain.CreateOrderEven;
import com.resort.trade.domain.model.Order;
import com.resort.trade.domain.model.OrderItem;
import com.resort.trade.domain.model.OrderItemSku;


/**
 * @author issavior
 */
@AbsBiz(value = DomainAbsBizType.TRADE)
public abstract class OrderCreateDomainBiz implements IDomainBiz {

    public abstract Boolean checkOrder(OrderRequest orderRequest);

    public abstract OrderRequest buildOrderCreateRequest(OrderCreateRequest orderCreateRequest);

    public abstract void buildOrderCreateRequestCheck(OrderRequest orderRequest);

    public abstract Order buildOrder(OrderRequest orderRequest);

    public abstract OrderItem buildOrderItem(GoodsSubSubDTO goodsSubSubSubDTO);

    public abstract OrderItemSku buildOrderItemSku(GoodsSubSubSubDTO goodsSubSubSubDTO);

    public abstract Boolean minusStock(OrderRequest orderRequest) ;
    public abstract Boolean addStock(OrderRequest orderRequest) ;

    public void evenDriver(Order order) {
        DomainPublisher.publish(CreateOrderEven.builder().order(order).build());
    }
}

package com.resort.trade.apps;

import com.disney.domain.sdk.anno.AbsBiz;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.IDomainBiz;
import com.resort.trade.api.order.request.OrderFillingRenderRequest;
import com.resort.trade.api.order.request.OrderSelectionRenderRequest;
import com.resort.trade.api.order.response.OrderFillingRenderDTO;
import com.resort.trade.api.order.response.OrderSelectionRenderDTO;


/**
 * @author issavior
 */
@AbsBiz(value = DomainAbsBizType.TRADE)
public abstract class OrderRenderDomainBiz implements IDomainBiz {

    public abstract OrderFillingRenderDTO fillingRender(OrderFillingRenderRequest request);

    public abstract OrderSelectionRenderDTO selectionRender(OrderSelectionRenderRequest request);


}

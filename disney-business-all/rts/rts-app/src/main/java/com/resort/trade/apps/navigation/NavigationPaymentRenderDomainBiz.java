package com.resort.trade.apps.navigation;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.trade.api.payment.request.PaymentRenderRequest;
import com.resort.trade.api.payment.response.PaymentRenderDTO;
import com.resort.trade.apps.PaymentRenderDomainBiz;

/**
 * @author issavior
 */
@Biz(DomainBizType.TICKET)
public class NavigationPaymentRenderDomainBiz extends PaymentRenderDomainBiz {


    @Override
    public PaymentRenderDTO paymentRender(PaymentRenderRequest paymentRenderRequest) {
        return null;
    }
}

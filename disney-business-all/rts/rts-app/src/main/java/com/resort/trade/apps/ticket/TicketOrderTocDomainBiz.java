package com.resort.trade.apps.ticket;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.trade.apps.OrderTocDomainBiz;

/**
 * @author issavior
 */
@Biz(DomainBizType.TICKET)
public class TicketOrderTocDomainBiz extends OrderTocDomainBiz {


    @Override
    public Integer orderPayExpireMin() {
        return null;
    }
}

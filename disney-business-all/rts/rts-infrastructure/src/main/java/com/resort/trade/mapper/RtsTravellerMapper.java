package com.resort.trade.mapper;

import com.resort.trade.pojo.RtsTraveller;
import com.resort.trade.pojo.RtsTravellerExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RtsTravellerMapper {
    long countByExample(RtsTravellerExample example);

    int deleteByExample(RtsTravellerExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RtsTraveller record);

    int insertSelective(RtsTraveller record);

    List<RtsTraveller> selectByExample(RtsTravellerExample example);

    RtsTraveller selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RtsTraveller record, @Param("example") RtsTravellerExample example);

    int updateByExample(@Param("record") RtsTraveller record, @Param("example") RtsTravellerExample example);

    int updateByPrimaryKeySelective(RtsTraveller record);

    int updateByPrimaryKey(RtsTraveller record);
}
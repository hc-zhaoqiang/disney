package com.resort.trade.gateway;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.trade.domain.gateway.RefundOrderGateway;

/**
 * @author issavior
 */
@DomainGateway
public class RefundOrderGatewayImpl implements RefundOrderGateway {
}

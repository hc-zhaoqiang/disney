package com.resort.trade.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RtsOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RtsOrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNull() {
            addCriterion("created_time is null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNotNull() {
            addCriterion("created_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeEqualTo(Date value) {
            addCriterion("created_time =", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotEqualTo(Date value) {
            addCriterion("created_time <>", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThan(Date value) {
            addCriterion("created_time >", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("created_time >=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThan(Date value) {
            addCriterion("created_time <", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("created_time <=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIn(List<Date> values) {
            addCriterion("created_time in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotIn(List<Date> values) {
            addCriterion("created_time not in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeBetween(Date value1, Date value2) {
            addCriterion("created_time between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("created_time not between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIsNull() {
            addCriterion("updated_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIsNotNull() {
            addCriterion("updated_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeEqualTo(Date value) {
            addCriterion("updated_time =", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotEqualTo(Date value) {
            addCriterion("updated_time <>", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeGreaterThan(Date value) {
            addCriterion("updated_time >", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("updated_time >=", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeLessThan(Date value) {
            addCriterion("updated_time <", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("updated_time <=", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIn(List<Date> values) {
            addCriterion("updated_time in", values, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotIn(List<Date> values) {
            addCriterion("updated_time not in", values, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeBetween(Date value1, Date value2) {
            addCriterion("updated_time between", value1, value2, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("updated_time not between", value1, value2, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andBizCodeIsNull() {
            addCriterion("biz_code is null");
            return (Criteria) this;
        }

        public Criteria andBizCodeIsNotNull() {
            addCriterion("biz_code is not null");
            return (Criteria) this;
        }

        public Criteria andBizCodeEqualTo(String value) {
            addCriterion("biz_code =", value, "bizCode");
            return (Criteria) this;
        }

        public Criteria andBizCodeNotEqualTo(String value) {
            addCriterion("biz_code <>", value, "bizCode");
            return (Criteria) this;
        }

        public Criteria andBizCodeGreaterThan(String value) {
            addCriterion("biz_code >", value, "bizCode");
            return (Criteria) this;
        }

        public Criteria andBizCodeGreaterThanOrEqualTo(String value) {
            addCriterion("biz_code >=", value, "bizCode");
            return (Criteria) this;
        }

        public Criteria andBizCodeLessThan(String value) {
            addCriterion("biz_code <", value, "bizCode");
            return (Criteria) this;
        }

        public Criteria andBizCodeLessThanOrEqualTo(String value) {
            addCriterion("biz_code <=", value, "bizCode");
            return (Criteria) this;
        }

        public Criteria andBizCodeLike(String value) {
            addCriterion("biz_code like", value, "bizCode");
            return (Criteria) this;
        }

        public Criteria andBizCodeNotLike(String value) {
            addCriterion("biz_code not like", value, "bizCode");
            return (Criteria) this;
        }

        public Criteria andBizCodeIn(List<String> values) {
            addCriterion("biz_code in", values, "bizCode");
            return (Criteria) this;
        }

        public Criteria andBizCodeNotIn(List<String> values) {
            addCriterion("biz_code not in", values, "bizCode");
            return (Criteria) this;
        }

        public Criteria andBizCodeBetween(String value1, String value2) {
            addCriterion("biz_code between", value1, value2, "bizCode");
            return (Criteria) this;
        }

        public Criteria andBizCodeNotBetween(String value1, String value2) {
            addCriterion("biz_code not between", value1, value2, "bizCode");
            return (Criteria) this;
        }

        public Criteria andOrderStatusIsNull() {
            addCriterion("order_status is null");
            return (Criteria) this;
        }

        public Criteria andOrderStatusIsNotNull() {
            addCriterion("order_status is not null");
            return (Criteria) this;
        }

        public Criteria andOrderStatusEqualTo(Integer value) {
            addCriterion("order_status =", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusNotEqualTo(Integer value) {
            addCriterion("order_status <>", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusGreaterThan(Integer value) {
            addCriterion("order_status >", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("order_status >=", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusLessThan(Integer value) {
            addCriterion("order_status <", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusLessThanOrEqualTo(Integer value) {
            addCriterion("order_status <=", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusIn(List<Integer> values) {
            addCriterion("order_status in", values, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusNotIn(List<Integer> values) {
            addCriterion("order_status not in", values, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusBetween(Integer value1, Integer value2) {
            addCriterion("order_status between", value1, value2, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("order_status not between", value1, value2, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andPayTypeIsNull() {
            addCriterion("pay_type is null");
            return (Criteria) this;
        }

        public Criteria andPayTypeIsNotNull() {
            addCriterion("pay_type is not null");
            return (Criteria) this;
        }

        public Criteria andPayTypeEqualTo(Integer value) {
            addCriterion("pay_type =", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotEqualTo(Integer value) {
            addCriterion("pay_type <>", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeGreaterThan(Integer value) {
            addCriterion("pay_type >", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("pay_type >=", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeLessThan(Integer value) {
            addCriterion("pay_type <", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeLessThanOrEqualTo(Integer value) {
            addCriterion("pay_type <=", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeIn(List<Integer> values) {
            addCriterion("pay_type in", values, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotIn(List<Integer> values) {
            addCriterion("pay_type not in", values, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeBetween(Integer value1, Integer value2) {
            addCriterion("pay_type between", value1, value2, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("pay_type not between", value1, value2, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTimeIsNull() {
            addCriterion("pay_time is null");
            return (Criteria) this;
        }

        public Criteria andPayTimeIsNotNull() {
            addCriterion("pay_time is not null");
            return (Criteria) this;
        }

        public Criteria andPayTimeEqualTo(Date value) {
            addCriterion("pay_time =", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotEqualTo(Date value) {
            addCriterion("pay_time <>", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThan(Date value) {
            addCriterion("pay_time >", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("pay_time >=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThan(Date value) {
            addCriterion("pay_time <", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThanOrEqualTo(Date value) {
            addCriterion("pay_time <=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeIn(List<Date> values) {
            addCriterion("pay_time in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotIn(List<Date> values) {
            addCriterion("pay_time not in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeBetween(Date value1, Date value2) {
            addCriterion("pay_time between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotBetween(Date value1, Date value2) {
            addCriterion("pay_time not between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayStatusIsNull() {
            addCriterion("pay_status is null");
            return (Criteria) this;
        }

        public Criteria andPayStatusIsNotNull() {
            addCriterion("pay_status is not null");
            return (Criteria) this;
        }

        public Criteria andPayStatusEqualTo(Integer value) {
            addCriterion("pay_status =", value, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusNotEqualTo(Integer value) {
            addCriterion("pay_status <>", value, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusGreaterThan(Integer value) {
            addCriterion("pay_status >", value, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("pay_status >=", value, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusLessThan(Integer value) {
            addCriterion("pay_status <", value, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusLessThanOrEqualTo(Integer value) {
            addCriterion("pay_status <=", value, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusIn(List<Integer> values) {
            addCriterion("pay_status in", values, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusNotIn(List<Integer> values) {
            addCriterion("pay_status not in", values, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusBetween(Integer value1, Integer value2) {
            addCriterion("pay_status between", value1, value2, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("pay_status not between", value1, value2, "payStatus");
            return (Criteria) this;
        }

        public Criteria andPayCurrencyTypeIsNull() {
            addCriterion("pay_currency_type is null");
            return (Criteria) this;
        }

        public Criteria andPayCurrencyTypeIsNotNull() {
            addCriterion("pay_currency_type is not null");
            return (Criteria) this;
        }

        public Criteria andPayCurrencyTypeEqualTo(Integer value) {
            addCriterion("pay_currency_type =", value, "payCurrencyType");
            return (Criteria) this;
        }

        public Criteria andPayCurrencyTypeNotEqualTo(Integer value) {
            addCriterion("pay_currency_type <>", value, "payCurrencyType");
            return (Criteria) this;
        }

        public Criteria andPayCurrencyTypeGreaterThan(Integer value) {
            addCriterion("pay_currency_type >", value, "payCurrencyType");
            return (Criteria) this;
        }

        public Criteria andPayCurrencyTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("pay_currency_type >=", value, "payCurrencyType");
            return (Criteria) this;
        }

        public Criteria andPayCurrencyTypeLessThan(Integer value) {
            addCriterion("pay_currency_type <", value, "payCurrencyType");
            return (Criteria) this;
        }

        public Criteria andPayCurrencyTypeLessThanOrEqualTo(Integer value) {
            addCriterion("pay_currency_type <=", value, "payCurrencyType");
            return (Criteria) this;
        }

        public Criteria andPayCurrencyTypeIn(List<Integer> values) {
            addCriterion("pay_currency_type in", values, "payCurrencyType");
            return (Criteria) this;
        }

        public Criteria andPayCurrencyTypeNotIn(List<Integer> values) {
            addCriterion("pay_currency_type not in", values, "payCurrencyType");
            return (Criteria) this;
        }

        public Criteria andPayCurrencyTypeBetween(Integer value1, Integer value2) {
            addCriterion("pay_currency_type between", value1, value2, "payCurrencyType");
            return (Criteria) this;
        }

        public Criteria andPayCurrencyTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("pay_currency_type not between", value1, value2, "payCurrencyType");
            return (Criteria) this;
        }

        public Criteria andCloseTypeIsNull() {
            addCriterion("close_type is null");
            return (Criteria) this;
        }

        public Criteria andCloseTypeIsNotNull() {
            addCriterion("close_type is not null");
            return (Criteria) this;
        }

        public Criteria andCloseTypeEqualTo(Integer value) {
            addCriterion("close_type =", value, "closeType");
            return (Criteria) this;
        }

        public Criteria andCloseTypeNotEqualTo(Integer value) {
            addCriterion("close_type <>", value, "closeType");
            return (Criteria) this;
        }

        public Criteria andCloseTypeGreaterThan(Integer value) {
            addCriterion("close_type >", value, "closeType");
            return (Criteria) this;
        }

        public Criteria andCloseTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("close_type >=", value, "closeType");
            return (Criteria) this;
        }

        public Criteria andCloseTypeLessThan(Integer value) {
            addCriterion("close_type <", value, "closeType");
            return (Criteria) this;
        }

        public Criteria andCloseTypeLessThanOrEqualTo(Integer value) {
            addCriterion("close_type <=", value, "closeType");
            return (Criteria) this;
        }

        public Criteria andCloseTypeIn(List<Integer> values) {
            addCriterion("close_type in", values, "closeType");
            return (Criteria) this;
        }

        public Criteria andCloseTypeNotIn(List<Integer> values) {
            addCriterion("close_type not in", values, "closeType");
            return (Criteria) this;
        }

        public Criteria andCloseTypeBetween(Integer value1, Integer value2) {
            addCriterion("close_type between", value1, value2, "closeType");
            return (Criteria) this;
        }

        public Criteria andCloseTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("close_type not between", value1, value2, "closeType");
            return (Criteria) this;
        }

        public Criteria andCloseDescIsNull() {
            addCriterion("close_desc is null");
            return (Criteria) this;
        }

        public Criteria andCloseDescIsNotNull() {
            addCriterion("close_desc is not null");
            return (Criteria) this;
        }

        public Criteria andCloseDescEqualTo(String value) {
            addCriterion("close_desc =", value, "closeDesc");
            return (Criteria) this;
        }

        public Criteria andCloseDescNotEqualTo(String value) {
            addCriterion("close_desc <>", value, "closeDesc");
            return (Criteria) this;
        }

        public Criteria andCloseDescGreaterThan(String value) {
            addCriterion("close_desc >", value, "closeDesc");
            return (Criteria) this;
        }

        public Criteria andCloseDescGreaterThanOrEqualTo(String value) {
            addCriterion("close_desc >=", value, "closeDesc");
            return (Criteria) this;
        }

        public Criteria andCloseDescLessThan(String value) {
            addCriterion("close_desc <", value, "closeDesc");
            return (Criteria) this;
        }

        public Criteria andCloseDescLessThanOrEqualTo(String value) {
            addCriterion("close_desc <=", value, "closeDesc");
            return (Criteria) this;
        }

        public Criteria andCloseDescLike(String value) {
            addCriterion("close_desc like", value, "closeDesc");
            return (Criteria) this;
        }

        public Criteria andCloseDescNotLike(String value) {
            addCriterion("close_desc not like", value, "closeDesc");
            return (Criteria) this;
        }

        public Criteria andCloseDescIn(List<String> values) {
            addCriterion("close_desc in", values, "closeDesc");
            return (Criteria) this;
        }

        public Criteria andCloseDescNotIn(List<String> values) {
            addCriterion("close_desc not in", values, "closeDesc");
            return (Criteria) this;
        }

        public Criteria andCloseDescBetween(String value1, String value2) {
            addCriterion("close_desc between", value1, value2, "closeDesc");
            return (Criteria) this;
        }

        public Criteria andCloseDescNotBetween(String value1, String value2) {
            addCriterion("close_desc not between", value1, value2, "closeDesc");
            return (Criteria) this;
        }

        public Criteria andCloseTimeIsNull() {
            addCriterion("close_time is null");
            return (Criteria) this;
        }

        public Criteria andCloseTimeIsNotNull() {
            addCriterion("close_time is not null");
            return (Criteria) this;
        }

        public Criteria andCloseTimeEqualTo(Date value) {
            addCriterion("close_time =", value, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeNotEqualTo(Date value) {
            addCriterion("close_time <>", value, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeGreaterThan(Date value) {
            addCriterion("close_time >", value, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("close_time >=", value, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeLessThan(Date value) {
            addCriterion("close_time <", value, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeLessThanOrEqualTo(Date value) {
            addCriterion("close_time <=", value, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeIn(List<Date> values) {
            addCriterion("close_time in", values, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeNotIn(List<Date> values) {
            addCriterion("close_time not in", values, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeBetween(Date value1, Date value2) {
            addCriterion("close_time between", value1, value2, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeNotBetween(Date value1, Date value2) {
            addCriterion("close_time not between", value1, value2, "closeTime");
            return (Criteria) this;
        }

        public Criteria andRefundStatusIsNull() {
            addCriterion("refund_status is null");
            return (Criteria) this;
        }

        public Criteria andRefundStatusIsNotNull() {
            addCriterion("refund_status is not null");
            return (Criteria) this;
        }

        public Criteria andRefundStatusEqualTo(Integer value) {
            addCriterion("refund_status =", value, "refundStatus");
            return (Criteria) this;
        }

        public Criteria andRefundStatusNotEqualTo(Integer value) {
            addCriterion("refund_status <>", value, "refundStatus");
            return (Criteria) this;
        }

        public Criteria andRefundStatusGreaterThan(Integer value) {
            addCriterion("refund_status >", value, "refundStatus");
            return (Criteria) this;
        }

        public Criteria andRefundStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("refund_status >=", value, "refundStatus");
            return (Criteria) this;
        }

        public Criteria andRefundStatusLessThan(Integer value) {
            addCriterion("refund_status <", value, "refundStatus");
            return (Criteria) this;
        }

        public Criteria andRefundStatusLessThanOrEqualTo(Integer value) {
            addCriterion("refund_status <=", value, "refundStatus");
            return (Criteria) this;
        }

        public Criteria andRefundStatusIn(List<Integer> values) {
            addCriterion("refund_status in", values, "refundStatus");
            return (Criteria) this;
        }

        public Criteria andRefundStatusNotIn(List<Integer> values) {
            addCriterion("refund_status not in", values, "refundStatus");
            return (Criteria) this;
        }

        public Criteria andRefundStatusBetween(Integer value1, Integer value2) {
            addCriterion("refund_status between", value1, value2, "refundStatus");
            return (Criteria) this;
        }

        public Criteria andRefundStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("refund_status not between", value1, value2, "refundStatus");
            return (Criteria) this;
        }

        public Criteria andRefundTypeIsNull() {
            addCriterion("refund_type is null");
            return (Criteria) this;
        }

        public Criteria andRefundTypeIsNotNull() {
            addCriterion("refund_type is not null");
            return (Criteria) this;
        }

        public Criteria andRefundTypeEqualTo(Integer value) {
            addCriterion("refund_type =", value, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeNotEqualTo(Integer value) {
            addCriterion("refund_type <>", value, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeGreaterThan(Integer value) {
            addCriterion("refund_type >", value, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("refund_type >=", value, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeLessThan(Integer value) {
            addCriterion("refund_type <", value, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeLessThanOrEqualTo(Integer value) {
            addCriterion("refund_type <=", value, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeIn(List<Integer> values) {
            addCriterion("refund_type in", values, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeNotIn(List<Integer> values) {
            addCriterion("refund_type not in", values, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeBetween(Integer value1, Integer value2) {
            addCriterion("refund_type between", value1, value2, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("refund_type not between", value1, value2, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundFeeIsNull() {
            addCriterion("refund_fee is null");
            return (Criteria) this;
        }

        public Criteria andRefundFeeIsNotNull() {
            addCriterion("refund_fee is not null");
            return (Criteria) this;
        }

        public Criteria andRefundFeeEqualTo(Long value) {
            addCriterion("refund_fee =", value, "refundFee");
            return (Criteria) this;
        }

        public Criteria andRefundFeeNotEqualTo(Long value) {
            addCriterion("refund_fee <>", value, "refundFee");
            return (Criteria) this;
        }

        public Criteria andRefundFeeGreaterThan(Long value) {
            addCriterion("refund_fee >", value, "refundFee");
            return (Criteria) this;
        }

        public Criteria andRefundFeeGreaterThanOrEqualTo(Long value) {
            addCriterion("refund_fee >=", value, "refundFee");
            return (Criteria) this;
        }

        public Criteria andRefundFeeLessThan(Long value) {
            addCriterion("refund_fee <", value, "refundFee");
            return (Criteria) this;
        }

        public Criteria andRefundFeeLessThanOrEqualTo(Long value) {
            addCriterion("refund_fee <=", value, "refundFee");
            return (Criteria) this;
        }

        public Criteria andRefundFeeIn(List<Long> values) {
            addCriterion("refund_fee in", values, "refundFee");
            return (Criteria) this;
        }

        public Criteria andRefundFeeNotIn(List<Long> values) {
            addCriterion("refund_fee not in", values, "refundFee");
            return (Criteria) this;
        }

        public Criteria andRefundFeeBetween(Long value1, Long value2) {
            addCriterion("refund_fee between", value1, value2, "refundFee");
            return (Criteria) this;
        }

        public Criteria andRefundFeeNotBetween(Long value1, Long value2) {
            addCriterion("refund_fee not between", value1, value2, "refundFee");
            return (Criteria) this;
        }

        public Criteria andRefundCurrencyTypeIsNull() {
            addCriterion("refund_currency_type is null");
            return (Criteria) this;
        }

        public Criteria andRefundCurrencyTypeIsNotNull() {
            addCriterion("refund_currency_type is not null");
            return (Criteria) this;
        }

        public Criteria andRefundCurrencyTypeEqualTo(Integer value) {
            addCriterion("refund_currency_type =", value, "refundCurrencyType");
            return (Criteria) this;
        }

        public Criteria andRefundCurrencyTypeNotEqualTo(Integer value) {
            addCriterion("refund_currency_type <>", value, "refundCurrencyType");
            return (Criteria) this;
        }

        public Criteria andRefundCurrencyTypeGreaterThan(Integer value) {
            addCriterion("refund_currency_type >", value, "refundCurrencyType");
            return (Criteria) this;
        }

        public Criteria andRefundCurrencyTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("refund_currency_type >=", value, "refundCurrencyType");
            return (Criteria) this;
        }

        public Criteria andRefundCurrencyTypeLessThan(Integer value) {
            addCriterion("refund_currency_type <", value, "refundCurrencyType");
            return (Criteria) this;
        }

        public Criteria andRefundCurrencyTypeLessThanOrEqualTo(Integer value) {
            addCriterion("refund_currency_type <=", value, "refundCurrencyType");
            return (Criteria) this;
        }

        public Criteria andRefundCurrencyTypeIn(List<Integer> values) {
            addCriterion("refund_currency_type in", values, "refundCurrencyType");
            return (Criteria) this;
        }

        public Criteria andRefundCurrencyTypeNotIn(List<Integer> values) {
            addCriterion("refund_currency_type not in", values, "refundCurrencyType");
            return (Criteria) this;
        }

        public Criteria andRefundCurrencyTypeBetween(Integer value1, Integer value2) {
            addCriterion("refund_currency_type between", value1, value2, "refundCurrencyType");
            return (Criteria) this;
        }

        public Criteria andRefundCurrencyTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("refund_currency_type not between", value1, value2, "refundCurrencyType");
            return (Criteria) this;
        }

        public Criteria andDiscountFeeIsNull() {
            addCriterion("discount_fee is null");
            return (Criteria) this;
        }

        public Criteria andDiscountFeeIsNotNull() {
            addCriterion("discount_fee is not null");
            return (Criteria) this;
        }

        public Criteria andDiscountFeeEqualTo(Long value) {
            addCriterion("discount_fee =", value, "discountFee");
            return (Criteria) this;
        }

        public Criteria andDiscountFeeNotEqualTo(Long value) {
            addCriterion("discount_fee <>", value, "discountFee");
            return (Criteria) this;
        }

        public Criteria andDiscountFeeGreaterThan(Long value) {
            addCriterion("discount_fee >", value, "discountFee");
            return (Criteria) this;
        }

        public Criteria andDiscountFeeGreaterThanOrEqualTo(Long value) {
            addCriterion("discount_fee >=", value, "discountFee");
            return (Criteria) this;
        }

        public Criteria andDiscountFeeLessThan(Long value) {
            addCriterion("discount_fee <", value, "discountFee");
            return (Criteria) this;
        }

        public Criteria andDiscountFeeLessThanOrEqualTo(Long value) {
            addCriterion("discount_fee <=", value, "discountFee");
            return (Criteria) this;
        }

        public Criteria andDiscountFeeIn(List<Long> values) {
            addCriterion("discount_fee in", values, "discountFee");
            return (Criteria) this;
        }

        public Criteria andDiscountFeeNotIn(List<Long> values) {
            addCriterion("discount_fee not in", values, "discountFee");
            return (Criteria) this;
        }

        public Criteria andDiscountFeeBetween(Long value1, Long value2) {
            addCriterion("discount_fee between", value1, value2, "discountFee");
            return (Criteria) this;
        }

        public Criteria andDiscountFeeNotBetween(Long value1, Long value2) {
            addCriterion("discount_fee not between", value1, value2, "discountFee");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoIsNull() {
            addCriterion("promotion_info is null");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoIsNotNull() {
            addCriterion("promotion_info is not null");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoEqualTo(String value) {
            addCriterion("promotion_info =", value, "promotionInfo");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoNotEqualTo(String value) {
            addCriterion("promotion_info <>", value, "promotionInfo");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoGreaterThan(String value) {
            addCriterion("promotion_info >", value, "promotionInfo");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoGreaterThanOrEqualTo(String value) {
            addCriterion("promotion_info >=", value, "promotionInfo");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoLessThan(String value) {
            addCriterion("promotion_info <", value, "promotionInfo");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoLessThanOrEqualTo(String value) {
            addCriterion("promotion_info <=", value, "promotionInfo");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoLike(String value) {
            addCriterion("promotion_info like", value, "promotionInfo");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoNotLike(String value) {
            addCriterion("promotion_info not like", value, "promotionInfo");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoIn(List<String> values) {
            addCriterion("promotion_info in", values, "promotionInfo");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoNotIn(List<String> values) {
            addCriterion("promotion_info not in", values, "promotionInfo");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoBetween(String value1, String value2) {
            addCriterion("promotion_info between", value1, value2, "promotionInfo");
            return (Criteria) this;
        }

        public Criteria andPromotionInfoNotBetween(String value1, String value2) {
            addCriterion("promotion_info not between", value1, value2, "promotionInfo");
            return (Criteria) this;
        }

        public Criteria andPerformanceStatusIsNull() {
            addCriterion("performance_status is null");
            return (Criteria) this;
        }

        public Criteria andPerformanceStatusIsNotNull() {
            addCriterion("performance_status is not null");
            return (Criteria) this;
        }

        public Criteria andPerformanceStatusEqualTo(Integer value) {
            addCriterion("performance_status =", value, "performanceStatus");
            return (Criteria) this;
        }

        public Criteria andPerformanceStatusNotEqualTo(Integer value) {
            addCriterion("performance_status <>", value, "performanceStatus");
            return (Criteria) this;
        }

        public Criteria andPerformanceStatusGreaterThan(Integer value) {
            addCriterion("performance_status >", value, "performanceStatus");
            return (Criteria) this;
        }

        public Criteria andPerformanceStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("performance_status >=", value, "performanceStatus");
            return (Criteria) this;
        }

        public Criteria andPerformanceStatusLessThan(Integer value) {
            addCriterion("performance_status <", value, "performanceStatus");
            return (Criteria) this;
        }

        public Criteria andPerformanceStatusLessThanOrEqualTo(Integer value) {
            addCriterion("performance_status <=", value, "performanceStatus");
            return (Criteria) this;
        }

        public Criteria andPerformanceStatusIn(List<Integer> values) {
            addCriterion("performance_status in", values, "performanceStatus");
            return (Criteria) this;
        }

        public Criteria andPerformanceStatusNotIn(List<Integer> values) {
            addCriterion("performance_status not in", values, "performanceStatus");
            return (Criteria) this;
        }

        public Criteria andPerformanceStatusBetween(Integer value1, Integer value2) {
            addCriterion("performance_status between", value1, value2, "performanceStatus");
            return (Criteria) this;
        }

        public Criteria andPerformanceStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("performance_status not between", value1, value2, "performanceStatus");
            return (Criteria) this;
        }

        public Criteria andTotalFeeIsNull() {
            addCriterion("total_fee is null");
            return (Criteria) this;
        }

        public Criteria andTotalFeeIsNotNull() {
            addCriterion("total_fee is not null");
            return (Criteria) this;
        }

        public Criteria andTotalFeeEqualTo(Long value) {
            addCriterion("total_fee =", value, "totalFee");
            return (Criteria) this;
        }

        public Criteria andTotalFeeNotEqualTo(Long value) {
            addCriterion("total_fee <>", value, "totalFee");
            return (Criteria) this;
        }

        public Criteria andTotalFeeGreaterThan(Long value) {
            addCriterion("total_fee >", value, "totalFee");
            return (Criteria) this;
        }

        public Criteria andTotalFeeGreaterThanOrEqualTo(Long value) {
            addCriterion("total_fee >=", value, "totalFee");
            return (Criteria) this;
        }

        public Criteria andTotalFeeLessThan(Long value) {
            addCriterion("total_fee <", value, "totalFee");
            return (Criteria) this;
        }

        public Criteria andTotalFeeLessThanOrEqualTo(Long value) {
            addCriterion("total_fee <=", value, "totalFee");
            return (Criteria) this;
        }

        public Criteria andTotalFeeIn(List<Long> values) {
            addCriterion("total_fee in", values, "totalFee");
            return (Criteria) this;
        }

        public Criteria andTotalFeeNotIn(List<Long> values) {
            addCriterion("total_fee not in", values, "totalFee");
            return (Criteria) this;
        }

        public Criteria andTotalFeeBetween(Long value1, Long value2) {
            addCriterion("total_fee between", value1, value2, "totalFee");
            return (Criteria) this;
        }

        public Criteria andTotalFeeNotBetween(Long value1, Long value2) {
            addCriterion("total_fee not between", value1, value2, "totalFee");
            return (Criteria) this;
        }

        public Criteria andPayFeeIsNull() {
            addCriterion("pay_fee is null");
            return (Criteria) this;
        }

        public Criteria andPayFeeIsNotNull() {
            addCriterion("pay_fee is not null");
            return (Criteria) this;
        }

        public Criteria andPayFeeEqualTo(Long value) {
            addCriterion("pay_fee =", value, "payFee");
            return (Criteria) this;
        }

        public Criteria andPayFeeNotEqualTo(Long value) {
            addCriterion("pay_fee <>", value, "payFee");
            return (Criteria) this;
        }

        public Criteria andPayFeeGreaterThan(Long value) {
            addCriterion("pay_fee >", value, "payFee");
            return (Criteria) this;
        }

        public Criteria andPayFeeGreaterThanOrEqualTo(Long value) {
            addCriterion("pay_fee >=", value, "payFee");
            return (Criteria) this;
        }

        public Criteria andPayFeeLessThan(Long value) {
            addCriterion("pay_fee <", value, "payFee");
            return (Criteria) this;
        }

        public Criteria andPayFeeLessThanOrEqualTo(Long value) {
            addCriterion("pay_fee <=", value, "payFee");
            return (Criteria) this;
        }

        public Criteria andPayFeeIn(List<Long> values) {
            addCriterion("pay_fee in", values, "payFee");
            return (Criteria) this;
        }

        public Criteria andPayFeeNotIn(List<Long> values) {
            addCriterion("pay_fee not in", values, "payFee");
            return (Criteria) this;
        }

        public Criteria andPayFeeBetween(Long value1, Long value2) {
            addCriterion("pay_fee between", value1, value2, "payFee");
            return (Criteria) this;
        }

        public Criteria andPayFeeNotBetween(Long value1, Long value2) {
            addCriterion("pay_fee not between", value1, value2, "payFee");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andAttributesIsNull() {
            addCriterion("attributes is null");
            return (Criteria) this;
        }

        public Criteria andAttributesIsNotNull() {
            addCriterion("attributes is not null");
            return (Criteria) this;
        }

        public Criteria andAttributesEqualTo(String value) {
            addCriterion("attributes =", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesNotEqualTo(String value) {
            addCriterion("attributes <>", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesGreaterThan(String value) {
            addCriterion("attributes >", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesGreaterThanOrEqualTo(String value) {
            addCriterion("attributes >=", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesLessThan(String value) {
            addCriterion("attributes <", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesLessThanOrEqualTo(String value) {
            addCriterion("attributes <=", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesLike(String value) {
            addCriterion("attributes like", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesNotLike(String value) {
            addCriterion("attributes not like", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesIn(List<String> values) {
            addCriterion("attributes in", values, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesNotIn(List<String> values) {
            addCriterion("attributes not in", values, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesBetween(String value1, String value2) {
            addCriterion("attributes between", value1, value2, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesNotBetween(String value1, String value2) {
            addCriterion("attributes not between", value1, value2, "attributes");
            return (Criteria) this;
        }

        public Criteria andContactsNameIsNull() {
            addCriterion("contacts_name is null");
            return (Criteria) this;
        }

        public Criteria andContactsNameIsNotNull() {
            addCriterion("contacts_name is not null");
            return (Criteria) this;
        }

        public Criteria andContactsNameEqualTo(String value) {
            addCriterion("contacts_name =", value, "contactsName");
            return (Criteria) this;
        }

        public Criteria andContactsNameNotEqualTo(String value) {
            addCriterion("contacts_name <>", value, "contactsName");
            return (Criteria) this;
        }

        public Criteria andContactsNameGreaterThan(String value) {
            addCriterion("contacts_name >", value, "contactsName");
            return (Criteria) this;
        }

        public Criteria andContactsNameGreaterThanOrEqualTo(String value) {
            addCriterion("contacts_name >=", value, "contactsName");
            return (Criteria) this;
        }

        public Criteria andContactsNameLessThan(String value) {
            addCriterion("contacts_name <", value, "contactsName");
            return (Criteria) this;
        }

        public Criteria andContactsNameLessThanOrEqualTo(String value) {
            addCriterion("contacts_name <=", value, "contactsName");
            return (Criteria) this;
        }

        public Criteria andContactsNameLike(String value) {
            addCriterion("contacts_name like", value, "contactsName");
            return (Criteria) this;
        }

        public Criteria andContactsNameNotLike(String value) {
            addCriterion("contacts_name not like", value, "contactsName");
            return (Criteria) this;
        }

        public Criteria andContactsNameIn(List<String> values) {
            addCriterion("contacts_name in", values, "contactsName");
            return (Criteria) this;
        }

        public Criteria andContactsNameNotIn(List<String> values) {
            addCriterion("contacts_name not in", values, "contactsName");
            return (Criteria) this;
        }

        public Criteria andContactsNameBetween(String value1, String value2) {
            addCriterion("contacts_name between", value1, value2, "contactsName");
            return (Criteria) this;
        }

        public Criteria andContactsNameNotBetween(String value1, String value2) {
            addCriterion("contacts_name not between", value1, value2, "contactsName");
            return (Criteria) this;
        }

        public Criteria andContactsMobileIsNull() {
            addCriterion("contacts_mobile is null");
            return (Criteria) this;
        }

        public Criteria andContactsMobileIsNotNull() {
            addCriterion("contacts_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andContactsMobileEqualTo(String value) {
            addCriterion("contacts_mobile =", value, "contactsMobile");
            return (Criteria) this;
        }

        public Criteria andContactsMobileNotEqualTo(String value) {
            addCriterion("contacts_mobile <>", value, "contactsMobile");
            return (Criteria) this;
        }

        public Criteria andContactsMobileGreaterThan(String value) {
            addCriterion("contacts_mobile >", value, "contactsMobile");
            return (Criteria) this;
        }

        public Criteria andContactsMobileGreaterThanOrEqualTo(String value) {
            addCriterion("contacts_mobile >=", value, "contactsMobile");
            return (Criteria) this;
        }

        public Criteria andContactsMobileLessThan(String value) {
            addCriterion("contacts_mobile <", value, "contactsMobile");
            return (Criteria) this;
        }

        public Criteria andContactsMobileLessThanOrEqualTo(String value) {
            addCriterion("contacts_mobile <=", value, "contactsMobile");
            return (Criteria) this;
        }

        public Criteria andContactsMobileLike(String value) {
            addCriterion("contacts_mobile like", value, "contactsMobile");
            return (Criteria) this;
        }

        public Criteria andContactsMobileNotLike(String value) {
            addCriterion("contacts_mobile not like", value, "contactsMobile");
            return (Criteria) this;
        }

        public Criteria andContactsMobileIn(List<String> values) {
            addCriterion("contacts_mobile in", values, "contactsMobile");
            return (Criteria) this;
        }

        public Criteria andContactsMobileNotIn(List<String> values) {
            addCriterion("contacts_mobile not in", values, "contactsMobile");
            return (Criteria) this;
        }

        public Criteria andContactsMobileBetween(String value1, String value2) {
            addCriterion("contacts_mobile between", value1, value2, "contactsMobile");
            return (Criteria) this;
        }

        public Criteria andContactsMobileNotBetween(String value1, String value2) {
            addCriterion("contacts_mobile not between", value1, value2, "contactsMobile");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxIsNull() {
            addCriterion("contacts_mailbox is null");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxIsNotNull() {
            addCriterion("contacts_mailbox is not null");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxEqualTo(String value) {
            addCriterion("contacts_mailbox =", value, "contactsMailbox");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxNotEqualTo(String value) {
            addCriterion("contacts_mailbox <>", value, "contactsMailbox");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxGreaterThan(String value) {
            addCriterion("contacts_mailbox >", value, "contactsMailbox");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxGreaterThanOrEqualTo(String value) {
            addCriterion("contacts_mailbox >=", value, "contactsMailbox");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxLessThan(String value) {
            addCriterion("contacts_mailbox <", value, "contactsMailbox");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxLessThanOrEqualTo(String value) {
            addCriterion("contacts_mailbox <=", value, "contactsMailbox");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxLike(String value) {
            addCriterion("contacts_mailbox like", value, "contactsMailbox");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxNotLike(String value) {
            addCriterion("contacts_mailbox not like", value, "contactsMailbox");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxIn(List<String> values) {
            addCriterion("contacts_mailbox in", values, "contactsMailbox");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxNotIn(List<String> values) {
            addCriterion("contacts_mailbox not in", values, "contactsMailbox");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxBetween(String value1, String value2) {
            addCriterion("contacts_mailbox between", value1, value2, "contactsMailbox");
            return (Criteria) this;
        }

        public Criteria andContactsMailboxNotBetween(String value1, String value2) {
            addCriterion("contacts_mailbox not between", value1, value2, "contactsMailbox");
            return (Criteria) this;
        }

        public Criteria andTravelDateIsNull() {
            addCriterion("travel_date is null");
            return (Criteria) this;
        }

        public Criteria andTravelDateIsNotNull() {
            addCriterion("travel_date is not null");
            return (Criteria) this;
        }

        public Criteria andTravelDateEqualTo(Date value) {
            addCriterion("travel_date =", value, "travelDate");
            return (Criteria) this;
        }

        public Criteria andTravelDateNotEqualTo(Date value) {
            addCriterion("travel_date <>", value, "travelDate");
            return (Criteria) this;
        }

        public Criteria andTravelDateGreaterThan(Date value) {
            addCriterion("travel_date >", value, "travelDate");
            return (Criteria) this;
        }

        public Criteria andTravelDateGreaterThanOrEqualTo(Date value) {
            addCriterion("travel_date >=", value, "travelDate");
            return (Criteria) this;
        }

        public Criteria andTravelDateLessThan(Date value) {
            addCriterion("travel_date <", value, "travelDate");
            return (Criteria) this;
        }

        public Criteria andTravelDateLessThanOrEqualTo(Date value) {
            addCriterion("travel_date <=", value, "travelDate");
            return (Criteria) this;
        }

        public Criteria andTravelDateIn(List<Date> values) {
            addCriterion("travel_date in", values, "travelDate");
            return (Criteria) this;
        }

        public Criteria andTravelDateNotIn(List<Date> values) {
            addCriterion("travel_date not in", values, "travelDate");
            return (Criteria) this;
        }

        public Criteria andTravelDateBetween(Date value1, Date value2) {
            addCriterion("travel_date between", value1, value2, "travelDate");
            return (Criteria) this;
        }

        public Criteria andTravelDateNotBetween(Date value1, Date value2) {
            addCriterion("travel_date not between", value1, value2, "travelDate");
            return (Criteria) this;
        }

        public Criteria andServiceStartTimeIsNull() {
            addCriterion("service_start_time is null");
            return (Criteria) this;
        }

        public Criteria andServiceStartTimeIsNotNull() {
            addCriterion("service_start_time is not null");
            return (Criteria) this;
        }

        public Criteria andServiceStartTimeEqualTo(Date value) {
            addCriterion("service_start_time =", value, "serviceStartTime");
            return (Criteria) this;
        }

        public Criteria andServiceStartTimeNotEqualTo(Date value) {
            addCriterion("service_start_time <>", value, "serviceStartTime");
            return (Criteria) this;
        }

        public Criteria andServiceStartTimeGreaterThan(Date value) {
            addCriterion("service_start_time >", value, "serviceStartTime");
            return (Criteria) this;
        }

        public Criteria andServiceStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("service_start_time >=", value, "serviceStartTime");
            return (Criteria) this;
        }

        public Criteria andServiceStartTimeLessThan(Date value) {
            addCriterion("service_start_time <", value, "serviceStartTime");
            return (Criteria) this;
        }

        public Criteria andServiceStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("service_start_time <=", value, "serviceStartTime");
            return (Criteria) this;
        }

        public Criteria andServiceStartTimeIn(List<Date> values) {
            addCriterion("service_start_time in", values, "serviceStartTime");
            return (Criteria) this;
        }

        public Criteria andServiceStartTimeNotIn(List<Date> values) {
            addCriterion("service_start_time not in", values, "serviceStartTime");
            return (Criteria) this;
        }

        public Criteria andServiceStartTimeBetween(Date value1, Date value2) {
            addCriterion("service_start_time between", value1, value2, "serviceStartTime");
            return (Criteria) this;
        }

        public Criteria andServiceStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("service_start_time not between", value1, value2, "serviceStartTime");
            return (Criteria) this;
        }

        public Criteria andServiceEndTimeIsNull() {
            addCriterion("service_end_time is null");
            return (Criteria) this;
        }

        public Criteria andServiceEndTimeIsNotNull() {
            addCriterion("service_end_time is not null");
            return (Criteria) this;
        }

        public Criteria andServiceEndTimeEqualTo(Date value) {
            addCriterion("service_end_time =", value, "serviceEndTime");
            return (Criteria) this;
        }

        public Criteria andServiceEndTimeNotEqualTo(Date value) {
            addCriterion("service_end_time <>", value, "serviceEndTime");
            return (Criteria) this;
        }

        public Criteria andServiceEndTimeGreaterThan(Date value) {
            addCriterion("service_end_time >", value, "serviceEndTime");
            return (Criteria) this;
        }

        public Criteria andServiceEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("service_end_time >=", value, "serviceEndTime");
            return (Criteria) this;
        }

        public Criteria andServiceEndTimeLessThan(Date value) {
            addCriterion("service_end_time <", value, "serviceEndTime");
            return (Criteria) this;
        }

        public Criteria andServiceEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("service_end_time <=", value, "serviceEndTime");
            return (Criteria) this;
        }

        public Criteria andServiceEndTimeIn(List<Date> values) {
            addCriterion("service_end_time in", values, "serviceEndTime");
            return (Criteria) this;
        }

        public Criteria andServiceEndTimeNotIn(List<Date> values) {
            addCriterion("service_end_time not in", values, "serviceEndTime");
            return (Criteria) this;
        }

        public Criteria andServiceEndTimeBetween(Date value1, Date value2) {
            addCriterion("service_end_time between", value1, value2, "serviceEndTime");
            return (Criteria) this;
        }

        public Criteria andServiceEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("service_end_time not between", value1, value2, "serviceEndTime");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixIsNull() {
            addCriterion("contact_mobile_pre_fix is null");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixIsNotNull() {
            addCriterion("contact_mobile_pre_fix is not null");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixEqualTo(String value) {
            addCriterion("contact_mobile_pre_fix =", value, "contactMobilePreFix");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixNotEqualTo(String value) {
            addCriterion("contact_mobile_pre_fix <>", value, "contactMobilePreFix");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixGreaterThan(String value) {
            addCriterion("contact_mobile_pre_fix >", value, "contactMobilePreFix");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixGreaterThanOrEqualTo(String value) {
            addCriterion("contact_mobile_pre_fix >=", value, "contactMobilePreFix");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixLessThan(String value) {
            addCriterion("contact_mobile_pre_fix <", value, "contactMobilePreFix");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixLessThanOrEqualTo(String value) {
            addCriterion("contact_mobile_pre_fix <=", value, "contactMobilePreFix");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixLike(String value) {
            addCriterion("contact_mobile_pre_fix like", value, "contactMobilePreFix");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixNotLike(String value) {
            addCriterion("contact_mobile_pre_fix not like", value, "contactMobilePreFix");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixIn(List<String> values) {
            addCriterion("contact_mobile_pre_fix in", values, "contactMobilePreFix");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixNotIn(List<String> values) {
            addCriterion("contact_mobile_pre_fix not in", values, "contactMobilePreFix");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixBetween(String value1, String value2) {
            addCriterion("contact_mobile_pre_fix between", value1, value2, "contactMobilePreFix");
            return (Criteria) this;
        }

        public Criteria andContactMobilePreFixNotBetween(String value1, String value2) {
            addCriterion("contact_mobile_pre_fix not between", value1, value2, "contactMobilePreFix");
            return (Criteria) this;
        }

        public Criteria andVersionIsNull() {
            addCriterion("version is null");
            return (Criteria) this;
        }

        public Criteria andVersionIsNotNull() {
            addCriterion("version is not null");
            return (Criteria) this;
        }

        public Criteria andVersionEqualTo(Integer value) {
            addCriterion("version =", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotEqualTo(Integer value) {
            addCriterion("version <>", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThan(Integer value) {
            addCriterion("version >", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThanOrEqualTo(Integer value) {
            addCriterion("version >=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThan(Integer value) {
            addCriterion("version <", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThanOrEqualTo(Integer value) {
            addCriterion("version <=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionIn(List<Integer> values) {
            addCriterion("version in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotIn(List<Integer> values) {
            addCriterion("version not in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionBetween(Integer value1, Integer value2) {
            addCriterion("version between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotBetween(Integer value1, Integer value2) {
            addCriterion("version not between", value1, value2, "version");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
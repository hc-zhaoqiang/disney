package com.resort.trade.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RtsOrderItemSkuExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RtsOrderItemSkuExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Long value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Long value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Long value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Long value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Long value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Long value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Long> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Long> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Long value1, Long value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Long value1, Long value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNull() {
            addCriterion("created_time is null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNotNull() {
            addCriterion("created_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeEqualTo(Date value) {
            addCriterion("created_time =", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotEqualTo(Date value) {
            addCriterion("created_time <>", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThan(Date value) {
            addCriterion("created_time >", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("created_time >=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThan(Date value) {
            addCriterion("created_time <", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("created_time <=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIn(List<Date> values) {
            addCriterion("created_time in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotIn(List<Date> values) {
            addCriterion("created_time not in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeBetween(Date value1, Date value2) {
            addCriterion("created_time between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("created_time not between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIsNull() {
            addCriterion("updated_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIsNotNull() {
            addCriterion("updated_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeEqualTo(Date value) {
            addCriterion("updated_time =", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotEqualTo(Date value) {
            addCriterion("updated_time <>", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeGreaterThan(Date value) {
            addCriterion("updated_time >", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("updated_time >=", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeLessThan(Date value) {
            addCriterion("updated_time <", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("updated_time <=", value, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeIn(List<Date> values) {
            addCriterion("updated_time in", values, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotIn(List<Date> values) {
            addCriterion("updated_time not in", values, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeBetween(Date value1, Date value2) {
            addCriterion("updated_time between", value1, value2, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andUpdatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("updated_time not between", value1, value2, "updatedTime");
            return (Criteria) this;
        }

        public Criteria andQuantityIsNull() {
            addCriterion("quantity is null");
            return (Criteria) this;
        }

        public Criteria andQuantityIsNotNull() {
            addCriterion("quantity is not null");
            return (Criteria) this;
        }

        public Criteria andQuantityEqualTo(Integer value) {
            addCriterion("quantity =", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityNotEqualTo(Integer value) {
            addCriterion("quantity <>", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityGreaterThan(Integer value) {
            addCriterion("quantity >", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityGreaterThanOrEqualTo(Integer value) {
            addCriterion("quantity >=", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityLessThan(Integer value) {
            addCriterion("quantity <", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityLessThanOrEqualTo(Integer value) {
            addCriterion("quantity <=", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityIn(List<Integer> values) {
            addCriterion("quantity in", values, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityNotIn(List<Integer> values) {
            addCriterion("quantity not in", values, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityBetween(Integer value1, Integer value2) {
            addCriterion("quantity between", value1, value2, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityNotBetween(Integer value1, Integer value2) {
            addCriterion("quantity not between", value1, value2, "quantity");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(Long value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(Long value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(Long value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(Long value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(Long value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(Long value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<Long> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<Long> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(Long value1, Long value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(Long value1, Long value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andItemIdIsNull() {
            addCriterion("item_id is null");
            return (Criteria) this;
        }

        public Criteria andItemIdIsNotNull() {
            addCriterion("item_id is not null");
            return (Criteria) this;
        }

        public Criteria andItemIdEqualTo(Long value) {
            addCriterion("item_id =", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotEqualTo(Long value) {
            addCriterion("item_id <>", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdGreaterThan(Long value) {
            addCriterion("item_id >", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdGreaterThanOrEqualTo(Long value) {
            addCriterion("item_id >=", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdLessThan(Long value) {
            addCriterion("item_id <", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdLessThanOrEqualTo(Long value) {
            addCriterion("item_id <=", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdIn(List<Long> values) {
            addCriterion("item_id in", values, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotIn(List<Long> values) {
            addCriterion("item_id not in", values, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdBetween(Long value1, Long value2) {
            addCriterion("item_id between", value1, value2, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotBetween(Long value1, Long value2) {
            addCriterion("item_id not between", value1, value2, "itemId");
            return (Criteria) this;
        }

        public Criteria andSkuNameIsNull() {
            addCriterion("sku_name is null");
            return (Criteria) this;
        }

        public Criteria andSkuNameIsNotNull() {
            addCriterion("sku_name is not null");
            return (Criteria) this;
        }

        public Criteria andSkuNameEqualTo(String value) {
            addCriterion("sku_name =", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameNotEqualTo(String value) {
            addCriterion("sku_name <>", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameGreaterThan(String value) {
            addCriterion("sku_name >", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameGreaterThanOrEqualTo(String value) {
            addCriterion("sku_name >=", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameLessThan(String value) {
            addCriterion("sku_name <", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameLessThanOrEqualTo(String value) {
            addCriterion("sku_name <=", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameLike(String value) {
            addCriterion("sku_name like", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameNotLike(String value) {
            addCriterion("sku_name not like", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameIn(List<String> values) {
            addCriterion("sku_name in", values, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameNotIn(List<String> values) {
            addCriterion("sku_name not in", values, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameBetween(String value1, String value2) {
            addCriterion("sku_name between", value1, value2, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameNotBetween(String value1, String value2) {
            addCriterion("sku_name not between", value1, value2, "skuName");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeIsNull() {
            addCriterion("rate_plan_code is null");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeIsNotNull() {
            addCriterion("rate_plan_code is not null");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeEqualTo(String value) {
            addCriterion("rate_plan_code =", value, "ratePlanCode");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeNotEqualTo(String value) {
            addCriterion("rate_plan_code <>", value, "ratePlanCode");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeGreaterThan(String value) {
            addCriterion("rate_plan_code >", value, "ratePlanCode");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeGreaterThanOrEqualTo(String value) {
            addCriterion("rate_plan_code >=", value, "ratePlanCode");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeLessThan(String value) {
            addCriterion("rate_plan_code <", value, "ratePlanCode");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeLessThanOrEqualTo(String value) {
            addCriterion("rate_plan_code <=", value, "ratePlanCode");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeLike(String value) {
            addCriterion("rate_plan_code like", value, "ratePlanCode");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeNotLike(String value) {
            addCriterion("rate_plan_code not like", value, "ratePlanCode");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeIn(List<String> values) {
            addCriterion("rate_plan_code in", values, "ratePlanCode");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeNotIn(List<String> values) {
            addCriterion("rate_plan_code not in", values, "ratePlanCode");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeBetween(String value1, String value2) {
            addCriterion("rate_plan_code between", value1, value2, "ratePlanCode");
            return (Criteria) this;
        }

        public Criteria andRatePlanCodeNotBetween(String value1, String value2) {
            addCriterion("rate_plan_code not between", value1, value2, "ratePlanCode");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeIsNull() {
            addCriterion("room_type_code is null");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeIsNotNull() {
            addCriterion("room_type_code is not null");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeEqualTo(String value) {
            addCriterion("room_type_code =", value, "roomTypeCode");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeNotEqualTo(String value) {
            addCriterion("room_type_code <>", value, "roomTypeCode");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeGreaterThan(String value) {
            addCriterion("room_type_code >", value, "roomTypeCode");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeGreaterThanOrEqualTo(String value) {
            addCriterion("room_type_code >=", value, "roomTypeCode");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeLessThan(String value) {
            addCriterion("room_type_code <", value, "roomTypeCode");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeLessThanOrEqualTo(String value) {
            addCriterion("room_type_code <=", value, "roomTypeCode");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeLike(String value) {
            addCriterion("room_type_code like", value, "roomTypeCode");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeNotLike(String value) {
            addCriterion("room_type_code not like", value, "roomTypeCode");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeIn(List<String> values) {
            addCriterion("room_type_code in", values, "roomTypeCode");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeNotIn(List<String> values) {
            addCriterion("room_type_code not in", values, "roomTypeCode");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeBetween(String value1, String value2) {
            addCriterion("room_type_code between", value1, value2, "roomTypeCode");
            return (Criteria) this;
        }

        public Criteria andRoomTypeCodeNotBetween(String value1, String value2) {
            addCriterion("room_type_code not between", value1, value2, "roomTypeCode");
            return (Criteria) this;
        }

        public Criteria andTravelDateTimeIsNull() {
            addCriterion("travel_date_time is null");
            return (Criteria) this;
        }

        public Criteria andTravelDateTimeIsNotNull() {
            addCriterion("travel_date_time is not null");
            return (Criteria) this;
        }

        public Criteria andTravelDateTimeEqualTo(Date value) {
            addCriterion("travel_date_time =", value, "travelDateTime");
            return (Criteria) this;
        }

        public Criteria andTravelDateTimeNotEqualTo(Date value) {
            addCriterion("travel_date_time <>", value, "travelDateTime");
            return (Criteria) this;
        }

        public Criteria andTravelDateTimeGreaterThan(Date value) {
            addCriterion("travel_date_time >", value, "travelDateTime");
            return (Criteria) this;
        }

        public Criteria andTravelDateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("travel_date_time >=", value, "travelDateTime");
            return (Criteria) this;
        }

        public Criteria andTravelDateTimeLessThan(Date value) {
            addCriterion("travel_date_time <", value, "travelDateTime");
            return (Criteria) this;
        }

        public Criteria andTravelDateTimeLessThanOrEqualTo(Date value) {
            addCriterion("travel_date_time <=", value, "travelDateTime");
            return (Criteria) this;
        }

        public Criteria andTravelDateTimeIn(List<Date> values) {
            addCriterion("travel_date_time in", values, "travelDateTime");
            return (Criteria) this;
        }

        public Criteria andTravelDateTimeNotIn(List<Date> values) {
            addCriterion("travel_date_time not in", values, "travelDateTime");
            return (Criteria) this;
        }

        public Criteria andTravelDateTimeBetween(Date value1, Date value2) {
            addCriterion("travel_date_time between", value1, value2, "travelDateTime");
            return (Criteria) this;
        }

        public Criteria andTravelDateTimeNotBetween(Date value1, Date value2) {
            addCriterion("travel_date_time not between", value1, value2, "travelDateTime");
            return (Criteria) this;
        }

        public Criteria andAdultNumIsNull() {
            addCriterion("adult_num is null");
            return (Criteria) this;
        }

        public Criteria andAdultNumIsNotNull() {
            addCriterion("adult_num is not null");
            return (Criteria) this;
        }

        public Criteria andAdultNumEqualTo(Integer value) {
            addCriterion("adult_num =", value, "adultNum");
            return (Criteria) this;
        }

        public Criteria andAdultNumNotEqualTo(Integer value) {
            addCriterion("adult_num <>", value, "adultNum");
            return (Criteria) this;
        }

        public Criteria andAdultNumGreaterThan(Integer value) {
            addCriterion("adult_num >", value, "adultNum");
            return (Criteria) this;
        }

        public Criteria andAdultNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("adult_num >=", value, "adultNum");
            return (Criteria) this;
        }

        public Criteria andAdultNumLessThan(Integer value) {
            addCriterion("adult_num <", value, "adultNum");
            return (Criteria) this;
        }

        public Criteria andAdultNumLessThanOrEqualTo(Integer value) {
            addCriterion("adult_num <=", value, "adultNum");
            return (Criteria) this;
        }

        public Criteria andAdultNumIn(List<Integer> values) {
            addCriterion("adult_num in", values, "adultNum");
            return (Criteria) this;
        }

        public Criteria andAdultNumNotIn(List<Integer> values) {
            addCriterion("adult_num not in", values, "adultNum");
            return (Criteria) this;
        }

        public Criteria andAdultNumBetween(Integer value1, Integer value2) {
            addCriterion("adult_num between", value1, value2, "adultNum");
            return (Criteria) this;
        }

        public Criteria andAdultNumNotBetween(Integer value1, Integer value2) {
            addCriterion("adult_num not between", value1, value2, "adultNum");
            return (Criteria) this;
        }

        public Criteria andChildNumIsNull() {
            addCriterion("child_num is null");
            return (Criteria) this;
        }

        public Criteria andChildNumIsNotNull() {
            addCriterion("child_num is not null");
            return (Criteria) this;
        }

        public Criteria andChildNumEqualTo(Integer value) {
            addCriterion("child_num =", value, "childNum");
            return (Criteria) this;
        }

        public Criteria andChildNumNotEqualTo(Integer value) {
            addCriterion("child_num <>", value, "childNum");
            return (Criteria) this;
        }

        public Criteria andChildNumGreaterThan(Integer value) {
            addCriterion("child_num >", value, "childNum");
            return (Criteria) this;
        }

        public Criteria andChildNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("child_num >=", value, "childNum");
            return (Criteria) this;
        }

        public Criteria andChildNumLessThan(Integer value) {
            addCriterion("child_num <", value, "childNum");
            return (Criteria) this;
        }

        public Criteria andChildNumLessThanOrEqualTo(Integer value) {
            addCriterion("child_num <=", value, "childNum");
            return (Criteria) this;
        }

        public Criteria andChildNumIn(List<Integer> values) {
            addCriterion("child_num in", values, "childNum");
            return (Criteria) this;
        }

        public Criteria andChildNumNotIn(List<Integer> values) {
            addCriterion("child_num not in", values, "childNum");
            return (Criteria) this;
        }

        public Criteria andChildNumBetween(Integer value1, Integer value2) {
            addCriterion("child_num between", value1, value2, "childNum");
            return (Criteria) this;
        }

        public Criteria andChildNumNotBetween(Integer value1, Integer value2) {
            addCriterion("child_num not between", value1, value2, "childNum");
            return (Criteria) this;
        }

        public Criteria andPersonTypeIsNull() {
            addCriterion("person_type is null");
            return (Criteria) this;
        }

        public Criteria andPersonTypeIsNotNull() {
            addCriterion("person_type is not null");
            return (Criteria) this;
        }

        public Criteria andPersonTypeEqualTo(String value) {
            addCriterion("person_type =", value, "personType");
            return (Criteria) this;
        }

        public Criteria andPersonTypeNotEqualTo(String value) {
            addCriterion("person_type <>", value, "personType");
            return (Criteria) this;
        }

        public Criteria andPersonTypeGreaterThan(String value) {
            addCriterion("person_type >", value, "personType");
            return (Criteria) this;
        }

        public Criteria andPersonTypeGreaterThanOrEqualTo(String value) {
            addCriterion("person_type >=", value, "personType");
            return (Criteria) this;
        }

        public Criteria andPersonTypeLessThan(String value) {
            addCriterion("person_type <", value, "personType");
            return (Criteria) this;
        }

        public Criteria andPersonTypeLessThanOrEqualTo(String value) {
            addCriterion("person_type <=", value, "personType");
            return (Criteria) this;
        }

        public Criteria andPersonTypeLike(String value) {
            addCriterion("person_type like", value, "personType");
            return (Criteria) this;
        }

        public Criteria andPersonTypeNotLike(String value) {
            addCriterion("person_type not like", value, "personType");
            return (Criteria) this;
        }

        public Criteria andPersonTypeIn(List<String> values) {
            addCriterion("person_type in", values, "personType");
            return (Criteria) this;
        }

        public Criteria andPersonTypeNotIn(List<String> values) {
            addCriterion("person_type not in", values, "personType");
            return (Criteria) this;
        }

        public Criteria andPersonTypeBetween(String value1, String value2) {
            addCriterion("person_type between", value1, value2, "personType");
            return (Criteria) this;
        }

        public Criteria andPersonTypeNotBetween(String value1, String value2) {
            addCriterion("person_type not between", value1, value2, "personType");
            return (Criteria) this;
        }

        public Criteria andNightsIsNull() {
            addCriterion("nights is null");
            return (Criteria) this;
        }

        public Criteria andNightsIsNotNull() {
            addCriterion("nights is not null");
            return (Criteria) this;
        }

        public Criteria andNightsEqualTo(Integer value) {
            addCriterion("nights =", value, "nights");
            return (Criteria) this;
        }

        public Criteria andNightsNotEqualTo(Integer value) {
            addCriterion("nights <>", value, "nights");
            return (Criteria) this;
        }

        public Criteria andNightsGreaterThan(Integer value) {
            addCriterion("nights >", value, "nights");
            return (Criteria) this;
        }

        public Criteria andNightsGreaterThanOrEqualTo(Integer value) {
            addCriterion("nights >=", value, "nights");
            return (Criteria) this;
        }

        public Criteria andNightsLessThan(Integer value) {
            addCriterion("nights <", value, "nights");
            return (Criteria) this;
        }

        public Criteria andNightsLessThanOrEqualTo(Integer value) {
            addCriterion("nights <=", value, "nights");
            return (Criteria) this;
        }

        public Criteria andNightsIn(List<Integer> values) {
            addCriterion("nights in", values, "nights");
            return (Criteria) this;
        }

        public Criteria andNightsNotIn(List<Integer> values) {
            addCriterion("nights not in", values, "nights");
            return (Criteria) this;
        }

        public Criteria andNightsBetween(Integer value1, Integer value2) {
            addCriterion("nights between", value1, value2, "nights");
            return (Criteria) this;
        }

        public Criteria andNightsNotBetween(Integer value1, Integer value2) {
            addCriterion("nights not between", value1, value2, "nights");
            return (Criteria) this;
        }

        public Criteria andHotelCodeIsNull() {
            addCriterion("hotel_code is null");
            return (Criteria) this;
        }

        public Criteria andHotelCodeIsNotNull() {
            addCriterion("hotel_code is not null");
            return (Criteria) this;
        }

        public Criteria andHotelCodeEqualTo(String value) {
            addCriterion("hotel_code =", value, "hotelCode");
            return (Criteria) this;
        }

        public Criteria andHotelCodeNotEqualTo(String value) {
            addCriterion("hotel_code <>", value, "hotelCode");
            return (Criteria) this;
        }

        public Criteria andHotelCodeGreaterThan(String value) {
            addCriterion("hotel_code >", value, "hotelCode");
            return (Criteria) this;
        }

        public Criteria andHotelCodeGreaterThanOrEqualTo(String value) {
            addCriterion("hotel_code >=", value, "hotelCode");
            return (Criteria) this;
        }

        public Criteria andHotelCodeLessThan(String value) {
            addCriterion("hotel_code <", value, "hotelCode");
            return (Criteria) this;
        }

        public Criteria andHotelCodeLessThanOrEqualTo(String value) {
            addCriterion("hotel_code <=", value, "hotelCode");
            return (Criteria) this;
        }

        public Criteria andHotelCodeLike(String value) {
            addCriterion("hotel_code like", value, "hotelCode");
            return (Criteria) this;
        }

        public Criteria andHotelCodeNotLike(String value) {
            addCriterion("hotel_code not like", value, "hotelCode");
            return (Criteria) this;
        }

        public Criteria andHotelCodeIn(List<String> values) {
            addCriterion("hotel_code in", values, "hotelCode");
            return (Criteria) this;
        }

        public Criteria andHotelCodeNotIn(List<String> values) {
            addCriterion("hotel_code not in", values, "hotelCode");
            return (Criteria) this;
        }

        public Criteria andHotelCodeBetween(String value1, String value2) {
            addCriterion("hotel_code between", value1, value2, "hotelCode");
            return (Criteria) this;
        }

        public Criteria andHotelCodeNotBetween(String value1, String value2) {
            addCriterion("hotel_code not between", value1, value2, "hotelCode");
            return (Criteria) this;
        }

        public Criteria andVersionIsNull() {
            addCriterion("version is null");
            return (Criteria) this;
        }

        public Criteria andVersionIsNotNull() {
            addCriterion("version is not null");
            return (Criteria) this;
        }

        public Criteria andVersionEqualTo(Integer value) {
            addCriterion("version =", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotEqualTo(Integer value) {
            addCriterion("version <>", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThan(Integer value) {
            addCriterion("version >", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThanOrEqualTo(Integer value) {
            addCriterion("version >=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThan(Integer value) {
            addCriterion("version <", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThanOrEqualTo(Integer value) {
            addCriterion("version <=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionIn(List<Integer> values) {
            addCriterion("version in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotIn(List<Integer> values) {
            addCriterion("version not in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionBetween(Integer value1, Integer value2) {
            addCriterion("version between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotBetween(Integer value1, Integer value2) {
            addCriterion("version not between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andStandardPriceIsNull() {
            addCriterion("standard_price is null");
            return (Criteria) this;
        }

        public Criteria andStandardPriceIsNotNull() {
            addCriterion("standard_price is not null");
            return (Criteria) this;
        }

        public Criteria andStandardPriceEqualTo(Long value) {
            addCriterion("standard_price =", value, "standardPrice");
            return (Criteria) this;
        }

        public Criteria andStandardPriceNotEqualTo(Long value) {
            addCriterion("standard_price <>", value, "standardPrice");
            return (Criteria) this;
        }

        public Criteria andStandardPriceGreaterThan(Long value) {
            addCriterion("standard_price >", value, "standardPrice");
            return (Criteria) this;
        }

        public Criteria andStandardPriceGreaterThanOrEqualTo(Long value) {
            addCriterion("standard_price >=", value, "standardPrice");
            return (Criteria) this;
        }

        public Criteria andStandardPriceLessThan(Long value) {
            addCriterion("standard_price <", value, "standardPrice");
            return (Criteria) this;
        }

        public Criteria andStandardPriceLessThanOrEqualTo(Long value) {
            addCriterion("standard_price <=", value, "standardPrice");
            return (Criteria) this;
        }

        public Criteria andStandardPriceIn(List<Long> values) {
            addCriterion("standard_price in", values, "standardPrice");
            return (Criteria) this;
        }

        public Criteria andStandardPriceNotIn(List<Long> values) {
            addCriterion("standard_price not in", values, "standardPrice");
            return (Criteria) this;
        }

        public Criteria andStandardPriceBetween(Long value1, Long value2) {
            addCriterion("standard_price between", value1, value2, "standardPrice");
            return (Criteria) this;
        }

        public Criteria andStandardPriceNotBetween(Long value1, Long value2) {
            addCriterion("standard_price not between", value1, value2, "standardPrice");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
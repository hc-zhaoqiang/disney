package com.resort.trade.mapper;

import com.resort.trade.pojo.RtsOrder;
import com.resort.trade.pojo.RtsOrderExample;
import com.resort.trade.pojo.RtsOrderWithBLOBs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RtsOrderMapper {
    long countByExample(RtsOrderExample example);

    int deleteByExample(RtsOrderExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RtsOrderWithBLOBs record);

    int insertSelective(RtsOrderWithBLOBs record);

    List<RtsOrderWithBLOBs> selectByExampleWithBLOBs(RtsOrderExample example);

    List<RtsOrder> selectByExample(RtsOrderExample example);

    RtsOrderWithBLOBs selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RtsOrderWithBLOBs record, @Param("example") RtsOrderExample example);

    int updateByExampleWithBLOBs(@Param("record") RtsOrderWithBLOBs record, @Param("example") RtsOrderExample example);

    int updateByExample(@Param("record") RtsOrder record, @Param("example") RtsOrderExample example);

    int updateByPrimaryKeySelective(RtsOrderWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(RtsOrderWithBLOBs record);

    int updateByPrimaryKey(RtsOrder record);
}
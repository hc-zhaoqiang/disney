package com.resort.trade.mapper;

import com.resort.trade.pojo.RtsRefundOrder;
import com.resort.trade.pojo.RtsRefundOrderExample;
import com.resort.trade.pojo.RtsRefundOrderWithBLOBs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RtsRefundOrderMapper {
    long countByExample(RtsRefundOrderExample example);

    int deleteByExample(RtsRefundOrderExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RtsRefundOrderWithBLOBs record);

    int insertSelective(RtsRefundOrderWithBLOBs record);

    List<RtsRefundOrderWithBLOBs> selectByExampleWithBLOBs(RtsRefundOrderExample example);

    List<RtsRefundOrder> selectByExample(RtsRefundOrderExample example);

    RtsRefundOrderWithBLOBs selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RtsRefundOrderWithBLOBs record, @Param("example") RtsRefundOrderExample example);

    int updateByExampleWithBLOBs(@Param("record") RtsRefundOrderWithBLOBs record, @Param("example") RtsRefundOrderExample example);

    int updateByExample(@Param("record") RtsRefundOrder record, @Param("example") RtsRefundOrderExample example);

    int updateByPrimaryKeySelective(RtsRefundOrderWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(RtsRefundOrderWithBLOBs record);

    int updateByPrimaryKey(RtsRefundOrder record);
}
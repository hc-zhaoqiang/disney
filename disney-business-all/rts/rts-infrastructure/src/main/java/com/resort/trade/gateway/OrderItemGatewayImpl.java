package com.resort.trade.gateway;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.trade.convert.OrderConvert;
import com.resort.trade.convert.OrderItemConvert;
import com.resort.trade.domain.gateway.OrderItemGateway;
import com.resort.trade.domain.model.Order;
import com.resort.trade.domain.model.OrderItem;
import com.resort.trade.mapper.RtsOrderItemMapper;
import com.resort.trade.mapper.RtsOrderMapper;
import com.resort.trade.pojo.RtsOrderItem;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author issavior
 */
@DomainGateway
public class OrderItemGatewayImpl implements OrderItemGateway {

    @Resource
    private RtsOrderItemMapper rtsOrderItemMapper;

    @Resource
    private OrderItemConvert orderItemConvert;

    @Override
    public void createOrderItem(OrderItem orderItem) {

        orderItem.setGmtCreate(new Date());
        orderItem.setGmtModified(new Date());
        orderItem.setVersion(1);
        rtsOrderItemMapper.insert(orderItemConvert.toRtsOrderItem(orderItem));
    }


}

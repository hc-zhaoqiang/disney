package com.resort.trade.gateway;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.trade.convert.OrderItemSkuConvert;
import com.resort.trade.domain.gateway.OrderItemSkuGateway;
import com.resort.trade.domain.model.OrderItemSku;
import com.resort.trade.mapper.RtsOrderItemSkuMapper;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author issavior
 */
@DomainGateway
public class OrderItemSkuGatewayImpl implements OrderItemSkuGateway {

    @Resource
    private RtsOrderItemSkuMapper rtsOrderItemSkuMapper;

    @Resource
    private OrderItemSkuConvert orderItemSkuConvert;

    @Override
    public void createOrderItemSku(OrderItemSku orderItemSku) {

        orderItemSku.setCreatedTime(new Date());
        orderItemSku.setUpdatedTime(new Date());
        orderItemSku.setVersion(1);
        rtsOrderItemSkuMapper.insert(orderItemSkuConvert.toRtsOrderItem(orderItemSku));
    }
}

package com.resort.trade.convert;

import com.disney.domain.sdk.anno.DomainConvert;
import com.resort.trade.domain.model.Order;
import com.resort.trade.pojo.RtsOrderWithBLOBs;

/**
 * @author issavior
 */
@DomainConvert
public class OrderConvert {
    public RtsOrderWithBLOBs toRtsOrder(Order order) {

        RtsOrderWithBLOBs rtsOrderWithBLOBs = new RtsOrderWithBLOBs();
        rtsOrderWithBLOBs.setBizCode(order.getBizCodeEnum().getCode());
        return rtsOrderWithBLOBs;
    }
}

package com.resort.trade.gateway;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.trade.convert.OrderConvert;
import com.resort.trade.domain.gateway.OrderGateway;
import com.resort.trade.domain.model.Order;
import com.resort.trade.mapper.RtsOrderMapper;
import com.resort.trade.pojo.RtsOrder;
import com.resort.trade.pojo.RtsOrderWithBLOBs;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author issavior
 */
@DomainGateway
public class OrderGatewayImpl implements OrderGateway {

    @Resource
    private RtsOrderMapper rtsOrderMapper;

    @Resource
    private OrderConvert orderConvert;

    @Override
    public Order findById(Long orderId) {
        return null;
    }

    @Override
    public void createOrder(Order order) {

        order.setGmtCreate(new Date());
        order.setGmtModified(new Date());
        order.setVersion(1);
        rtsOrderMapper.insert(orderConvert.toRtsOrder(order));
    }

    @Override
    public void updateStatuses(Order order) {

    }
}

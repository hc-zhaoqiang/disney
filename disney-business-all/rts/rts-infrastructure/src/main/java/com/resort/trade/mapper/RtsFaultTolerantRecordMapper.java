package com.resort.trade.mapper;

import com.resort.trade.pojo.RtsFaultTolerantRecord;
import com.resort.trade.pojo.RtsFaultTolerantRecordExample;
import com.resort.trade.pojo.RtsFaultTolerantRecordWithBLOBs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RtsFaultTolerantRecordMapper {
    long countByExample(RtsFaultTolerantRecordExample example);

    int deleteByExample(RtsFaultTolerantRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RtsFaultTolerantRecordWithBLOBs record);

    int insertSelective(RtsFaultTolerantRecordWithBLOBs record);

    List<RtsFaultTolerantRecordWithBLOBs> selectByExampleWithBLOBs(RtsFaultTolerantRecordExample example);

    List<RtsFaultTolerantRecord> selectByExample(RtsFaultTolerantRecordExample example);

    RtsFaultTolerantRecordWithBLOBs selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RtsFaultTolerantRecordWithBLOBs record, @Param("example") RtsFaultTolerantRecordExample example);

    int updateByExampleWithBLOBs(@Param("record") RtsFaultTolerantRecordWithBLOBs record, @Param("example") RtsFaultTolerantRecordExample example);

    int updateByExample(@Param("record") RtsFaultTolerantRecord record, @Param("example") RtsFaultTolerantRecordExample example);

    int updateByPrimaryKeySelective(RtsFaultTolerantRecordWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(RtsFaultTolerantRecordWithBLOBs record);

    int updateByPrimaryKey(RtsFaultTolerantRecord record);
}
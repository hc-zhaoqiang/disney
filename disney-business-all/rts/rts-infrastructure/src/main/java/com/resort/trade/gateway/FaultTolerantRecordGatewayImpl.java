package com.resort.trade.gateway;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.trade.domain.gateway.FaultTolerantRecordGateway;

/**
 * @author issavior
 */
@DomainGateway
public class FaultTolerantRecordGatewayImpl implements FaultTolerantRecordGateway {
}

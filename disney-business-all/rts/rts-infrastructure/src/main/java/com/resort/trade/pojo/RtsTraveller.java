package com.resort.trade.pojo;

import java.util.Date;

public class RtsTraveller {
    private Long id;

    private Date createdTime;

    private Date updatedTime;

    private String userId;

    private String name;

    private String mobile;

    private Short docType;

    private String docId;

    private Integer personCode;

    private Short effective;

    private Integer version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Short getDocType() {
        return docType;
    }

    public void setDocType(Short docType) {
        this.docType = docType;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public Integer getPersonCode() {
        return personCode;
    }

    public void setPersonCode(Integer personCode) {
        this.personCode = personCode;
    }

    public Short getEffective() {
        return effective;
    }

    public void setEffective(Short effective) {
        this.effective = effective;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
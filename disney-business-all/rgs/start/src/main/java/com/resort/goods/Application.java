package com.resort.goods;

import com.disney.domain.sdk.anno.FactorySpecs;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Spring Boot Starter
 *
 * @author Frank Zhang
 */
@SpringBootApplication(scanBasePackages = {"com.resort.goods", "com.alibaba.cola"})
@EnableDubbo
@MapperScan(value = "com.resort.goods.mapper")
@FactorySpecs({"com.resort", "com.kk"})
public class Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(Application.class, args);
        System.out.println(run);
    }
}

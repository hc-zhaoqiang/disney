package com.resort.goods.domain.gateway.pass;

import com.resort.goods.domain.model.pass.AnnualPass;
import com.resort.goods.domain.model.ticket.Ticket;

import java.util.List;

/**
 * @author issavior
 */
public interface AnnualPassGateway {

   List<AnnualPass> getAnnualPasses();

   AnnualPass getAnnualPassSubById(Long id);
}

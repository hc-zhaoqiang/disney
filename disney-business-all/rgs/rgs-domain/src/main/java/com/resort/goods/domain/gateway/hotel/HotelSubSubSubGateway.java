package com.resort.goods.domain.gateway.hotel;

import com.resort.goods.domain.model.hotel.HotelSubSubSub;

import java.util.List;

/**
 * @author issavior
 */
public interface HotelSubSubSubGateway {

   List<HotelSubSubSub> getHotelSubSubSubs();

   List<HotelSubSubSub> getHotelsBySubSubId();

   List<HotelSubSubSub> getHotelsBySubSubId(Long id);
}

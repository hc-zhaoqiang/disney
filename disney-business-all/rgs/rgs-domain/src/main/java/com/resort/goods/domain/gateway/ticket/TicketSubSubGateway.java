package com.resort.goods.domain.gateway.ticket;

import com.resort.goods.domain.model.ticket.TicketSubSub;

import java.util.List;

/**
 * @author issavior
 */
public interface TicketSubSubGateway {


   List<TicketSubSub> getTicketsBySubId(Long id);

   List<TicketSubSub> getTickets();
}

package com.resort.goods.domain.model.pass;

import com.disney.domain.sdk.common.Lazy;
import com.resort.goods.domain.model.agree.AgreeSubSub;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 年卡
 *
 * @author issavior
 */
@Data
@Builder
public class AnnualPass {
    private Lazy<List<AnnualPassSubSub>> annualPassSubSubs;

}

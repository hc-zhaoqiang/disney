package com.resort.goods.domain.gateway.agree;


import com.resort.goods.domain.model.agree.AgreeSubSubSub;

import java.util.List;

/**
 * @author issavior
 */
public interface AgreeSubSubSubGateway {

   List<AgreeSubSubSub> getAgreeSubSubSubs();

   List<AgreeSubSubSub> getAgreesBySubSubId();

   List<AgreeSubSubSub> getAgreesBySubSubId(Long id);
}

package com.resort.goods.domain.gateway.ticket;

import com.resort.goods.domain.model.ticket.TicketSubSubSub;

import java.util.List;

/**
 * @author issavior
 */
public interface TicketSubSubSubGateway {

   List<TicketSubSubSub> getTicketSubSubSubs();

   List<TicketSubSubSub> getTicketsBySubSubId();

   List<TicketSubSubSub> getTicketsBySubSubId(Long id);
}

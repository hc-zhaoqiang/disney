package com.resort.goods.domain.gateway.navigation;

import com.resort.goods.domain.model.navigation.Navigation;
import com.resort.goods.domain.model.pass.AnnualPass;

import java.util.List;

/**
 * @author issavior
 */
public interface NavigationGateway {

   List<Navigation> getNavigations();

   Navigation getNavigationSubById(Long id);

}

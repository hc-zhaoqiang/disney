package com.resort.goods.domain.model.ticket;

import com.alibaba.fastjson.JSON;
import com.disney.domain.sdk.common.Lazy;
import com.resort.goods.domain.model.price.PriceAndStock;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 门票｜票型｜ticketSubSub
 *
 * @author issavior
 */
@Data
@Builder
public class TicketSubSub {

    private Long id;

    private PriceAndStock priceAndStock;

    private Lazy<List<TicketSubSubSub>> ticketSubSubSubs;

}

package com.resort.goods.domain.model.ticket;

import com.disney.domain.sdk.common.Lazy;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 门票｜票类｜ticketSub
 *
 * @author issavior
 */
@Data
@Builder
public class Ticket {

   private Lazy<List<TicketSubSub>> ticketSubSubs;

}

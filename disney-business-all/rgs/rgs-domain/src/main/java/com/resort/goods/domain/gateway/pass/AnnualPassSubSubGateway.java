package com.resort.goods.domain.gateway.pass;

import com.resort.goods.domain.model.pass.AnnualPassSubSub;
import com.resort.goods.domain.model.ticket.TicketSubSub;

import java.util.List;

/**
 * @author issavior
 */
public interface AnnualPassSubSubGateway {


   List<AnnualPassSubSub> getAnnualPassBySubId(Long id);

   List<AnnualPassSubSub> getAnnualPass();
}

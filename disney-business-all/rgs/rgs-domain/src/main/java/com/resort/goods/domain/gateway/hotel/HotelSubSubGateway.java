package com.resort.goods.domain.gateway.hotel;

import com.resort.goods.domain.model.hotel.HotelSubSub;

import java.util.List;

/**
 * @author issavior
 */
public interface HotelSubSubGateway {


   List<HotelSubSub> getHotelsBySubId(Long id);

   List<HotelSubSub> getHotels();
}

package com.resort.goods.domain.model.navigation;

import com.disney.domain.sdk.common.Lazy;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 门票｜票种｜ticketSubSubSub
 *
 * @author issavior
 */
@Data
@Builder
public class NavigationSubSubSub {
    private Lazy<List<NavigationSubSubSub>> navigationSubSubSubs;


}

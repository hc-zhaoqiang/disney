package com.resort.goods.domain.model.navigation;

import com.disney.domain.sdk.common.Lazy;
import com.resort.goods.domain.model.service.ServiceSubSub;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 活动
 *
 * @author issavior
 */
@Data
@Builder
public class Navigation {
    private Lazy<List<NavigationSubSub>> navigationSubSubs;

}

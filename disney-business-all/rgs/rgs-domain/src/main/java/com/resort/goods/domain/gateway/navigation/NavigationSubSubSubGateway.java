package com.resort.goods.domain.gateway.navigation;

import com.resort.goods.domain.model.navigation.NavigationSubSubSub;
import com.resort.goods.domain.model.ticket.TicketSubSubSub;

import java.util.List;

/**
 * @author issavior
 */
public interface NavigationSubSubSubGateway {

   List<NavigationSubSubSub> getNavigationsBySubSubId();

   List<NavigationSubSubSub> getNavigationsBySubSubId(Long id);
}

package com.resort.goods.domain.gateway.navigation;

import com.resort.goods.domain.model.navigation.NavigationSubSub;

import java.util.List;

/**
 * @author issavior
 */
public interface NavigationSubSubGateway {


   List<NavigationSubSub> getNavigationsBySubId(Long id);

   List<NavigationSubSub> getNavigations();
}

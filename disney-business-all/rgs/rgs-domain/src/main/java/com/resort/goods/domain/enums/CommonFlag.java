package com.resort.goods.domain.enums;

/**
 * @author issavior
 */
public interface CommonFlag {

    /**
     * 有效
     */
    Integer EFFECTIVE = 1;
    Integer INVALID = 0;

    /**
     * 展示
     */
    Integer HOME_SHOW = 1;
    Integer HOME_NO_SHOW = 0;

    /**
     * 是否上架
     */
    Integer GROUNDING = 1;
    Integer NO_GROUNDING = 1;

}

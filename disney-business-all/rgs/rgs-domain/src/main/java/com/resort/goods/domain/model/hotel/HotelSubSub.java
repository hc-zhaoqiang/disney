package com.resort.goods.domain.model.hotel;

import com.disney.domain.sdk.common.Lazy;
import com.resort.goods.domain.model.price.PriceAndStock;
import com.resort.goods.domain.model.ticket.TicketSubSubSub;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 门票｜票型｜ticketSubSub
 *
 * @author issavior
 */
@Data
@Builder
public class HotelSubSub {

    private Long id;

    private PriceAndStock priceAndStock;

    private Lazy<List<HotelSubSubSub>> hotelSubSubSubs;


}

package com.resort.goods.domain.model.service;

import com.disney.domain.sdk.common.Lazy;
import com.resort.goods.domain.model.ticket.TicketSubSub;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 服务
 *
 * @author issavior
 */
@Data
@Builder
public class Service {

    private Lazy<List<ServiceSubSub>> serviceSubSubs;

}

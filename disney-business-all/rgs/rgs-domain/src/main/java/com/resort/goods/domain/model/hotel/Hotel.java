package com.resort.goods.domain.model.hotel;

import com.disney.domain.sdk.common.Lazy;
import com.resort.goods.domain.model.navigation.NavigationSubSub;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 酒店
 *
 * @author issavior
 */
@Data
@Builder
public class Hotel {
    private Lazy<List<HotelSubSub>> hotelSubSubs;

}

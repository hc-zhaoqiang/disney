package com.resort.goods.domain.gateway.pass;

import com.resort.goods.domain.model.pass.AnnualPassSubSubSub;
import com.resort.goods.domain.model.ticket.TicketSubSubSub;

import java.util.List;

/**
 * @author issavior
 */
public interface AnnualPassSubSubSubGateway {

   List<AnnualPassSubSubSub> getAnnualPassSubSubSubs();

   List<AnnualPassSubSubSub> getAnnualPasssBySubSubId();

   List<AnnualPassSubSubSub> getAnnualPasssBySubSubId(Long id);
}

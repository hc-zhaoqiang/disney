package com.resort.goods.domain.model.agree;

import com.disney.domain.sdk.common.Lazy;
import com.resort.goods.domain.model.price.PriceAndStock;
import com.resort.goods.domain.model.ticket.TicketSubSubSub;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 门票｜票型｜ticketSubSub
 *
 * @author issavior
 */
@Data
@Builder
public class AgreeSubSub {

    private Long id;

    private PriceAndStock priceAndStock;

    private Lazy<List<AgreeSubSubSub>> agreeSubSubSubs;


}

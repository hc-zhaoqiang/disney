package com.resort.goods.domain.gateway.service;

import com.resort.goods.domain.model.service.ServiceSubSub;
import com.resort.goods.domain.model.ticket.TicketSubSub;

import java.util.List;

/**
 * @author issavior
 */
public interface ServiceSubSubGateway {


   List<ServiceSubSub> getServicesBySubId(Long id);

   List<ServiceSubSub> getServices();
}

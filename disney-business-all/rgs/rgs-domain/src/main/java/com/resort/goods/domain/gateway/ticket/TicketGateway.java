package com.resort.goods.domain.gateway.ticket;

import com.resort.goods.domain.model.ticket.Ticket;

import java.util.List;

/**
 * @author issavior
 */
public interface TicketGateway {

   List<Ticket> getTicketSubs();
   Ticket getTicketSubById(Long id);
}

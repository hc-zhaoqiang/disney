package com.resort.goods.domain.gateway.service;

import com.resort.goods.domain.model.service.Service;
import com.resort.goods.domain.model.service.ServiceSubSub;
import com.resort.goods.domain.model.ticket.Ticket;

import java.util.List;

/**
 * @author issavior
 */
public interface ServiceGateway {

    List<Service> getServices();

    Service getServiceSubById(Long id);


}

package com.resort.goods.domain.gateway.element;

import com.resort.goods.domain.model.element.Element;

import java.util.List;

/**
 * @author issavior
 */
public interface ElementGateway {

   List<Element> getHomeElements();
}

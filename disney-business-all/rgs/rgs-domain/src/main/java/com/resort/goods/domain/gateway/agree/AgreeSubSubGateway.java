package com.resort.goods.domain.gateway.agree;

import com.resort.goods.domain.model.agree.AgreeSubSub;

import java.util.List;

/**
 * @author issavior
 */
public interface AgreeSubSubGateway {


   List<AgreeSubSub> getAgreesBySubId(Long id);

   List<AgreeSubSub> getAgrees();
}

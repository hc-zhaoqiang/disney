package com.resort.goods.domain.gateway.hotel;

import com.resort.goods.domain.model.hotel.Hotel;
import com.resort.goods.domain.model.navigation.Navigation;

import java.util.List;

/**
 * @author issavior
 */
public interface HotelGateway {

   List<Hotel> getHotels();

   Hotel getHotelSubById(Long id);

}

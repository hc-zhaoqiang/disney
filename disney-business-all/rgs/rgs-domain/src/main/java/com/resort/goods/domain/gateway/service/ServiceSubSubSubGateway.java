package com.resort.goods.domain.gateway.service;

import com.resort.goods.domain.model.service.ServiceSubSubSub;
import com.resort.goods.domain.model.ticket.TicketSubSubSub;

import java.util.List;

/**
 * @author issavior
 */
public interface ServiceSubSubSubGateway {

   List<ServiceSubSubSub> getServiceSubSubSubs();

   List<ServiceSubSubSub> getServicesBySubSubId();

   List<ServiceSubSubSub> getServicesBySubSubId(Long id);
}

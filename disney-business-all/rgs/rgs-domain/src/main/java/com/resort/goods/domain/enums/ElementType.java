package com.resort.goods.domain.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * 商品首页枚举
 * 1. 首页温馨提示
 * 2. 首页公告
 * 3. 首页顶部图片
 * 4.底部导航栏
 * @author issavior
 */
@Getter
public enum ElementType {

    TIPS(1, "温馨提示"),
    NOTICE(2, "公告"),
    PICTURE(3, "首页图片"),
    // 一键小写变大写 [cmd + shift + u]
    NAVIGATION(4,"底部导航栏"),
    AGREE(5, "我同意条款"),
    TOP_ITEMS(6,"顶部商品预览"),

    UNKNOWN(-1,"未知")
    ;

    private final Integer code;
    private final String msg;

    ElementType(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static ElementType of(Integer code) {
        return Arrays.stream(ElementType.values())
                .filter(e -> Objects.equals(e.getCode(), code))
                .findFirst()
                .orElse(UNKNOWN);
    }

}

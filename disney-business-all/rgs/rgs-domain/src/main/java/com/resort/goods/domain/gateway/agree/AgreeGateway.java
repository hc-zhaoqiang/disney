package com.resort.goods.domain.gateway.agree;

import com.resort.goods.domain.model.agree.Agree;
import com.resort.goods.domain.model.hotel.Hotel;

import java.util.List;

/**
 * @author issavior
 */
public interface AgreeGateway {

   List<Agree> getAgrees();

   Agree getAgreeSubById(Long id);

}

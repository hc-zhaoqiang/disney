package com.resort.goods.domain.model.agree;

import com.disney.domain.sdk.common.Lazy;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 餐饮
 *
 * @author issavior
 */
@Data
@Builder
public class Agree {
    private Lazy<List<AgreeSubSub>> agreeSubSubs;

}

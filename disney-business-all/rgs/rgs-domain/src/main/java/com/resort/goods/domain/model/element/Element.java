package com.resort.goods.domain.model.element;

import com.resort.goods.domain.enums.ElementType;
import lombok.Builder;
import lombok.Data;

/**
 * 商城元素域
 * @author issavior
 */
@Data
@Builder
public class Element {

    private ElementType elementType;
    private String title;
    private String context;
    // 1：展示；0不展示。
    private Integer show;

    public boolean isShow() {
        return this.show == 1;
    }


}

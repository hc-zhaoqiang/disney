package com.resort.goods.dto;

import com.resort.goods.dto.data.CustomerDTO;
import lombok.Data;

/**
 * @author issavior
 */
@Data
public class CustomerAddCmd{

    private CustomerDTO customerDTO;

}

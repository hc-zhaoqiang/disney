package com.resort.goods.dto.data;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author issavior
 */
@Data
public class CustomerDTO{
    private String customerId;
    private String memberId;
    private String customerName;
    private String customerType;
    @NotEmpty
    private String companyName;
    @NotEmpty
    private String source;
}

package com.resort.goods.api.cms.request;

import com.disney.domain.sdk.enums.DomainBizType;
import lombok.Builder;
import lombok.Data;

/**
 * @author issavior
 */
@Data
@Builder
public class GoodsReleaseRequest {

    /**
     * @see DomainBizType
     */
    private String bizCode;
}

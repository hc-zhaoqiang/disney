package com.resort.goods.dto;

import com.alibaba.cola.dto.Query;
import lombok.Data;

/**
 * @author issavior
 */
@Data
public class CustomerListByNameQry extends Query{
   private String name;
}

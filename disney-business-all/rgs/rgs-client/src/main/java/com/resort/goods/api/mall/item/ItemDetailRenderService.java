package com.resort.goods.api.mall.item;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.goods.api.mall.item.request.ItemDetailRenderRequest;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;

import java.util.List;

/**
 * @author issavior
 */
public interface ItemDetailRenderService {

    DisneyResult<List<ItemDetailRenderDTO>> getItemDetailRender(ItemDetailRenderRequest request);
}

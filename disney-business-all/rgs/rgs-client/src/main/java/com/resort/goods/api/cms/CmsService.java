package com.resort.goods.api.cms;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.goods.api.cms.request.*;

/**
 * @author issavior
 */
public interface CmsService {

    DisneyResult<Void> configGoods(ConfigGoodsRequest request);
    DisneyResult<Void> configGoodsSub(ConfigGoodsSubRequest request);
    DisneyResult<Void> configGoodsSubSub(ConfigGoodsSubSubRequest request);
    DisneyResult<Void> configGoodsSubSubSub(ConfigGoodsSubSubSubRequest request);

    /**
     * sp发布
     * @param request 发布商品
     * @return 发布成功或失败
     */
    DisneyResult<Void> goodsRelease(GoodsReleaseRequest request);


}

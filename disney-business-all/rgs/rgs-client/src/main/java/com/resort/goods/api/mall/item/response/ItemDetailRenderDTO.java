package com.resort.goods.api.mall.item.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder
public class ItemDetailRenderDTO implements Serializable {

    /**
     * 商品名
     */
    private String itemTitle;
    /**
     * 商品套餐/类型名字
     */
    private String itemName;
    /**
     * 商品图片描述链接
     */
    private String itemImageDescUrl;
    /**
     * 产品说明
     */
    private String itemProductDesc;
    /**
     * 变更说明
     */
    private String itemChangeDesc;
    /**
     * 购买须知
     */
    private String itemPurchaseNotes;
    /**
     * 入园须知
     */
    private String itemAdmissionNotice;



}

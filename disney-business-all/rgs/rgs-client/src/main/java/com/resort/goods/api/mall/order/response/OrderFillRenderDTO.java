package com.resort.goods.api.mall.order.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class OrderFillRenderDTO implements Serializable {
}

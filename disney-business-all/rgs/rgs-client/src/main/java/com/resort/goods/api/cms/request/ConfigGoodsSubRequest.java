package com.resort.goods.api.cms.request;

import com.disney.domain.sdk.enums.DomainBizType;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder
public class ConfigGoodsSubRequest implements Serializable {

    /**
     * @see DomainBizType
     */
    private String bizCode;
}

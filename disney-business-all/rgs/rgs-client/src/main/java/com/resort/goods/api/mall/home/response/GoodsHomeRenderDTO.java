package com.resort.goods.api.mall.home.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author issavior
 */
@Data
@Builder
public class GoodsHomeRenderDTO implements Serializable {

    /**
     * 商品名
     */
    private String goodsName;
    /**
     * 商品类别
     */
    private String goodsType;
    /**
     * 商品下的子商品列表
     */
    private List<GoodsTabulationDTO> goodsEntries;
}

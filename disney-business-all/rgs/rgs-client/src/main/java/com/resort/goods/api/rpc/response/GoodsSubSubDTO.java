package com.resort.goods.api.rpc.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author issavior
 */
@Data
@Builder
public class GoodsSubSubDTO {

    List<GoodsSubSubSubDTO> goodsSubSubSubS;
}

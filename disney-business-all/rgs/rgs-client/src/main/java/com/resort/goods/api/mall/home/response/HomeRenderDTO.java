package com.resort.goods.api.mall.home.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder
public class HomeRenderDTO implements Serializable {

    private Integer type;
    private String title;
    private String context;
    // 1：展示；0不展示。
    private Integer show;
}

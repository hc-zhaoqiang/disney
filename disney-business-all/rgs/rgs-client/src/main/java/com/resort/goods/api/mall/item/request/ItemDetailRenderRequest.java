package com.resort.goods.api.mall.item.request;

import com.disney.domain.sdk.enums.DomainBizType;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder
public class ItemDetailRenderRequest implements Serializable {

    /**
     * @see DomainBizType
     */
    private String bizCode;

    /**
     * 商品类id
     */
    private Long goodsSubConfigId;
}

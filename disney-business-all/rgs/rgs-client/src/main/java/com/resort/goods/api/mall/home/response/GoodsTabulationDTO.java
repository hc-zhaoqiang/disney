package com.resort.goods.api.mall.home.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author issavior
 */
@Data
@Builder
public class GoodsTabulationDTO implements Serializable {

    /**
     * 商品的子商品名
     */
    private String itemName;
    /**
     * 商品的子商品描述
     */
    private String itemDesc;
    /**
     * 商品的子商品标签
     */
    private List<String> itemTag;
    /**
     * 最低价
     */
    private String itemBottomPrice;
    /**
     * 最低价描述
     */
    private String itemBottomPriceDesc;
    /**
     * 是否可售
     */
    private boolean saleFlag;
    /**
     * 是否展示查询价格
     */
    private boolean queryPriceFlag;

}

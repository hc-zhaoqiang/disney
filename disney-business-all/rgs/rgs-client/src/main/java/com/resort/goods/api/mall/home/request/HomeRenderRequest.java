package com.resort.goods.api.mall.home.request;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder
public class HomeRenderRequest implements Serializable {
}

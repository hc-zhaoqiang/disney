package com.resort.goods.api.mall.order;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.goods.api.mall.order.request.OrderFillRenderRequest;
import com.resort.goods.api.mall.order.request.OrderValidateRenderRequest;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;

/**
 * @author issavior
 */
public interface OrderRenderService {

    /**
     * 订单选择页渲染
     * @return 订单选择页
     */
    DisneyResult<OrderValidateRenderDTO> orderValidateRender(OrderValidateRenderRequest request);

    /**
     * 订单填写页渲染
     * @return 订单填写页
     */
    DisneyResult<OrderFillRenderDTO> orderFillRender(OrderFillRenderRequest request);


}

package com.resort.goods.api.mall.home;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.goods.api.mall.home.response.HomeRenderDTO;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;

import java.util.List;

/**
 * 商城首页渲染
 * @author issavior
 */
public interface HomeRenderService {

    /**
     * 获取商城首页元素信息
     * @return 元素信息
     */
    public DisneyResult<List<HomeRenderDTO>> getHomes();

    /**
     * 获取商城首页商品信息
     * @return 商品信息
     */
    public DisneyResult<List<GoodsHomeRenderDTO>> getItems();
}

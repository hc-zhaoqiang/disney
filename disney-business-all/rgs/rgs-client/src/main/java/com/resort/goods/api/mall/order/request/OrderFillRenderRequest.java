package com.resort.goods.api.mall.order.request;

import com.disney.domain.sdk.enums.DomainBizType;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author issavior
 */
@Data
@Builder
public class OrderFillRenderRequest implements Serializable {

    /**
     * @see DomainBizType
     */
    private String bizCode;

    private Long subId;

    private Long subSubId;

}

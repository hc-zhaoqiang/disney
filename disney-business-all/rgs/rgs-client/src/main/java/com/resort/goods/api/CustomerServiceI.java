package com.resort.goods.api;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.resort.goods.dto.CustomerAddCmd;
import com.resort.goods.dto.CustomerListByNameQry;
import com.resort.goods.dto.data.CustomerDTO;

/**
 * @author issavior
 */
public interface CustomerServiceI {

    Response addCustomer(CustomerAddCmd customerAddCmd);

    MultiResponse<CustomerDTO> listByName(CustomerListByNameQry customerListByNameQry);
}

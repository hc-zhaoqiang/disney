package com.resort.goods.apps.goods;

import com.disney.domain.sdk.anno.AbsBiz;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.IDomainBiz;
import com.resort.goods.api.mall.order.request.OrderFillRenderRequest;
import com.resort.goods.api.mall.order.request.OrderValidateRenderRequest;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;

/**
 * @author issavior
 */
@AbsBiz(value = DomainAbsBizType.GOODS)
public abstract class OrderRenderDomainBiz implements IDomainBiz {

    /**
     * 订单选择页渲染
     *
     * @return 订单选择页
     */
    public abstract OrderValidateRenderDTO orderValidateRender(OrderValidateRenderRequest request);

    /**
     * 订单填写页渲染
     *
     * @return 订单填写页
     */
    public abstract OrderFillRenderDTO orderFillRender(OrderFillRenderRequest request);

}

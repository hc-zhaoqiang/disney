package com.resort.goods.apps.goods.navigation;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.apps.goods.HomeDomainBiz;
import com.resort.goods.apps.goods.navigation.convert.NavigationConvert;
import com.resort.goods.domain.gateway.navigation.NavigationGateway;

import javax.annotation.Resource;

/**
 * @author issavior
 */
@Biz(DomainBizType.NAVIGATION)
public class NavigationHomeDomainBiz extends HomeDomainBiz {

    @Resource
    private NavigationGateway navigationGateway;

    @Override
    public GoodsHomeRenderDTO homeRender() {
        return NavigationConvert.toGoodsHomeRenderDTO(navigationGateway.getNavigations());

    }

}

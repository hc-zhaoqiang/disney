package com.resort.goods.apps.goods.ticket;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.order.request.OrderFillRenderRequest;
import com.resort.goods.api.mall.order.request.OrderValidateRenderRequest;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.apps.goods.OrderRenderDomainBiz;
import com.resort.goods.domain.gateway.ticket.TicketGateway;

import javax.annotation.Resource;
import java.util.Objects;

import com.resort.goods.apps.goods.ticket.convert.TicketConvert;

/**
 * @author issavior
 */
@Biz(DomainBizType.TICKET)
public class TicketOrderRenderDomainBiz extends OrderRenderDomainBiz {

    @Resource
    private TicketGateway ticketGateway;

    @Override
    public OrderValidateRenderDTO orderValidateRender(OrderValidateRenderRequest request) {
        return TicketConvert.toOrderValidateRenderDTO(ticketGateway.getTicketSubById(request.getSubId())
                .getTicketSubSubs().get().stream()
                .filter(subSub -> Objects.equals(subSub.getId(), request.getSubSubId()))
                .findFirst()
                .orElse(null));
    }

    @Override
    public OrderFillRenderDTO orderFillRender(OrderFillRenderRequest request) {
        return TicketConvert.toOrderFillRenderDTO(ticketGateway.getTicketSubById(request.getSubId())
                .getTicketSubSubs().get().stream()
                .filter(subSub -> Objects.equals(subSub.getId(), request.getSubSubId()))
                .findFirst()
                .orElse(null));
    }
}

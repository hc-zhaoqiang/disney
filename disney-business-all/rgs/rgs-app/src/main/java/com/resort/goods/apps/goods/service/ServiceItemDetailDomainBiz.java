package com.resort.goods.apps.goods.service;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.item.request.ItemDetailRenderRequest;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.apps.goods.ItemDetailDomainBiz;
import com.resort.goods.domain.gateway.service.ServiceGateway;
import com.resort.goods.apps.goods.service.convert.ServiceConvert;
import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@Biz(DomainBizType.TICKET)
public class ServiceItemDetailDomainBiz extends ItemDetailDomainBiz {

    @Resource
    private ServiceGateway serviceGateway;

    @Override
    public List<ItemDetailRenderDTO> itemDetailRender(ItemDetailRenderRequest request) {
        return ServiceConvert.toItemDetailRenderDTO(serviceGateway.getServiceSubById(request.getGoodsSubConfigId()));

    }
}

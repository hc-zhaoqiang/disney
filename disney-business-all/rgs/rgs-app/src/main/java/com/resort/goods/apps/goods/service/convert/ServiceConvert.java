package com.resort.goods.apps.goods.service.convert;

import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;

import com.resort.goods.api.mall.home.response.GoodsTabulationDTO;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.domain.model.service.Service;
import com.resort.goods.domain.model.service.ServiceSubSub;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
public class ServiceConvert {


    public static GoodsHomeRenderDTO toGoodsHomeRenderDTO(List<Service> services) {
        return GoodsHomeRenderDTO.builder()
                .goodsType(DomainBizType.SERVICE.getCode())
                .goodsEntries(services.stream().map(a -> GoodsTabulationDTO.builder().build()).collect(Collectors.toList()))
                .build();
    }

    public static List<ItemDetailRenderDTO> toItemDetailRenderDTO(Service service) {
        return service.getServiceSubSubs().get().stream()
                .map(subSub -> ItemDetailRenderDTO.builder().build())
                .collect(Collectors.toList());
    }

    public static OrderValidateRenderDTO toOrderValidateRenderDTO(ServiceSubSub serviceSubSub) {
        return OrderValidateRenderDTO.builder().build();
    }

    public static OrderFillRenderDTO toOrderFillRenderDTO(ServiceSubSub serviceSubSub) {
        return OrderFillRenderDTO.builder().build();
    }
}

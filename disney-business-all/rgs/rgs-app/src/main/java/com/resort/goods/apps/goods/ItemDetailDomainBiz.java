package com.resort.goods.apps.goods;

import com.disney.domain.sdk.anno.AbsBiz;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.IDomainBiz;
import com.resort.goods.api.mall.item.request.ItemDetailRenderRequest;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;

import java.util.List;

/**
 * @author issavior
 */
@AbsBiz(value = DomainAbsBizType.GOODS)
public abstract class ItemDetailDomainBiz implements IDomainBiz {

    public abstract List<ItemDetailRenderDTO> itemDetailRender(ItemDetailRenderRequest request);
}

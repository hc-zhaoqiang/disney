package com.resort.goods.apps.goods.pass;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.order.request.OrderFillRenderRequest;
import com.resort.goods.api.mall.order.request.OrderValidateRenderRequest;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.apps.goods.OrderRenderDomainBiz;
import com.resort.goods.apps.goods.pass.convert.AnnualPassConvert;
import com.resort.goods.domain.gateway.pass.AnnualPassGateway;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author issavior
 */
@Biz(DomainBizType.ANNUAL_PASS)
public class AnnualPassOrderRenderDomainBiz extends OrderRenderDomainBiz {

    @Resource
    private AnnualPassGateway annualPassGateway;

    @Override
    public OrderValidateRenderDTO orderValidateRender(OrderValidateRenderRequest request) {
        return AnnualPassConvert.toOrderValidateRenderDTO(annualPassGateway.getAnnualPassSubById(request.getSubId())
                .getAnnualPassSubSubs().get().stream()
                .filter(subSub -> Objects.equals(subSub.getId(), request.getSubSubId()))
                .findFirst()
                .orElse(null));
    }

    @Override
    public OrderFillRenderDTO orderFillRender(OrderFillRenderRequest request) {
        return AnnualPassConvert.toOrderFillRenderDTO(annualPassGateway.getAnnualPassSubById(request.getSubId())
                .getAnnualPassSubSubs().get().stream()
                .filter(subSub -> Objects.equals(subSub.getId(), request.getSubSubId()))
                .findFirst()
                .orElse(null));
    }
}

package com.resort.goods.apps.goods.pass;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.apps.goods.HomeDomainBiz;
import com.resort.goods.apps.goods.pass.convert.AnnualPassConvert;
import com.resort.goods.domain.gateway.pass.AnnualPassGateway;

import javax.annotation.Resource;

/**
 * @author issavior
 */
@Biz(DomainBizType.ANNUAL_PASS)
public class AnnualPassHomeDomainBiz extends HomeDomainBiz {

    @Resource
    private AnnualPassGateway annualPassGateway;

    @Override
    public GoodsHomeRenderDTO homeRender() {
        return AnnualPassConvert.toGoodsHomeRenderDTO(annualPassGateway.getAnnualPasses());
    }

}

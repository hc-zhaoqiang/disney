package com.resort.goods.apps.cms;

import com.disney.domain.sdk.common.DisneyResult;
import com.disney.domain.sdk.anno.AbsBiz;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.IDomainBiz;
import com.resort.goods.api.cms.request.*;

/**
 * @author issavior
 */
@AbsBiz(value = DomainAbsBizType.CMS)
public abstract class CmsDomainBiz implements IDomainBiz {


    public abstract DisneyResult<Void> configGoods(ConfigGoodsRequest configGoodsRequest);

    public abstract DisneyResult<Void> configGoodsSub(ConfigGoodsSubRequest configGoodsSubRequest);

    public abstract DisneyResult<Void> configGoodsSubSub(ConfigGoodsSubSubRequest configGoodsSubSubRequest);

    public abstract DisneyResult<Void> configGoodsSubSubSub(ConfigGoodsSubSubSubRequest configGoodsSubSubSubRequest);

    /**
     * sp发布
     *
     * @param goodsReleaseRequest 发布商品
     * @return 发布成功或失败
     */
    public abstract DisneyResult<Void> goodsRelease(GoodsReleaseRequest goodsReleaseRequest);
}

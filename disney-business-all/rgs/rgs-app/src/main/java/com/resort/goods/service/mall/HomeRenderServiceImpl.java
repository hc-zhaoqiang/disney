package com.resort.goods.service.mall;

import com.disney.domain.sdk.common.DisneyResult;
import com.disney.domain.sdk.enums.DomainBizType;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.DomainExecutor;
import com.disney.domain.sdk.factory.DomainRequest;
import com.google.common.collect.Lists;
import com.resort.goods.api.mall.home.HomeRenderService;
import com.resort.goods.api.mall.home.response.HomeRenderDTO;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.apps.goods.HomeDomainBiz;
import com.resort.goods.domain.gateway.element.ElementGateway;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.resort.goods.convert.HomeRenderConvert;

import java.util.List;

/**
 * @author issavior
 */
@Service
@RequiredArgsConstructor
public class HomeRenderServiceImpl implements HomeRenderService {

    private final ElementGateway elementGateway;

    private static final DomainExecutor<HomeDomainBiz> EXECUTOR = new DomainExecutor<>(HomeDomainBiz.class);

    /**
     * 展示首页其他元素：同意条款、顶部首页图片展示、公告相关等
     *
     * @return 首页渲染元素
     */
    @Override
    public DisneyResult<List<HomeRenderDTO>> getHomes() {

        return DisneyResult.ok(HomeRenderConvert.fromHome(elementGateway.getHomeElements()));
    }

    @Override
    public DisneyResult<List<GoodsHomeRenderDTO>> getItems() {

        // todo 待优化，通过扩展点能力一个接口返回
        return DisneyResult.ok(Lists.newArrayList(EXECUTOR.execFirst(DomainRequest.to()
                                .code(DomainBizType.TICKET)
                                .scenario(DomainAbsBizType.GOODS)
                                .end(),
                        HomeDomainBiz::homeRender),
                EXECUTOR.execFirst(DomainRequest.to()
                                .code(DomainBizType.HOTEL)
                                .scenario(DomainAbsBizType.GOODS)
                                .end(),
                        HomeDomainBiz::homeRender),
                EXECUTOR.execFirst(DomainRequest.to()
                                .code(DomainBizType.SERVICE)
                                .scenario(DomainAbsBizType.GOODS)
                                .end(),
                        HomeDomainBiz::homeRender),
                EXECUTOR.execFirst(DomainRequest.to()
                                .code(DomainBizType.AGREE)
                                .scenario(DomainAbsBizType.GOODS)
                                .end(),
                        HomeDomainBiz::homeRender),
                EXECUTOR.execFirst(DomainRequest.to()
                                .code(DomainBizType.NAVIGATION)
                                .scenario(DomainAbsBizType.GOODS)
                                .end(),
                        HomeDomainBiz::homeRender),
                EXECUTOR.execFirst(DomainRequest.to()
                                .code(DomainBizType.ANNUAL_PASS)
                                .scenario(DomainAbsBizType.GOODS)
                                .end(),
                        HomeDomainBiz::homeRender)

        ));
    }
}

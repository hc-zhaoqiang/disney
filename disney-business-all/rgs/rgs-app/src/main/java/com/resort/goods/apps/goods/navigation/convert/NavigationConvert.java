package com.resort.goods.apps.goods.navigation.convert;

import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.api.mall.home.response.GoodsTabulationDTO;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.domain.model.navigation.Navigation;
import com.resort.goods.domain.model.navigation.NavigationSubSub;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
public class NavigationConvert {


    public static GoodsHomeRenderDTO toGoodsHomeRenderDTO(List<Navigation> navigations) {
        return GoodsHomeRenderDTO.builder()
                .goodsType(DomainBizType.NAVIGATION.getCode())
                .goodsEntries(navigations.stream().map(a -> GoodsTabulationDTO.builder().build()).collect(Collectors.toList()))
                .build();
    }

    public static List<ItemDetailRenderDTO> toItemDetailRenderDTO(Navigation navigation) {
        return navigation.getNavigationSubSubs().get().stream()
                .map(subSub -> ItemDetailRenderDTO.builder().build())
                .collect(Collectors.toList());
    }

    public static OrderValidateRenderDTO toOrderValidateRenderDTO(NavigationSubSub navigationSubSub) {
        return OrderValidateRenderDTO.builder().build();
    }

    public static OrderFillRenderDTO toOrderFillRenderDTO(NavigationSubSub navigationSubSub) {
        return OrderFillRenderDTO.builder().build();
    }
}

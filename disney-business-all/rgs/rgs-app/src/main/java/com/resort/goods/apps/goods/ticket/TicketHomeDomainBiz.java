package com.resort.goods.apps.goods.ticket;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.apps.goods.HomeDomainBiz;
import com.resort.goods.apps.goods.ticket.convert.TicketConvert;
import com.resort.goods.domain.gateway.ticket.TicketGateway;

import javax.annotation.Resource;

/**
 * @author issavior
 */
@Biz(DomainBizType.TICKET)
public class TicketHomeDomainBiz extends HomeDomainBiz {

    @Resource
    private TicketGateway ticketGateway;

    @Override
    public GoodsHomeRenderDTO homeRender() {
        return TicketConvert.toGoodsHomeRenderDTO(ticketGateway.getTicketSubs());
    }

}

package com.resort.goods.service.mall;

import com.disney.domain.sdk.common.DisneyResult;
import com.disney.domain.sdk.enums.DomainBizType;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.DomainExecutor;
import com.disney.domain.sdk.factory.DomainRequest;
import com.resort.goods.api.mall.item.ItemDetailRenderService;
import com.resort.goods.api.mall.item.request.ItemDetailRenderRequest;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.apps.goods.ItemDetailDomainBiz;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author issavior
 */
@Service
public class ItemDetailRenderServiceImpl implements ItemDetailRenderService {

    private static final DomainExecutor<ItemDetailDomainBiz> EXECUTOR = new DomainExecutor<>(ItemDetailDomainBiz.class);

    @Override
    public DisneyResult<List<ItemDetailRenderDTO>> getItemDetailRender(ItemDetailRenderRequest request) {
        return DisneyResult.ok(EXECUTOR.execFirst(DomainRequest.to()
                        .code(DomainBizType.of(request.getBizCode()))
                        .scenario(DomainAbsBizType.GOODS)
                        .end(),
                biz -> biz.itemDetailRender(request)));
    }
}

package com.resort.goods.apps.cms.agree;

import com.disney.domain.sdk.common.DisneyResult;
import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.cms.request.*;
import com.resort.goods.apps.cms.CmsDomainBiz;

/**
 * @author issavior
 */
@Biz(DomainBizType.AGREE)
public class AgreeCmsDomainBiz extends CmsDomainBiz {

    @Override
    public DisneyResult<Void> configGoods(ConfigGoodsRequest configGoodsRequest) {
        return null;
    }

    @Override
    public DisneyResult<Void> configGoodsSub(ConfigGoodsSubRequest configGoodsSubRequest) {
        return null;
    }

    @Override
    public DisneyResult<Void> configGoodsSubSub(ConfigGoodsSubSubRequest configGoodsSubSubRequest) {
        return null;
    }

    @Override
    public DisneyResult<Void> configGoodsSubSubSub(ConfigGoodsSubSubSubRequest configGoodsSubSubSubRequest) {
        return null;
    }

    @Override
    public DisneyResult<Void> goodsRelease(GoodsReleaseRequest goodsReleaseRequest) {
        return null;
    }
}

package com.resort.goods.apps.goods.pass.convert;

import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.api.mall.home.response.GoodsTabulationDTO;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.domain.model.pass.AnnualPass;
import com.resort.goods.domain.model.pass.AnnualPassSubSub;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
public class AnnualPassConvert {

    public static GoodsHomeRenderDTO toGoodsHomeRenderDTO(List<AnnualPass> annualPasses) {
        return GoodsHomeRenderDTO.builder()
                .goodsType(DomainBizType.ANNUAL_PASS.getCode())
                .goodsEntries(annualPasses.stream().map(a -> GoodsTabulationDTO.builder().build()).collect(Collectors.toList()))
                .build();
    }

    public static List<ItemDetailRenderDTO> toItemDetailRenderDTO(AnnualPass annualPass) {
        return annualPass.getAnnualPassSubSubs().get().stream()
                .map(subSub -> ItemDetailRenderDTO.builder().build())
                .collect(Collectors.toList());
    }

    public static OrderValidateRenderDTO toOrderValidateRenderDTO(AnnualPassSubSub annualPassSubSub) {
        return OrderValidateRenderDTO.builder().build();
    }

    public static OrderFillRenderDTO toOrderFillRenderDTO(AnnualPassSubSub annualPassSubSub) {
        return OrderFillRenderDTO.builder().build();
    }
}

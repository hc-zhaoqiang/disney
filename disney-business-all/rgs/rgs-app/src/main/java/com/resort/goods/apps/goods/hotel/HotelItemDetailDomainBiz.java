package com.resort.goods.apps.goods.hotel;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.item.request.ItemDetailRenderRequest;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.apps.goods.ItemDetailDomainBiz;
import com.resort.goods.apps.goods.hotel.convert.HotelConvert;
import com.resort.goods.domain.gateway.hotel.HotelGateway;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@Biz(DomainBizType.TICKET)
public class HotelItemDetailDomainBiz extends ItemDetailDomainBiz {

    @Resource
    private HotelGateway hotelGateway;

    @Override
    public List<ItemDetailRenderDTO> itemDetailRender(ItemDetailRenderRequest request) {
        return HotelConvert.toItemDetailRenderDTO(hotelGateway.getHotelSubById(request.getGoodsSubConfigId()));

    }
}

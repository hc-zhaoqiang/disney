package com.resort.goods.apps.goods.hotel.convert;

import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.api.mall.home.response.GoodsTabulationDTO;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.domain.model.hotel.Hotel;
import com.resort.goods.domain.model.hotel.HotelSubSub;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
public class HotelConvert {

    public static GoodsHomeRenderDTO toGoodsHomeRenderDTO(List<Hotel> hotels) {
        return GoodsHomeRenderDTO.builder()
                .goodsType(DomainBizType.HOTEL.getCode())
                .goodsEntries(hotels.stream().map(a -> GoodsTabulationDTO.builder().build()).collect(Collectors.toList()))
                .build();
    }

    public static List<ItemDetailRenderDTO> toItemDetailRenderDTO(Hotel hotel) {
        return hotel.getHotelSubSubs().get().stream()
                .map(subSub -> ItemDetailRenderDTO.builder().build())
                .collect(Collectors.toList());
    }

    public static OrderValidateRenderDTO toOrderValidateRenderDTO(HotelSubSub hotelSubSub) {
        return OrderValidateRenderDTO.builder().build();
    }

    public static OrderFillRenderDTO toOrderFillRenderDTO(HotelSubSub hotelSubSub) {
        return OrderFillRenderDTO.builder().build();
    }
}

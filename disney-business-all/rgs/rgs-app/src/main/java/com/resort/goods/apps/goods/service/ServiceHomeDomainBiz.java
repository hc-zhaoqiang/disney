package com.resort.goods.apps.goods.service;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.apps.goods.HomeDomainBiz;
import com.resort.goods.apps.goods.service.convert.ServiceConvert;
import com.resort.goods.domain.gateway.service.ServiceGateway;

import javax.annotation.Resource;

/**
 * @author issavior
 */
@Biz(DomainBizType.SERVICE)
public class ServiceHomeDomainBiz extends HomeDomainBiz {

    @Resource
    private ServiceGateway serviceGateway;
    @Override
    public GoodsHomeRenderDTO homeRender() {
        return ServiceConvert.toGoodsHomeRenderDTO(serviceGateway.getServices());
    }

}

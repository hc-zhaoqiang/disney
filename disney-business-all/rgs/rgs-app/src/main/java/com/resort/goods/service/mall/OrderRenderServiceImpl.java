package com.resort.goods.service.mall;

import com.disney.domain.sdk.common.DisneyResult;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.enums.DomainBizType;
import com.disney.domain.sdk.factory.DomainExecutor;
import com.disney.domain.sdk.factory.DomainRequest;
import com.resort.goods.api.mall.order.OrderRenderService;
import com.resort.goods.api.mall.order.request.OrderFillRenderRequest;
import com.resort.goods.api.mall.order.request.OrderValidateRenderRequest;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.apps.goods.OrderRenderDomainBiz;
import org.springframework.stereotype.Service;

/**
 * @author issavior
 */
@Service
public class OrderRenderServiceImpl implements OrderRenderService {

    private static final DomainExecutor<OrderRenderDomainBiz> EXECUTOR = new DomainExecutor<>(OrderRenderDomainBiz.class);

    @Override
    public DisneyResult<OrderValidateRenderDTO> orderValidateRender(OrderValidateRenderRequest request) {
        return DisneyResult.ok(EXECUTOR.execFirst(DomainRequest.to()
                        .code(DomainBizType.of(request.getBizCode()))
                        .scenario(DomainAbsBizType.GOODS)
                        .end(),
                biz -> biz.orderValidateRender(request)));
    }

    @Override
    public DisneyResult<OrderFillRenderDTO> orderFillRender(OrderFillRenderRequest request) {
        return DisneyResult.ok(EXECUTOR.execFirst(DomainRequest.to()
                        .code(DomainBizType.of(request.getBizCode()))
                        .scenario(DomainAbsBizType.GOODS)
                        .end(),
                biz -> biz.orderFillRender(request)));
    }
}

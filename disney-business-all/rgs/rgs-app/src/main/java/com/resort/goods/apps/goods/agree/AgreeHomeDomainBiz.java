package com.resort.goods.apps.goods.agree;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.apps.goods.HomeDomainBiz;
import com.resort.goods.domain.gateway.agree.AgreeGateway;
import com.resort.goods.apps.goods.agree.convert.AgreeConvert;

import javax.annotation.Resource;

/**
 * @author issavior
 */
@Biz(DomainBizType.AGREE)
public class AgreeHomeDomainBiz extends HomeDomainBiz {

    @Resource
    private AgreeGateway agreeGateway;

    @Override
    public GoodsHomeRenderDTO homeRender() {
        return AgreeConvert.toGoodsHomeRenderDTO(agreeGateway.getAgrees());
    }

}

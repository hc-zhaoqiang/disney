package com.resort.goods.service.cms;

import com.disney.domain.sdk.common.DisneyResult;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.enums.DomainBizType;
import com.disney.domain.sdk.factory.DomainExecutor;
import com.disney.domain.sdk.factory.DomainRequest;
import com.resort.goods.api.cms.CmsService;
import com.resort.goods.api.cms.request.*;
import com.resort.goods.apps.cms.CmsDomainBiz;
import org.springframework.stereotype.Service;

/**
 * @author issavior
 */
@Service
public class CmsServiceImpl implements CmsService {

    private static final DomainExecutor<CmsDomainBiz> EXECUTOR = new DomainExecutor<>(CmsDomainBiz.class);

    @Override
    public DisneyResult<Void> configGoods(ConfigGoodsRequest request) {
        return EXECUTOR.execFirst(DomainRequest.to()
                        .code(DomainBizType.of(request.getBizCode()))
                        .scenario(DomainAbsBizType.GOODS)
                        .end(),
                biz -> biz.configGoods(request));
    }

    @Override
    public DisneyResult<Void> configGoodsSub(ConfigGoodsSubRequest request) {
        return EXECUTOR.execFirst(DomainRequest.to()
                        .code(DomainBizType.of(request.getBizCode()))
                        .scenario(DomainAbsBizType.GOODS)
                        .end(),
                biz -> biz.configGoodsSub(request));
    }

    @Override
    public DisneyResult<Void> configGoodsSubSub(ConfigGoodsSubSubRequest request) {
        return EXECUTOR.execFirst(DomainRequest.to()
                        .code(DomainBizType.of(request.getBizCode()))
                        .scenario(DomainAbsBizType.GOODS)
                        .end(),
                biz -> biz.configGoodsSubSub(request));
    }

    @Override
    public DisneyResult<Void> configGoodsSubSubSub(ConfigGoodsSubSubSubRequest request) {
        return EXECUTOR.execFirst(DomainRequest.to()
                        .code(DomainBizType.of(request.getBizCode()))
                        .scenario(DomainAbsBizType.GOODS)
                        .end(),
                biz -> biz.configGoodsSubSubSub(request));
    }

    @Override
    public DisneyResult<Void> goodsRelease(GoodsReleaseRequest request) {
        return EXECUTOR.execFirst(DomainRequest.to()
                        .code(DomainBizType.of(request.getBizCode()))
                        .scenario(DomainAbsBizType.GOODS)
                        .end(),
                biz -> biz.goodsRelease(request));
    }
}

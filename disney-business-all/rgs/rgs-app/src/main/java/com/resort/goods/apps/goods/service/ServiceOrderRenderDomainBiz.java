package com.resort.goods.apps.goods.service;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.order.request.OrderFillRenderRequest;
import com.resort.goods.api.mall.order.request.OrderValidateRenderRequest;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.apps.goods.OrderRenderDomainBiz;
import com.resort.goods.apps.goods.service.convert.ServiceConvert;
import com.resort.goods.domain.gateway.service.ServiceGateway;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author issavior
 */
@Biz(DomainBizType.SERVICE)
public class ServiceOrderRenderDomainBiz extends OrderRenderDomainBiz {

    @Resource
    private ServiceGateway serviceGateway;

    @Override
    public OrderValidateRenderDTO orderValidateRender(OrderValidateRenderRequest request) {
        return ServiceConvert.toOrderValidateRenderDTO(serviceGateway.getServiceSubById(request.getSubId())
                .getServiceSubSubs().get().stream()
                .filter(subSub -> Objects.equals(subSub.getId(), request.getSubSubId()))
                .findFirst()
                .orElse(null));
    }

    @Override
    public OrderFillRenderDTO orderFillRender(OrderFillRenderRequest request) {
        return ServiceConvert.toOrderFillRenderDTO(serviceGateway.getServiceSubById(request.getSubId())
                .getServiceSubSubs().get().stream()
                .filter(subSub -> Objects.equals(subSub.getId(), request.getSubSubId()))
                .findFirst()
                .orElse(null));
    }
}

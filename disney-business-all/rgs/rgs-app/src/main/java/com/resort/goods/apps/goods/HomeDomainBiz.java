package com.resort.goods.apps.goods;

import com.disney.domain.sdk.anno.AbsBiz;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import com.disney.domain.sdk.factory.IDomainBiz;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;

/**
 * @author issavior
 */
@AbsBiz(value = DomainAbsBizType.GOODS)
public abstract class HomeDomainBiz implements IDomainBiz {

    public abstract GoodsHomeRenderDTO homeRender();

}

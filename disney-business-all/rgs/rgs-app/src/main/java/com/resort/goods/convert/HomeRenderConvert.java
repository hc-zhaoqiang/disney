package com.resort.goods.convert;

import com.resort.goods.api.mall.home.response.HomeRenderDTO;
import com.resort.goods.domain.model.element.Element;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
public class HomeRenderConvert {

    public static List<HomeRenderDTO> fromHome(List<Element> elements) {
        return elements.stream()
                .filter(Element::isShow)
                .map(h -> HomeRenderDTO.builder()
                        .type(h.getElementType().getCode())
                        .title(h.getTitle())
                        .context(h.getContext())
                        .build())
                .collect(Collectors.toList());
    }
}

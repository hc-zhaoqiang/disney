package com.resort.goods.apps.goods.ticket;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.item.request.ItemDetailRenderRequest;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.apps.goods.ItemDetailDomainBiz;
import com.resort.goods.apps.goods.ticket.convert.TicketConvert;
import com.resort.goods.domain.gateway.ticket.TicketGateway;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@Biz(DomainBizType.TICKET)
public class TicketItemDetailDomainBiz extends ItemDetailDomainBiz {

    @Resource
    private TicketGateway ticketGateway;

    @Override
    public List<ItemDetailRenderDTO> itemDetailRender(ItemDetailRenderRequest request) {
        return TicketConvert.toItemDetailRenderDTO(ticketGateway.getTicketSubById(request.getGoodsSubConfigId()));

    }
}

package com.resort.goods.apps.goods.agree.convert;

import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.api.mall.home.response.GoodsTabulationDTO;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.domain.model.agree.Agree;
import com.resort.goods.domain.model.agree.AgreeSubSub;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
public class AgreeConvert {

    public static GoodsHomeRenderDTO toGoodsHomeRenderDTO(List<Agree> agrees) {
        return GoodsHomeRenderDTO.builder()
                .goodsType(DomainBizType.AGREE.getCode())
                .goodsEntries(agrees.stream().map(a -> GoodsTabulationDTO.builder().build()).collect(Collectors.toList()))
                .build();
    }

    public static List<ItemDetailRenderDTO> toItemDetailRenderDTO(Agree agree) {
        return agree.getAgreeSubSubs().get().stream()
                .map(subSub -> ItemDetailRenderDTO.builder().build())
                .collect(Collectors.toList());
    }

    public static OrderValidateRenderDTO toOrderValidateRenderDTO(AgreeSubSub agreeSubSub) {
        return OrderValidateRenderDTO.builder().build();
    }

    public static OrderFillRenderDTO toOrderFillRenderDTO(AgreeSubSub agreeSubSub) {
        return OrderFillRenderDTO.builder().build();
    }
}

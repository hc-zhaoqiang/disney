package com.resort.goods.apps.goods.ticket.convert;

import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.api.mall.home.response.GoodsTabulationDTO;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.domain.model.ticket.Ticket;
import com.resort.goods.domain.model.ticket.TicketSubSub;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
public class TicketConvert {

    public static GoodsHomeRenderDTO toGoodsHomeRenderDTO(List<Ticket> tickets) {
        return GoodsHomeRenderDTO.builder()
                .goodsType(DomainBizType.TICKET.getCode())
                .goodsEntries(tickets.stream().map(a -> GoodsTabulationDTO.builder().build()).collect(Collectors.toList()))
                .build();
    }

    public static List<ItemDetailRenderDTO> toItemDetailRenderDTO(Ticket ticket) {
        return ticket.getTicketSubSubs().get().stream()
                .map(subSub -> ItemDetailRenderDTO.builder().build())
                .collect(Collectors.toList());
    }

    public static OrderValidateRenderDTO toOrderValidateRenderDTO(TicketSubSub ticketSubSub) {
        return OrderValidateRenderDTO.builder().build();
    }

    public static OrderFillRenderDTO toOrderFillRenderDTO(TicketSubSub ticketSubSub) {
        return OrderFillRenderDTO.builder().build();
    }
}

package com.resort.goods.apps.goods.hotel;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.order.request.OrderFillRenderRequest;
import com.resort.goods.api.mall.order.request.OrderValidateRenderRequest;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.apps.goods.OrderRenderDomainBiz;
import com.resort.goods.apps.goods.hotel.convert.HotelConvert;
import com.resort.goods.domain.gateway.hotel.HotelGateway;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author issavior
 */
@Biz(DomainBizType.HOTEL)
public class HotelOrderRenderDomainBiz extends OrderRenderDomainBiz {

    @Resource
    private HotelGateway hotelGateway;

    @Override
    public OrderValidateRenderDTO orderValidateRender(OrderValidateRenderRequest request) {
        return HotelConvert.toOrderValidateRenderDTO(hotelGateway.getHotelSubById(request.getSubId())
                .getHotelSubSubs().get().stream()
                .filter(subSub -> Objects.equals(subSub.getId(), request.getSubSubId()))
                .findFirst()
                .orElse(null));
    }

    @Override
    public OrderFillRenderDTO orderFillRender(OrderFillRenderRequest request) {
        return HotelConvert.toOrderFillRenderDTO(hotelGateway.getHotelSubById(request.getSubId())
                .getHotelSubSubs().get().stream()
                .filter(subSub -> Objects.equals(subSub.getId(), request.getSubSubId()))
                .findFirst()
                .orElse(null));
    }
}

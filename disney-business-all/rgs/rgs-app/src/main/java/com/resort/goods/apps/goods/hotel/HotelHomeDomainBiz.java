package com.resort.goods.apps.goods.hotel;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.apps.goods.HomeDomainBiz;
import com.resort.goods.domain.gateway.hotel.HotelGateway;
import com.resort.goods.apps.goods.hotel.convert.HotelConvert;

import javax.annotation.Resource;

/**
 * @author issavior
 */
@Biz(DomainBizType.HOTEL)
public class HotelHomeDomainBiz extends HomeDomainBiz {

    @Resource
    private HotelGateway hotelGateway;

    @Override
    public GoodsHomeRenderDTO homeRender() {
        return HotelConvert.toGoodsHomeRenderDTO(hotelGateway.getHotels());
    }

}

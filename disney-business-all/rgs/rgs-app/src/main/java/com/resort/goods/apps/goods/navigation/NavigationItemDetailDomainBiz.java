package com.resort.goods.apps.goods.navigation;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.item.request.ItemDetailRenderRequest;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.apps.goods.ItemDetailDomainBiz;
import com.resort.goods.apps.goods.navigation.convert.NavigationConvert;
import com.resort.goods.domain.gateway.navigation.NavigationGateway;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@Biz(DomainBizType.TICKET)
public class NavigationItemDetailDomainBiz extends ItemDetailDomainBiz {

    @Resource
    private NavigationGateway navigationGateway;

    @Override
    public List<ItemDetailRenderDTO> itemDetailRender(ItemDetailRenderRequest request) {
        return NavigationConvert.toItemDetailRenderDTO(navigationGateway.getNavigationSubById(request.getGoodsSubConfigId()));
    }
}

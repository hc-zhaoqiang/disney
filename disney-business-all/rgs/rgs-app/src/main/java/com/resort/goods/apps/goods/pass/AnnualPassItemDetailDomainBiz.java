package com.resort.goods.apps.goods.pass;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.item.request.ItemDetailRenderRequest;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.apps.goods.ItemDetailDomainBiz;
import com.resort.goods.apps.goods.pass.convert.AnnualPassConvert;
import com.resort.goods.domain.gateway.pass.AnnualPassGateway;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@Biz(DomainBizType.TICKET)
public class AnnualPassItemDetailDomainBiz extends ItemDetailDomainBiz {

    @Resource
    private AnnualPassGateway annualPassGateway;

    @Override
    public List<ItemDetailRenderDTO> itemDetailRender(ItemDetailRenderRequest request) {
        return AnnualPassConvert.toItemDetailRenderDTO(annualPassGateway.getAnnualPassSubById(request.getGoodsSubConfigId()));
    }
}

package com.resort.goods.apps.goods.navigation;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.order.request.OrderFillRenderRequest;
import com.resort.goods.api.mall.order.request.OrderValidateRenderRequest;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.apps.goods.OrderRenderDomainBiz;
import com.resort.goods.apps.goods.navigation.convert.NavigationConvert;
import com.resort.goods.domain.gateway.navigation.NavigationGateway;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author issavior
 */
@Biz(DomainBizType.NAVIGATION)
public class NavigationOrderRenderDomainBiz extends OrderRenderDomainBiz {

    @Resource
    private NavigationGateway navigationGateway;

    @Override
    public OrderValidateRenderDTO orderValidateRender(OrderValidateRenderRequest request) {
        return NavigationConvert.toOrderValidateRenderDTO(navigationGateway.getNavigationSubById(request.getSubId())
                .getNavigationSubSubs().get().stream()
                .filter(subSub -> Objects.equals(subSub.getId(), request.getSubSubId()))
                .findFirst()
                .orElse(null));
    }

    @Override
    public OrderFillRenderDTO orderFillRender(OrderFillRenderRequest request) {
        return NavigationConvert.toOrderFillRenderDTO(navigationGateway.getNavigationSubById(request.getSubId())
                .getNavigationSubSubs().get().stream()
                .filter(subSub -> Objects.equals(subSub.getId(), request.getSubSubId()))
                .findFirst()
                .orElse(null));
    }
}

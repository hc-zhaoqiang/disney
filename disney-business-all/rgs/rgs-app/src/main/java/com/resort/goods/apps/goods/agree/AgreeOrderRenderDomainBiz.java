package com.resort.goods.apps.goods.agree;

import com.disney.domain.sdk.anno.Biz;
import com.disney.domain.sdk.enums.DomainBizType;
import com.resort.goods.api.mall.order.request.OrderFillRenderRequest;
import com.resort.goods.api.mall.order.request.OrderValidateRenderRequest;
import com.resort.goods.api.mall.order.response.OrderFillRenderDTO;
import com.resort.goods.api.mall.order.response.OrderValidateRenderDTO;
import com.resort.goods.apps.goods.OrderRenderDomainBiz;
import com.resort.goods.apps.goods.agree.convert.AgreeConvert;
import com.resort.goods.domain.gateway.agree.AgreeGateway;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author issavior
 */
@Biz(DomainBizType.AGREE)
public class AgreeOrderRenderDomainBiz extends OrderRenderDomainBiz {

    @Resource
    private AgreeGateway agreeGateway;

    @Override
    public OrderValidateRenderDTO orderValidateRender(OrderValidateRenderRequest request) {
        return AgreeConvert.toOrderValidateRenderDTO(agreeGateway.getAgreeSubById(request.getSubId())
                .getAgreeSubSubs().get().stream()
                .filter(subSub -> Objects.equals(subSub.getId(), request.getSubSubId()))
                .findFirst()
                .orElse(null));
    }

    @Override
    public OrderFillRenderDTO orderFillRender(OrderFillRenderRequest request) {
        return AgreeConvert.toOrderFillRenderDTO(agreeGateway.getAgreeSubById(request.getSubId())
                .getAgreeSubSubs().get().stream()
                .filter(subSub -> Objects.equals(subSub.getId(), request.getSubSubId()))
                .findFirst()
                .orElse(null));
    }
}

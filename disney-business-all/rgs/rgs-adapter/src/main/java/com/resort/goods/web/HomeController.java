package com.resort.goods.web;

import com.disney.domain.sdk.common.DisneyResult;
import com.resort.goods.api.mall.home.HomeRenderService;
import com.resort.goods.api.mall.item.ItemDetailRenderService;
import com.resort.goods.api.mall.item.request.ItemDetailRenderRequest;
import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.api.mall.home.response.HomeRenderDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/app/home")
public class HomeController {

    @Resource
    private HomeRenderService homeRenderService;
    @Resource
    private ItemDetailRenderService itemDetailRenderService;

    /**
     * 商城首页
     *
     * @return 渲染数据
     */
    @GetMapping("/render")
    public ResponseEntity<Object> homeRender() {
        DisneyResult<List<HomeRenderDTO>> homes = homeRenderService.getHomes();
        return ResponseEntity.ok(homes);
    }

    @GetMapping("/items")
    public ResponseEntity<Object> items() {
        DisneyResult<List<GoodsHomeRenderDTO>> homes = homeRenderService.getItems();
        return ResponseEntity.ok(homes);
    }

    @GetMapping("/items")
    public ResponseEntity<Object> items(ItemDetailRenderRequest request) {
        return ResponseEntity.ok(itemDetailRenderService.getItemDetailRender(request));
    }


}

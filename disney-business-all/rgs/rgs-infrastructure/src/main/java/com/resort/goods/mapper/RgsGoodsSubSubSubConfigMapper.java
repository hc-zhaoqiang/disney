package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsGoodsSubSubSubConfig;
import com.resort.goods.pojo.RgsGoodsSubSubSubConfigExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsGoodsSubSubSubConfigMapper {
    long countByExample(RgsGoodsSubSubSubConfigExample example);

    int deleteByExample(RgsGoodsSubSubSubConfigExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsGoodsSubSubSubConfig record);

    int insertSelective(RgsGoodsSubSubSubConfig record);

    List<RgsGoodsSubSubSubConfig> selectByExample(RgsGoodsSubSubSubConfigExample example);

    RgsGoodsSubSubSubConfig selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsGoodsSubSubSubConfig record, @Param("example") RgsGoodsSubSubSubConfigExample example);

    int updateByExample(@Param("record") RgsGoodsSubSubSubConfig record, @Param("example") RgsGoodsSubSubSubConfigExample example);

    int updateByPrimaryKeySelective(RgsGoodsSubSubSubConfig record);

    int updateByPrimaryKey(RgsGoodsSubSubSubConfig record);
}
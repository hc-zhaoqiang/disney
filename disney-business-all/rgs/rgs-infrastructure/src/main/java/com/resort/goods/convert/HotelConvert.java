package com.resort.goods.convert;

import com.disney.domain.sdk.anno.DomainConvert;
import com.disney.domain.sdk.common.Lazy;
import com.resort.goods.domain.gateway.hotel.HotelSubSubGateway;
import com.resort.goods.domain.gateway.hotel.HotelSubSubSubGateway;
import com.resort.goods.domain.model.hotel.Hotel;
import com.resort.goods.pojo.RgsGoodsSubConfig;
import com.resort.goods.pojo.RgsGoodsSubSubConfig;
import com.resort.goods.pojo.RgsGoodsSubSubSubConfig;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;
import com.resort.goods.domain.model.hotel.HotelSubSubSub;
import com.resort.goods.domain.model.hotel.HotelSubSub;

/**
 * @author issavior
 */
@DomainConvert
public class HotelConvert {

    @Resource
    private HotelSubSubGateway hotelSubSubGateway;
    @Resource
    private HotelSubSubSubGateway hotelSubSubSubGateway;

    public Hotel fromRgsGoodsSub(RgsGoodsSubConfig ticketSub) {

        return Hotel.builder()
                .hotelSubSubs(Lazy.of(() -> hotelSubSubGateway.getHotelsBySubId(ticketSub.getId())))
                .build();
    }

    public List<Hotel> fromRgsGoodsSubs(List<RgsGoodsSubConfig> ticketSubs) {


        return ticketSubs.stream()
                .map(ticketSub -> Hotel.builder()
                        .hotelSubSubs(Lazy.of(() -> hotelSubSubGateway.getHotelsBySubId(ticketSub.getId())))
                        .build())
                .collect(Collectors.toList());
    }

    public List<HotelSubSubSub> fromRgsGoodsSubSubSubs(List<RgsGoodsSubSubSubConfig> ticketSubSubSubs) {
        return ticketSubSubSubs.stream()
                .map(ticketSubSubSub -> HotelSubSubSub.builder()
                        .build())
                .collect(Collectors.toList());
    }

    public List<HotelSubSub> fromRgsGoodsSubSubs(List<RgsGoodsSubSubConfig> ticketSubSubs) {
        return ticketSubSubs.stream()
                .map(ticketSubSub -> HotelSubSub.builder()
                        .hotelSubSubSubs(Lazy.of(() -> hotelSubSubSubGateway.getHotelsBySubSubId(ticketSubSub.getId())))
                        .build())
                .collect(Collectors.toList());
    }
}

package com.resort.goods.customer;

import lombok.Data;

/**
 * @author issavior
 */
@Data
public class CustomerDO{
  private String customerId;
  private String memberId;
  private String globalId;
  private long registeredCapital;
  private String companyName;
}

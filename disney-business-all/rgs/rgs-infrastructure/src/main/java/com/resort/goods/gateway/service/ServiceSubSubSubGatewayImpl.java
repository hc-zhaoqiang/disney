package com.resort.goods.gateway.service;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.convert.ServiceConvert;
import com.resort.goods.domain.gateway.service.ServiceSubSubSubGateway;
import com.resort.goods.domain.model.service.ServiceSubSubSub;
import com.resort.goods.mapper.RgsGoodsSubSubSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsSubSubSubConfigExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class ServiceSubSubSubGatewayImpl implements ServiceSubSubSubGateway {

    @Resource
    private RgsGoodsSubSubSubConfigMapper rgsGoodsSubSubSubConfigMapper;
    @Resource
    private ServiceConvert serviceConvert;

    @Override
    public List<ServiceSubSubSub> getServiceSubSubSubs() {
        return null;
    }

    @Override
    public List<ServiceSubSubSub> getServicesBySubSubId() {
        return null;
    }

    @Override
    public List<ServiceSubSubSub> getServicesBySubSubId(Long id) {
        RgsGoodsSubSubSubConfigExample example = new RgsGoodsSubSubSubConfigExample();
        example.createCriteria().andGoodsSubSubIdEqualTo(id);
        return serviceConvert.fromRgsGoodsSubSubSubs(rgsGoodsSubSubSubConfigMapper.selectByExample(example));
    }
}

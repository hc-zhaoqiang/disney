package com.resort.goods.gateway.navigation;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.convert.NavigationConvert;
import com.resort.goods.domain.gateway.navigation.NavigationSubSubGateway;
import com.resort.goods.domain.model.navigation.NavigationSubSub;
import com.resort.goods.mapper.RgsGoodsSubSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsSubSubConfigExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class NavigationSubSubGatewayImpl implements NavigationSubSubGateway {

    @Resource
    private RgsGoodsSubSubConfigMapper rgsGoodsSubSubConfigMapper;
    @Resource
    private NavigationConvert navigationConvert;


    @Override
    public List<NavigationSubSub> getNavigationsBySubId(Long id) {
        RgsGoodsSubSubConfigExample example = new RgsGoodsSubSubConfigExample();
        example.createCriteria().andGoodsSubIdEqualTo(id);
        return navigationConvert.fromRgsGoodsSubSubs(rgsGoodsSubSubConfigMapper.selectByExample(example));
    }

    @Override
    public List<NavigationSubSub> getNavigations() {
        return null;
    }
}

package com.resort.goods.gateway.pass;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.convert.AnnualPassConvert;
import com.resort.goods.domain.gateway.pass.AnnualPassSubSubSubGateway;
import com.resort.goods.domain.model.pass.AnnualPassSubSubSub;
import com.resort.goods.mapper.RgsGoodsSubSubSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsSubSubSubConfigExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class AnnualPassSubSubSubGatewayImpl implements AnnualPassSubSubSubGateway {

    @Resource
    private RgsGoodsSubSubSubConfigMapper rgsGoodsSubSubSubConfigMapper;
    @Resource
    private AnnualPassConvert annualPassConvert;

    @Override
    public List<AnnualPassSubSubSub> getAnnualPassSubSubSubs() {
        return null;
    }

    @Override
    public List<AnnualPassSubSubSub> getAnnualPasssBySubSubId() {
        return null;
    }

    @Override
    public List<AnnualPassSubSubSub> getAnnualPasssBySubSubId(Long id) {
        RgsGoodsSubSubSubConfigExample example = new RgsGoodsSubSubSubConfigExample();
        example.createCriteria().andGoodsSubSubIdEqualTo(id);
        return annualPassConvert.fromRgsGoodsSubSubSubs(rgsGoodsSubSubSubConfigMapper.selectByExample(example));
    }
}

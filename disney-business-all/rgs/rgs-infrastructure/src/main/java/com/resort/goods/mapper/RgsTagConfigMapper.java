package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsTagConfig;
import com.resort.goods.pojo.RgsTagConfigExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsTagConfigMapper {
    long countByExample(RgsTagConfigExample example);

    int deleteByExample(RgsTagConfigExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsTagConfig record);

    int insertSelective(RgsTagConfig record);

    List<RgsTagConfig> selectByExample(RgsTagConfigExample example);

    RgsTagConfig selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsTagConfig record, @Param("example") RgsTagConfigExample example);

    int updateByExample(@Param("record") RgsTagConfig record, @Param("example") RgsTagConfigExample example);

    int updateByPrimaryKeySelective(RgsTagConfig record);

    int updateByPrimaryKey(RgsTagConfig record);
}
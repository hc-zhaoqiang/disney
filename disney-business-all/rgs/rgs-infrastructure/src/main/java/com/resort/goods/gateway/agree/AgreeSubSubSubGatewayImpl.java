package com.resort.goods.gateway.agree;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.convert.AgreeConvert;
import com.resort.goods.domain.gateway.agree.AgreeSubSubSubGateway;
import com.resort.goods.domain.model.agree.AgreeSubSubSub;
import com.resort.goods.mapper.RgsGoodsSubSubSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsSubSubSubConfigExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class AgreeSubSubSubGatewayImpl implements AgreeSubSubSubGateway {

    @Resource
    private RgsGoodsSubSubSubConfigMapper rgsGoodsSubSubSubConfigMapper;
    @Resource
    private AgreeConvert agreeConvert;

    @Override
    public List<AgreeSubSubSub> getAgreeSubSubSubs() {
        return null;
    }

    @Override
    public List<AgreeSubSubSub> getAgreesBySubSubId() {
        return null;
    }

    @Override
    public List<AgreeSubSubSub> getAgreesBySubSubId(Long id) {
        RgsGoodsSubSubSubConfigExample example = new RgsGoodsSubSubSubConfigExample();
        example.createCriteria().andGoodsSubSubIdEqualTo(id);
        return agreeConvert.fromRgsGoodsSubSubSubs(rgsGoodsSubSubSubConfigMapper.selectByExample(example));
    }
}

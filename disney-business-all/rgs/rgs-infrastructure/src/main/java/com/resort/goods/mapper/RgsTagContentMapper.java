package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsTagContent;
import com.resort.goods.pojo.RgsTagContentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsTagContentMapper {
    long countByExample(RgsTagContentExample example);

    int deleteByExample(RgsTagContentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsTagContent record);

    int insertSelective(RgsTagContent record);

    List<RgsTagContent> selectByExample(RgsTagContentExample example);

    RgsTagContent selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsTagContent record, @Param("example") RgsTagContentExample example);

    int updateByExample(@Param("record") RgsTagContent record, @Param("example") RgsTagContentExample example);

    int updateByPrimaryKeySelective(RgsTagContent record);

    int updateByPrimaryKey(RgsTagContent record);
}
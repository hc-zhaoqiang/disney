package com.resort.goods.gateway.navigation;

import com.disney.domain.sdk.anno.DomainGateway;
import com.disney.domain.sdk.enums.DomainBizType;
import com.google.common.collect.Lists;
import com.resort.goods.convert.NavigationConvert;
import com.resort.goods.domain.enums.CommonFlag;
import com.resort.goods.domain.gateway.navigation.NavigationGateway;
import com.resort.goods.domain.model.navigation.Navigation;
import com.resort.goods.mapper.RgsGoodsReleaseMapper;
import com.resort.goods.mapper.RgsGoodsSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsRelease;
import com.resort.goods.pojo.RgsGoodsReleaseExample;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
@DomainGateway
public class NavigationGatewayImpl implements NavigationGateway {
    @Resource
    private RgsGoodsReleaseMapper rgsGoodsReleaseMapper;
    @Resource
    private RgsGoodsSubConfigMapper rgsGoodsSubConfigMapper;
    @Resource
    private NavigationConvert navigationConvert;
    @Override
    public List<Navigation> getNavigations() {
        RgsGoodsReleaseExample example = new RgsGoodsReleaseExample();
        example.createCriteria()
                .andGroundingEqualTo(CommonFlag.GROUNDING)
                .andGoodsTypeEqualTo(DomainBizType.NAVIGATION.getCode());

        return navigationConvert.fromRgsGoodsSubs(Optional
                .ofNullable(rgsGoodsReleaseMapper.selectByExample(example))
                .orElse(Lists.newArrayList()).stream()
                .map(RgsGoodsRelease::getGoodsSubId)
                .map(rgsGoodsSubConfigMapper::selectByPrimaryKey)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
    }

    @Override
    public Navigation getNavigationSubById(Long id) {
        return navigationConvert.fromRgsGoodsSub(rgsGoodsSubConfigMapper.selectByPrimaryKey(id));

    }
}

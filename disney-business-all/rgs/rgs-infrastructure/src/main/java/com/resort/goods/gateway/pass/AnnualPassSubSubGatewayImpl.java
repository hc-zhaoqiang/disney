package com.resort.goods.gateway.pass;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.convert.AnnualPassConvert;
import com.resort.goods.domain.gateway.pass.AnnualPassSubSubGateway;
import com.resort.goods.domain.model.pass.AnnualPassSubSub;
import com.resort.goods.mapper.RgsGoodsSubSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsSubSubConfigExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class AnnualPassSubSubGatewayImpl implements AnnualPassSubSubGateway {

    @Resource
    private RgsGoodsSubSubConfigMapper rgsGoodsSubSubConfigMapper;
    @Resource
    private AnnualPassConvert annualPassConvert;


    @Override
    public List<AnnualPassSubSub> getAnnualPassBySubId(Long id) {
        RgsGoodsSubSubConfigExample example = new RgsGoodsSubSubConfigExample();
        example.createCriteria().andGoodsSubIdEqualTo(id);
        return annualPassConvert.fromRgsGoodsSubSubs(rgsGoodsSubSubConfigMapper.selectByExample(example));
    }

    @Override
    public List<AnnualPassSubSub> getAnnualPass() {
        return null;
    }
}

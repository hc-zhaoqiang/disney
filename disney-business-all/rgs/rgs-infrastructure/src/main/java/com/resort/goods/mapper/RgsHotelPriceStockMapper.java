package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsHotelPriceStock;
import com.resort.goods.pojo.RgsHotelPriceStockExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsHotelPriceStockMapper {
    long countByExample(RgsHotelPriceStockExample example);

    int deleteByExample(RgsHotelPriceStockExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsHotelPriceStock record);

    int insertSelective(RgsHotelPriceStock record);

    List<RgsHotelPriceStock> selectByExample(RgsHotelPriceStockExample example);

    RgsHotelPriceStock selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsHotelPriceStock record, @Param("example") RgsHotelPriceStockExample example);

    int updateByExample(@Param("record") RgsHotelPriceStock record, @Param("example") RgsHotelPriceStockExample example);

    int updateByPrimaryKeySelective(RgsHotelPriceStock record);

    int updateByPrimaryKey(RgsHotelPriceStock record);
}
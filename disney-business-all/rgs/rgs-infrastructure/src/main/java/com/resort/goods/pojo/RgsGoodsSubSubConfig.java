package com.resort.goods.pojo;

public class RgsGoodsSubSubConfig {
    private Long id;

    private Long goodsSubId;

    private String name;

    private Integer goodsType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsSubId() {
        return goodsSubId;
    }

    public void setGoodsSubId(Long goodsSubId) {
        this.goodsSubId = goodsSubId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(Integer goodsType) {
        this.goodsType = goodsType;
    }
}
package com.resort.goods.gateway.element;

import com.google.common.collect.Lists;
import com.resort.goods.convert.HomeConvert;
import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.domain.enums.ElementType;
import com.resort.goods.domain.gateway.element.ElementGateway;
import com.resort.goods.domain.enums.CommonFlag;
import com.resort.goods.domain.model.element.Element;
import com.resort.goods.mapper.RgsGoodsElementMapper;
import com.resort.goods.pojo.RgsGoodsElementExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class ElementGatewayImpl implements ElementGateway {

    @Resource
    private RgsGoodsElementMapper rgsGoodsElementMapper;

    @Override
    public List<Element> getHomeElements() {
        RgsGoodsElementExample example = new RgsGoodsElementExample();
        example.createCriteria()
                .andEffectiveEqualTo(CommonFlag.EFFECTIVE)
                .andTypeIn(Lists.newArrayList(ElementType.AGREE.getCode(),
                        ElementType.TIPS.getCode(),
                        ElementType.NOTICE.getCode(),
                        ElementType.PICTURE.getCode(),
                        ElementType.NAVIGATION.getCode()));
        return HomeConvert.fromRgsGoodsHomes(rgsGoodsElementMapper.selectByExample(example));
    }
}

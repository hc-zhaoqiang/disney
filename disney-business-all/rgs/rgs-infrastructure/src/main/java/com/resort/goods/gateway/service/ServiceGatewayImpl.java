package com.resort.goods.gateway.service;

import com.disney.domain.sdk.anno.DomainGateway;
import com.disney.domain.sdk.enums.DomainBizType;
import com.google.common.collect.Lists;
import com.resort.goods.convert.ServiceConvert;
import com.resort.goods.domain.enums.CommonFlag;
import com.resort.goods.domain.gateway.service.ServiceGateway;
import com.resort.goods.domain.model.service.Service;
import com.resort.goods.mapper.RgsGoodsReleaseMapper;
import com.resort.goods.mapper.RgsGoodsSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsRelease;
import com.resort.goods.pojo.RgsGoodsReleaseExample;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
@DomainGateway
public class ServiceGatewayImpl implements ServiceGateway {

    @Resource
    private RgsGoodsReleaseMapper rgsGoodsReleaseMapper;
    @Resource
    private RgsGoodsSubConfigMapper rgsGoodsSubConfigMapper;
    @Resource
    private ServiceConvert serviceConvert;

    @Override
    public List<Service> getServices() {

        RgsGoodsReleaseExample example = new RgsGoodsReleaseExample();
        example.createCriteria()
                .andGroundingEqualTo(CommonFlag.GROUNDING)
                .andGoodsTypeEqualTo(DomainBizType.SERVICE.getCode());

        return serviceConvert.fromRgsGoodsSubs(Optional
                .ofNullable(rgsGoodsReleaseMapper.selectByExample(example))
                .orElse(Lists.newArrayList()).stream()
                .map(RgsGoodsRelease::getGoodsSubId)
                .map(rgsGoodsSubConfigMapper::selectByPrimaryKey)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
    }

    @Override
    public Service getServiceSubById(Long id) {
        return serviceConvert.fromRgsGoodsSub(rgsGoodsSubConfigMapper.selectByPrimaryKey(id));

    }
}

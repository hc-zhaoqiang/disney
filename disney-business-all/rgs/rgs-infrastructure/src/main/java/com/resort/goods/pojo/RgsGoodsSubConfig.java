package com.resort.goods.pojo;

public class RgsGoodsSubConfig {
    private Long id;

    private Long goodsId;

    private String name;

    private String desc;

    private Long titleTagId;

    private String descTagId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Long getTitleTagId() {
        return titleTagId;
    }

    public void setTitleTagId(Long titleTagId) {
        this.titleTagId = titleTagId;
    }

    public String getDescTagId() {
        return descTagId;
    }

    public void setDescTagId(String descTagId) {
        this.descTagId = descTagId;
    }
}
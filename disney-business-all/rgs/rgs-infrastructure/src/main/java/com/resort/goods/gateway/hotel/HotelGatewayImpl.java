package com.resort.goods.gateway.hotel;

import com.disney.domain.sdk.anno.DomainGateway;
import com.disney.domain.sdk.enums.DomainBizType;
import com.google.common.collect.Lists;
import com.resort.goods.convert.HotelConvert;
import com.resort.goods.domain.enums.CommonFlag;
import com.resort.goods.domain.gateway.hotel.HotelGateway;
import com.resort.goods.domain.model.hotel.Hotel;
import com.resort.goods.mapper.RgsGoodsReleaseMapper;
import com.resort.goods.mapper.RgsGoodsSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsRelease;
import com.resort.goods.pojo.RgsGoodsReleaseExample;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
@DomainGateway
public class HotelGatewayImpl implements HotelGateway {
    @Resource
    private RgsGoodsReleaseMapper rgsGoodsReleaseMapper;
    @Resource
    private RgsGoodsSubConfigMapper rgsGoodsSubConfigMapper;
    @Resource
    private HotelConvert hotelConvert;
    @Override
    public List<Hotel> getHotels() {
        RgsGoodsReleaseExample example = new RgsGoodsReleaseExample();
        example.createCriteria()
                .andGroundingEqualTo(CommonFlag.GROUNDING)
                .andGoodsTypeEqualTo(DomainBizType.HOTEL.getCode());

        return hotelConvert.fromRgsGoodsSubs(Optional
                .ofNullable(rgsGoodsReleaseMapper.selectByExample(example))
                .orElse(Lists.newArrayList()).stream()
                .map(RgsGoodsRelease::getGoodsSubId)
                .map(rgsGoodsSubConfigMapper::selectByPrimaryKey)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
    }

    @Override
    public Hotel getHotelSubById(Long id) {
        return hotelConvert.fromRgsGoodsSub(rgsGoodsSubConfigMapper.selectByPrimaryKey(id));

    }
}

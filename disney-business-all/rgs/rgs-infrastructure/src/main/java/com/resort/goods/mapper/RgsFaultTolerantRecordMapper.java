package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsFaultTolerantRecord;
import com.resort.goods.pojo.RgsFaultTolerantRecordExample;
import com.resort.goods.pojo.RgsFaultTolerantRecordWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsFaultTolerantRecordMapper {
    long countByExample(RgsFaultTolerantRecordExample example);

    int deleteByExample(RgsFaultTolerantRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsFaultTolerantRecordWithBLOBs record);

    int insertSelective(RgsFaultTolerantRecordWithBLOBs record);

    List<RgsFaultTolerantRecordWithBLOBs> selectByExampleWithBLOBs(RgsFaultTolerantRecordExample example);

    List<RgsFaultTolerantRecord> selectByExample(RgsFaultTolerantRecordExample example);

    RgsFaultTolerantRecordWithBLOBs selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsFaultTolerantRecordWithBLOBs record, @Param("example") RgsFaultTolerantRecordExample example);

    int updateByExampleWithBLOBs(@Param("record") RgsFaultTolerantRecordWithBLOBs record, @Param("example") RgsFaultTolerantRecordExample example);

    int updateByExample(@Param("record") RgsFaultTolerantRecord record, @Param("example") RgsFaultTolerantRecordExample example);

    int updateByPrimaryKeySelective(RgsFaultTolerantRecordWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(RgsFaultTolerantRecordWithBLOBs record);

    int updateByPrimaryKey(RgsFaultTolerantRecord record);
}
package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsAnnualPassPriceStock;
import com.resort.goods.pojo.RgsAnnualPassPriceStockExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsAnnualPassPriceStockMapper {
    long countByExample(RgsAnnualPassPriceStockExample example);

    int deleteByExample(RgsAnnualPassPriceStockExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsAnnualPassPriceStock record);

    int insertSelective(RgsAnnualPassPriceStock record);

    List<RgsAnnualPassPriceStock> selectByExample(RgsAnnualPassPriceStockExample example);

    RgsAnnualPassPriceStock selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsAnnualPassPriceStock record, @Param("example") RgsAnnualPassPriceStockExample example);

    int updateByExample(@Param("record") RgsAnnualPassPriceStock record, @Param("example") RgsAnnualPassPriceStockExample example);

    int updateByPrimaryKeySelective(RgsAnnualPassPriceStock record);

    int updateByPrimaryKey(RgsAnnualPassPriceStock record);
}
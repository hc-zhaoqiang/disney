package com.resort.goods.pojo;

public class RgsGoodsSubSubSubConfig {
    private Long id;

    private Long goodsSubSubId;

    private String name;

    private String type;

    private String desc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsSubSubId() {
        return goodsSubSubId;
    }

    public void setGoodsSubSubId(Long goodsSubSubId) {
        this.goodsSubSubId = goodsSubSubId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
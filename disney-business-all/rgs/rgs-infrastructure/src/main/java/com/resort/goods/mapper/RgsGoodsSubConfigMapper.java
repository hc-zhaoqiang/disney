package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsGoodsSubConfig;
import com.resort.goods.pojo.RgsGoodsSubConfigExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsGoodsSubConfigMapper {
    long countByExample(RgsGoodsSubConfigExample example);

    int deleteByExample(RgsGoodsSubConfigExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsGoodsSubConfig record);

    int insertSelective(RgsGoodsSubConfig record);

    List<RgsGoodsSubConfig> selectByExample(RgsGoodsSubConfigExample example);

    RgsGoodsSubConfig selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsGoodsSubConfig record, @Param("example") RgsGoodsSubConfigExample example);

    int updateByExample(@Param("record") RgsGoodsSubConfig record, @Param("example") RgsGoodsSubConfigExample example);

    int updateByPrimaryKeySelective(RgsGoodsSubConfig record);

    int updateByPrimaryKey(RgsGoodsSubConfig record);
}
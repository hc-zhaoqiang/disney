package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsNavigationPriceStock;
import com.resort.goods.pojo.RgsNavigationPriceStockExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsNavigationPriceStockMapper {
    long countByExample(RgsNavigationPriceStockExample example);

    int deleteByExample(RgsNavigationPriceStockExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsNavigationPriceStock record);

    int insertSelective(RgsNavigationPriceStock record);

    List<RgsNavigationPriceStock> selectByExample(RgsNavigationPriceStockExample example);

    RgsNavigationPriceStock selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsNavigationPriceStock record, @Param("example") RgsNavigationPriceStockExample example);

    int updateByExample(@Param("record") RgsNavigationPriceStock record, @Param("example") RgsNavigationPriceStockExample example);

    int updateByPrimaryKeySelective(RgsNavigationPriceStock record);

    int updateByPrimaryKey(RgsNavigationPriceStock record);
}
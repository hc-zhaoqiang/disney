package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsGoodsElement;
import com.resort.goods.pojo.RgsGoodsElementExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsGoodsElementMapper {
    long countByExample(RgsGoodsElementExample example);

    int deleteByExample(RgsGoodsElementExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsGoodsElement record);

    int insertSelective(RgsGoodsElement record);

    List<RgsGoodsElement> selectByExampleWithBLOBs(RgsGoodsElementExample example);

    List<RgsGoodsElement> selectByExample(RgsGoodsElementExample example);

    RgsGoodsElement selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsGoodsElement record, @Param("example") RgsGoodsElementExample example);

    int updateByExampleWithBLOBs(@Param("record") RgsGoodsElement record, @Param("example") RgsGoodsElementExample example);

    int updateByExample(@Param("record") RgsGoodsElement record, @Param("example") RgsGoodsElementExample example);

    int updateByPrimaryKeySelective(RgsGoodsElement record);

    int updateByPrimaryKeyWithBLOBs(RgsGoodsElement record);

    int updateByPrimaryKey(RgsGoodsElement record);
}
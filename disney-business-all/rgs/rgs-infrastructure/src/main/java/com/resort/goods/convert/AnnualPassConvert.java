package com.resort.goods.convert;

import com.disney.domain.sdk.anno.DomainConvert;
import com.disney.domain.sdk.common.Lazy;
import com.resort.goods.domain.gateway.pass.AnnualPassSubSubGateway;
import com.resort.goods.domain.gateway.pass.AnnualPassSubSubSubGateway;
import com.resort.goods.domain.model.pass.AnnualPass;
import com.resort.goods.domain.model.pass.AnnualPassSubSub;
import com.resort.goods.domain.model.pass.AnnualPassSubSubSub;
import com.resort.goods.pojo.RgsGoodsSubConfig;
import com.resort.goods.pojo.RgsGoodsSubSubConfig;
import com.resort.goods.pojo.RgsGoodsSubSubSubConfig;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
@DomainConvert
public class AnnualPassConvert {

    @Resource
    private AnnualPassSubSubGateway annualPassSubSubGateway;
    @Resource
    private AnnualPassSubSubSubGateway annualPassSubSubSubGateway;

    public AnnualPass fromRgsGoodsSub(RgsGoodsSubConfig ticketSub) {

        return AnnualPass.builder()
                .annualPassSubSubs(Lazy.of(() -> annualPassSubSubGateway.getAnnualPassBySubId(ticketSub.getId())))
                .build();
    }

    public List<AnnualPass> fromRgsGoodsSubs(List<RgsGoodsSubConfig> ticketSubs) {


        return ticketSubs.stream()
                .map(ticketSub -> AnnualPass.builder()
                        .annualPassSubSubs(Lazy.of(() -> annualPassSubSubGateway.getAnnualPassBySubId(ticketSub.getId())))
                        .build())
                .collect(Collectors.toList());
    }

    public List<AnnualPassSubSubSub> fromRgsGoodsSubSubSubs(List<RgsGoodsSubSubSubConfig> ticketSubSubSubs) {
        return ticketSubSubSubs.stream()
                .map(ticketSubSubSub -> AnnualPassSubSubSub.builder()
                        .build())
                .collect(Collectors.toList());
    }

    public List<AnnualPassSubSub> fromRgsGoodsSubSubs(List<RgsGoodsSubSubConfig> ticketSubSubs) {
        return ticketSubSubs.stream()
                .map(ticketSubSub -> AnnualPassSubSub.builder()
                        .annualPassSubSubSubs(Lazy.of(() -> annualPassSubSubSubGateway.getAnnualPasssBySubSubId(ticketSubSub.getId())))
                        .build())
                .collect(Collectors.toList());
    }
}

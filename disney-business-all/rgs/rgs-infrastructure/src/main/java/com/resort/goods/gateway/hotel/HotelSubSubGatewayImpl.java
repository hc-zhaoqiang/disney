package com.resort.goods.gateway.hotel;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.convert.HotelConvert;
import com.resort.goods.domain.gateway.hotel.HotelSubSubGateway;
import com.resort.goods.domain.model.hotel.HotelSubSub;
import com.resort.goods.mapper.RgsGoodsSubSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsSubSubConfigExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class HotelSubSubGatewayImpl implements HotelSubSubGateway {

    @Resource
    private RgsGoodsSubSubConfigMapper rgsGoodsSubSubConfigMapper;
    @Resource
    private HotelConvert hotelConvert;


    @Override
    public List<HotelSubSub> getHotelsBySubId(Long id) {
        RgsGoodsSubSubConfigExample example = new RgsGoodsSubSubConfigExample();
        example.createCriteria().andGoodsSubIdEqualTo(id);
        return hotelConvert.fromRgsGoodsSubSubs(rgsGoodsSubSubConfigMapper.selectByExample(example));
    }

    @Override
    public List<HotelSubSub> getHotels() {
        return null;
    }
}

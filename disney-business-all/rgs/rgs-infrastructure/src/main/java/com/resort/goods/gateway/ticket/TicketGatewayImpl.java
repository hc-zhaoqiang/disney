package com.resort.goods.gateway.ticket;

import com.disney.domain.sdk.anno.DomainGateway;
import com.disney.domain.sdk.enums.DomainBizType;
import com.google.common.collect.Lists;
import com.resort.goods.convert.TicketConvert;
import com.resort.goods.domain.enums.CommonFlag;
import com.resort.goods.domain.gateway.ticket.TicketGateway;
import com.resort.goods.domain.model.ticket.Ticket;
import com.resort.goods.mapper.RgsGoodsReleaseMapper;
import com.resort.goods.mapper.RgsGoodsSubConfigMapper;
import com.resort.goods.pojo.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
@DomainGateway
public class TicketGatewayImpl implements TicketGateway {

    @Resource
    private RgsGoodsReleaseMapper rgsGoodsReleaseMapper;
    @Resource
    private RgsGoodsSubConfigMapper rgsGoodsSubConfigMapper;
    @Resource
    private TicketConvert ticketConvert;

    @Override
    public List<Ticket> getTicketSubs() {

        RgsGoodsReleaseExample example = new RgsGoodsReleaseExample();
        example.createCriteria()
                .andGroundingEqualTo(CommonFlag.GROUNDING)
                .andGoodsTypeEqualTo(DomainBizType.TICKET.getCode());

        return ticketConvert.fromRgsGoodsSubs(Optional
                .ofNullable(rgsGoodsReleaseMapper.selectByExample(example))
                .orElse(Lists.newArrayList()).stream()
                .map(RgsGoodsRelease::getGoodsSubId)
                .map(rgsGoodsSubConfigMapper::selectByPrimaryKey)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));

    }

    @Override
    public Ticket getTicketSubById(Long id) {
        return ticketConvert.fromRgsGoodsSub(rgsGoodsSubConfigMapper.selectByPrimaryKey(id));
    }
}

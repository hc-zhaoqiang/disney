package com.resort.goods.convert;

import com.disney.domain.sdk.anno.DomainConvert;
import com.resort.goods.domain.enums.ElementType;
import com.resort.goods.domain.model.element.Element;
import com.resort.goods.pojo.RgsGoodsElement;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
@DomainConvert
public class HomeConvert {

    public static List<Element> fromRgsGoodsHomes(List<RgsGoodsElement> rgsGoodsHomes) {

        return rgsGoodsHomes.stream()
                .map(rgs -> Element.builder()
                        .elementType(ElementType.of(rgs.getType()))
                        .title(rgs.getTitle())
                        .show(rgs.getShow())
                        .build())
                .collect(Collectors.toList());
    }
}

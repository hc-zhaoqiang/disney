package com.resort.goods.pojo;

import java.util.Date;

public class RgsGoodsRelease {
    private Long id;

    private String goodsType;

    private Long goodsId;

    private Long goodsSubId;

    private Long goodsSubSubId;

    private Long goodsSubSubSubId;

    private String picture;

    private Integer grounding;

    private Date publishTime;

    private Date offlineTime;

    private Date createTime;

    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(String goodsType) {
        this.goodsType = goodsType;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getGoodsSubId() {
        return goodsSubId;
    }

    public void setGoodsSubId(Long goodsSubId) {
        this.goodsSubId = goodsSubId;
    }

    public Long getGoodsSubSubId() {
        return goodsSubSubId;
    }

    public void setGoodsSubSubId(Long goodsSubSubId) {
        this.goodsSubSubId = goodsSubSubId;
    }

    public Long getGoodsSubSubSubId() {
        return goodsSubSubSubId;
    }

    public void setGoodsSubSubSubId(Long goodsSubSubSubId) {
        this.goodsSubSubSubId = goodsSubSubSubId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getGrounding() {
        return grounding;
    }

    public void setGrounding(Integer grounding) {
        this.grounding = grounding;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public Date getOfflineTime() {
        return offlineTime;
    }

    public void setOfflineTime(Date offlineTime) {
        this.offlineTime = offlineTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
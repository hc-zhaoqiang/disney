package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsGoodsSubSubConfig;
import com.resort.goods.pojo.RgsGoodsSubSubConfigExample;
import com.resort.goods.pojo.RgsGoodsSubSubConfigWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsGoodsSubSubConfigMapper {
    long countByExample(RgsGoodsSubSubConfigExample example);

    int deleteByExample(RgsGoodsSubSubConfigExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsGoodsSubSubConfigWithBLOBs record);

    int insertSelective(RgsGoodsSubSubConfigWithBLOBs record);

    List<RgsGoodsSubSubConfigWithBLOBs> selectByExampleWithBLOBs(RgsGoodsSubSubConfigExample example);

    List<RgsGoodsSubSubConfig> selectByExample(RgsGoodsSubSubConfigExample example);

    RgsGoodsSubSubConfigWithBLOBs selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsGoodsSubSubConfigWithBLOBs record, @Param("example") RgsGoodsSubSubConfigExample example);

    int updateByExampleWithBLOBs(@Param("record") RgsGoodsSubSubConfigWithBLOBs record, @Param("example") RgsGoodsSubSubConfigExample example);

    int updateByExample(@Param("record") RgsGoodsSubSubConfig record, @Param("example") RgsGoodsSubSubConfigExample example);

    int updateByPrimaryKeySelective(RgsGoodsSubSubConfigWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(RgsGoodsSubSubConfigWithBLOBs record);

    int updateByPrimaryKey(RgsGoodsSubSubConfig record);
}
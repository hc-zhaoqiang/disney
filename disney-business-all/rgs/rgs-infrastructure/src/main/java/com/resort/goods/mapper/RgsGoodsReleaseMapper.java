package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsGoodsRelease;
import com.resort.goods.pojo.RgsGoodsReleaseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsGoodsReleaseMapper {
    long countByExample(RgsGoodsReleaseExample example);

    int deleteByExample(RgsGoodsReleaseExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsGoodsRelease record);

    int insertSelective(RgsGoodsRelease record);

    List<RgsGoodsRelease> selectByExample(RgsGoodsReleaseExample example);

    RgsGoodsRelease selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsGoodsRelease record, @Param("example") RgsGoodsReleaseExample example);

    int updateByExample(@Param("record") RgsGoodsRelease record, @Param("example") RgsGoodsReleaseExample example);

    int updateByPrimaryKeySelective(RgsGoodsRelease record);

    int updateByPrimaryKey(RgsGoodsRelease record);
}
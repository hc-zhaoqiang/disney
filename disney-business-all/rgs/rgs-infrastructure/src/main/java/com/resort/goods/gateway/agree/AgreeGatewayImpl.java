package com.resort.goods.gateway.agree;

import com.disney.domain.sdk.anno.DomainGateway;
import com.disney.domain.sdk.enums.DomainBizType;
import com.google.common.collect.Lists;
import com.resort.goods.convert.AgreeConvert;
import com.resort.goods.domain.enums.CommonFlag;
import com.resort.goods.domain.gateway.agree.AgreeGateway;
import com.resort.goods.domain.model.agree.Agree;
import com.resort.goods.mapper.RgsGoodsReleaseMapper;
import com.resort.goods.mapper.RgsGoodsSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsRelease;
import com.resort.goods.pojo.RgsGoodsReleaseExample;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
@DomainGateway
public class AgreeGatewayImpl implements AgreeGateway {
    @Resource
    private RgsGoodsReleaseMapper rgsGoodsReleaseMapper;
    @Resource
    private RgsGoodsSubConfigMapper rgsGoodsSubConfigMapper;
    @Resource
    private AgreeConvert agreeConvert;
    @Override
    public List<Agree> getAgrees() {
        RgsGoodsReleaseExample example = new RgsGoodsReleaseExample();
        example.createCriteria()
                .andGroundingEqualTo(CommonFlag.GROUNDING)
                .andGoodsTypeEqualTo(DomainBizType.AGREE.getCode());

        return agreeConvert.fromRgsGoodsSubs(Optional
                .ofNullable(rgsGoodsReleaseMapper.selectByExample(example))
                .orElse(Lists.newArrayList()).stream()
                .map(RgsGoodsRelease::getGoodsSubId)
                .map(rgsGoodsSubConfigMapper::selectByPrimaryKey)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
    }

    @Override
    public Agree getAgreeSubById(Long id) {
        return null;
    }
}

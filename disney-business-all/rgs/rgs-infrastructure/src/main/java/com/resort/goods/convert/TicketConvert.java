package com.resort.goods.convert;

import com.disney.domain.sdk.anno.DomainConvert;
import com.disney.domain.sdk.common.Lazy;
import com.resort.goods.domain.gateway.ticket.TicketSubSubGateway;
import com.resort.goods.domain.gateway.ticket.TicketSubSubSubGateway;
import com.resort.goods.domain.model.ticket.Ticket;
import com.resort.goods.domain.model.ticket.TicketSubSub;
import com.resort.goods.domain.model.ticket.TicketSubSubSub;
import com.resort.goods.pojo.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
@DomainConvert
public class TicketConvert {

    @Resource
    private TicketSubSubGateway ticketSubSubGateway;
    @Resource
    private TicketSubSubSubGateway ticketSubSubSubGateway;

    public Ticket fromRgsGoodsSub(RgsGoodsSubConfig ticketSub) {

        return Ticket.builder()
                .ticketSubSubs(Lazy.of(() -> ticketSubSubGateway.getTicketsBySubId(ticketSub.getId())))
                .build();
    }

    public List<Ticket> fromRgsGoodsSubs(List<RgsGoodsSubConfig> ticketSubs) {


        return ticketSubs.stream()
                .map(ticketSub -> Ticket.builder()
                        .ticketSubSubs(Lazy.of(() -> ticketSubSubGateway.getTicketsBySubId(ticketSub.getId())))
                        .build())
                .collect(Collectors.toList());
    }

    public List<TicketSubSubSub> fromRgsGoodsSubSubSubs(List<RgsGoodsSubSubSubConfig> ticketSubSubSubs) {
        return ticketSubSubSubs.stream()
                .map(ticketSubSubSub -> TicketSubSubSub.builder()
                        .build())
                .collect(Collectors.toList());
    }

    public List<TicketSubSub> fromRgsGoodsSubSubs(List<RgsGoodsSubSubConfig> ticketSubSubs) {
        return ticketSubSubs.stream()
                .map(ticketSubSub -> TicketSubSub.builder()
                        .ticketSubSubSubs(Lazy.of(() -> ticketSubSubSubGateway.getTicketsBySubSubId(ticketSubSub.getId())))
                        .build())
                .collect(Collectors.toList());
    }
}

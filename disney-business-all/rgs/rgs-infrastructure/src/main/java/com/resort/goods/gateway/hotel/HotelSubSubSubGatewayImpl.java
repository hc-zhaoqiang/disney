package com.resort.goods.gateway.hotel;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.convert.HotelConvert;
import com.resort.goods.domain.gateway.hotel.HotelSubSubSubGateway;
import com.resort.goods.domain.model.hotel.HotelSubSubSub;
import com.resort.goods.mapper.RgsGoodsSubSubSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsSubSubSubConfigExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class HotelSubSubSubGatewayImpl implements HotelSubSubSubGateway {

    @Resource
    private RgsGoodsSubSubSubConfigMapper rgsGoodsSubSubSubConfigMapper;
    @Resource
    private HotelConvert hotelConvert;

    @Override
    public List<HotelSubSubSub> getHotelSubSubSubs() {
        return null;
    }

    @Override
    public List<HotelSubSubSub> getHotelsBySubSubId() {
        return null;
    }

    @Override
    public List<HotelSubSubSub> getHotelsBySubSubId(Long id) {
        RgsGoodsSubSubSubConfigExample example = new RgsGoodsSubSubSubConfigExample();
        example.createCriteria().andGoodsSubSubIdEqualTo(id);
        return hotelConvert.fromRgsGoodsSubSubSubs(rgsGoodsSubSubSubConfigMapper.selectByExample(example));
    }
}

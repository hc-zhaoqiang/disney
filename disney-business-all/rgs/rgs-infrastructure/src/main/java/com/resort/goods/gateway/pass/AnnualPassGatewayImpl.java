package com.resort.goods.gateway.pass;

import com.disney.domain.sdk.anno.DomainGateway;
import com.disney.domain.sdk.enums.DomainBizType;
import com.google.common.collect.Lists;
import com.resort.goods.convert.AnnualPassConvert;
import com.resort.goods.domain.enums.CommonFlag;
import com.resort.goods.domain.gateway.pass.AnnualPassGateway;
import com.resort.goods.domain.model.pass.AnnualPass;
import com.resort.goods.mapper.RgsGoodsReleaseMapper;
import com.resort.goods.mapper.RgsGoodsSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsRelease;
import com.resort.goods.pojo.RgsGoodsReleaseExample;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
@DomainGateway
public class AnnualPassGatewayImpl implements AnnualPassGateway {
    @Resource
    private RgsGoodsReleaseMapper rgsGoodsReleaseMapper;
    @Resource
    private RgsGoodsSubConfigMapper rgsGoodsSubConfigMapper;
    @Resource
    private AnnualPassConvert annualPassConvert;
    @Override
    public List<AnnualPass> getAnnualPasses() {
        RgsGoodsReleaseExample example = new RgsGoodsReleaseExample();
        example.createCriteria()
                .andGroundingEqualTo(CommonFlag.GROUNDING)
                .andGoodsTypeEqualTo(DomainBizType.ANNUAL_PASS.getCode());

        return annualPassConvert.fromRgsGoodsSubs(Optional
                .ofNullable(rgsGoodsReleaseMapper.selectByExample(example))
                .orElse(Lists.newArrayList()).stream()
                .map(RgsGoodsRelease::getGoodsSubId)
                .map(rgsGoodsSubConfigMapper::selectByPrimaryKey)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
    }

    @Override
    public AnnualPass getAnnualPassSubById(Long id) {
        return annualPassConvert.fromRgsGoodsSub(rgsGoodsSubConfigMapper.selectByPrimaryKey(id));

    }
}

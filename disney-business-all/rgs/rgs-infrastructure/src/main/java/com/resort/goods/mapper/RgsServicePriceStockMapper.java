package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsServicePriceStock;
import com.resort.goods.pojo.RgsServicePriceStockExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsServicePriceStockMapper {
    long countByExample(RgsServicePriceStockExample example);

    int deleteByExample(RgsServicePriceStockExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsServicePriceStock record);

    int insertSelective(RgsServicePriceStock record);

    List<RgsServicePriceStock> selectByExample(RgsServicePriceStockExample example);

    RgsServicePriceStock selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsServicePriceStock record, @Param("example") RgsServicePriceStockExample example);

    int updateByExample(@Param("record") RgsServicePriceStock record, @Param("example") RgsServicePriceStockExample example);

    int updateByPrimaryKeySelective(RgsServicePriceStock record);

    int updateByPrimaryKey(RgsServicePriceStock record);
}
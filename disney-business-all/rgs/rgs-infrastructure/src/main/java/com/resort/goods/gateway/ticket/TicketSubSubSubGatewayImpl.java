package com.resort.goods.gateway.ticket;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.convert.TicketConvert;
import com.resort.goods.domain.gateway.ticket.TicketSubSubSubGateway;
import com.resort.goods.domain.model.ticket.TicketSubSubSub;
import com.resort.goods.mapper.RgsGoodsSubSubSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsSubSubSubConfigExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class TicketSubSubSubGatewayImpl implements TicketSubSubSubGateway {

    @Resource
    private RgsGoodsSubSubSubConfigMapper rgsGoodsSubSubSubConfigMapper;
    @Resource
    private TicketConvert ticketConvert;

    @Override
    public List<TicketSubSubSub> getTicketSubSubSubs() {
        return null;
    }

    @Override
    public List<TicketSubSubSub> getTicketsBySubSubId() {
        return null;
    }

    @Override
    public List<TicketSubSubSub> getTicketsBySubSubId(Long id) {
        RgsGoodsSubSubSubConfigExample example = new RgsGoodsSubSubSubConfigExample();
        example.createCriteria().andGoodsSubSubIdEqualTo(id);
        return ticketConvert.fromRgsGoodsSubSubSubs(rgsGoodsSubSubSubConfigMapper.selectByExample(example));
    }
}

package com.resort.goods.convert;

import com.disney.domain.sdk.anno.DomainConvert;
import com.disney.domain.sdk.common.Lazy;

import com.resort.goods.api.mall.home.response.GoodsHomeRenderDTO;
import com.resort.goods.api.mall.item.response.ItemDetailRenderDTO;
import com.resort.goods.domain.gateway.service.ServiceSubSubGateway;
import com.resort.goods.domain.gateway.service.ServiceSubSubSubGateway;
import com.resort.goods.domain.model.service.Service;

import com.resort.goods.domain.model.service.ServiceSubSub;
import com.resort.goods.domain.model.ticket.Ticket;
import com.resort.goods.pojo.RgsGoodsSubConfig;
import com.resort.goods.pojo.RgsGoodsSubSubConfig;
import com.resort.goods.pojo.RgsGoodsSubSubSubConfig;
import com.resort.goods.domain.model.service.ServiceSubSubSub;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author issavior
 */
@DomainConvert
public class ServiceConvert {

    @Resource
    private ServiceSubSubGateway serviceSubSubGateway;
    @Resource
    private ServiceSubSubSubGateway serviceSubSubSubGateway;


    public Service fromRgsGoodsSub(RgsGoodsSubConfig ticketSub) {

        return Service.builder()
                .serviceSubSubs(Lazy.of(() -> serviceSubSubGateway.getServicesBySubId(ticketSub.getId())))
                .build();
    }

    public List<Service> fromRgsGoodsSubs(List<RgsGoodsSubConfig> ticketSubs) {


        return ticketSubs.stream()
                .map(ticketSub -> Service.builder()
                        .serviceSubSubs(Lazy.of(() -> serviceSubSubGateway.getServicesBySubId(ticketSub.getId())))
                        .build())
                .collect(Collectors.toList());
    }

    public List<ServiceSubSubSub> fromRgsGoodsSubSubSubs(List<RgsGoodsSubSubSubConfig> ticketSubSubSubs) {
        return ticketSubSubSubs.stream()
                .map(ticketSubSubSub -> ServiceSubSubSub.builder()
                        .build())
                .collect(Collectors.toList());
    }

    public List<ServiceSubSub> fromRgsGoodsSubSubs(List<RgsGoodsSubSubConfig> ticketSubSubs) {
        return ticketSubSubs.stream()
                .map(ticketSubSub -> ServiceSubSub.builder()
                        .serviceSubSubSubs(Lazy.of(() -> serviceSubSubSubGateway.getServicesBySubSubId(ticketSubSub.getId())))
                        .build())
                .collect(Collectors.toList());
    }
}

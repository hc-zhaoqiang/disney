package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsAgreePriceStock;
import com.resort.goods.pojo.RgsAgreePriceStockExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsAgreePriceStockMapper {
    long countByExample(RgsAgreePriceStockExample example);

    int deleteByExample(RgsAgreePriceStockExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsAgreePriceStock record);

    int insertSelective(RgsAgreePriceStock record);

    List<RgsAgreePriceStock> selectByExample(RgsAgreePriceStockExample example);

    RgsAgreePriceStock selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsAgreePriceStock record, @Param("example") RgsAgreePriceStockExample example);

    int updateByExample(@Param("record") RgsAgreePriceStock record, @Param("example") RgsAgreePriceStockExample example);

    int updateByPrimaryKeySelective(RgsAgreePriceStock record);

    int updateByPrimaryKey(RgsAgreePriceStock record);
}
package com.resort.goods.gateway.ticket;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.convert.TicketConvert;
import com.resort.goods.domain.gateway.ticket.TicketSubSubGateway;
import com.resort.goods.domain.model.ticket.TicketSubSub;
import com.resort.goods.mapper.RgsGoodsSubSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsSubSubConfigExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class TicketSubSubGatewayImpl implements TicketSubSubGateway {

    @Resource
    private RgsGoodsSubSubConfigMapper rgsGoodsSubSubConfigMapper;
    @Resource
    private TicketConvert ticketConvert;


    @Override
    public List<TicketSubSub> getTicketsBySubId(Long id) {
        RgsGoodsSubSubConfigExample example = new RgsGoodsSubSubConfigExample();
        example.createCriteria().andGoodsSubIdEqualTo(id);
        return ticketConvert.fromRgsGoodsSubSubs(rgsGoodsSubSubConfigMapper.selectByExample(example));
    }

    @Override
    public List<TicketSubSub> getTickets() {
        return null;
    }
}

package com.resort.goods.customer;

import org.apache.ibatis.annotations.Mapper;

/**
 * @author issavior
 */
public interface CustomerMapper{

  CustomerDO getById(String customerId);
}

package com.resort.goods.gateway.service;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.convert.ServiceConvert;
import com.resort.goods.domain.gateway.service.ServiceSubSubGateway;
import com.resort.goods.domain.model.service.ServiceSubSub;
import com.resort.goods.mapper.RgsGoodsSubSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsSubSubConfigExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class ServiceSubSubGatewayImpl implements ServiceSubSubGateway {

    @Resource
    private RgsGoodsSubSubConfigMapper rgsGoodsSubSubConfigMapper;
    @Resource
    private ServiceConvert serviceConvert;


    @Override
    public List<ServiceSubSub> getServicesBySubId(Long id) {
        RgsGoodsSubSubConfigExample example = new RgsGoodsSubSubConfigExample();
        example.createCriteria().andGoodsSubIdEqualTo(id);
        return serviceConvert.fromRgsGoodsSubSubs(rgsGoodsSubSubConfigMapper.selectByExample(example));
    }

    @Override
    public List<ServiceSubSub> getServices() {
        return null;
    }
}

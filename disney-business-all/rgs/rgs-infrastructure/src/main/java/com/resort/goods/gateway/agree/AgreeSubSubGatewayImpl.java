package com.resort.goods.gateway.agree;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.convert.AgreeConvert;
import com.resort.goods.domain.gateway.agree.AgreeSubSubGateway;
import com.resort.goods.domain.model.agree.AgreeSubSub;
import com.resort.goods.mapper.RgsGoodsSubSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsSubSubConfigExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class AgreeSubSubGatewayImpl implements AgreeSubSubGateway {

    @Resource
    private RgsGoodsSubSubConfigMapper rgsGoodsSubSubConfigMapper;
    @Resource
    private AgreeConvert agreeConvert;
    

    @Override
    public List<AgreeSubSub> getAgreesBySubId(Long id) {
        RgsGoodsSubSubConfigExample example = new RgsGoodsSubSubConfigExample();
        example.createCriteria().andGoodsSubIdEqualTo(id);
        return agreeConvert.fromRgsGoodsSubSubs(rgsGoodsSubSubConfigMapper.selectByExample(example));
    }

    @Override
    public List<AgreeSubSub> getAgrees() {
        return null;
    }
}

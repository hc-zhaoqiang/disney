package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsTicketPriceStock;
import com.resort.goods.pojo.RgsTicketPriceStockExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsTicketPriceStockMapper {
    long countByExample(RgsTicketPriceStockExample example);

    int deleteByExample(RgsTicketPriceStockExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsTicketPriceStock record);

    int insertSelective(RgsTicketPriceStock record);

    List<RgsTicketPriceStock> selectByExample(RgsTicketPriceStockExample example);

    RgsTicketPriceStock selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsTicketPriceStock record, @Param("example") RgsTicketPriceStockExample example);

    int updateByExample(@Param("record") RgsTicketPriceStock record, @Param("example") RgsTicketPriceStockExample example);

    int updateByPrimaryKeySelective(RgsTicketPriceStock record);

    int updateByPrimaryKey(RgsTicketPriceStock record);
}
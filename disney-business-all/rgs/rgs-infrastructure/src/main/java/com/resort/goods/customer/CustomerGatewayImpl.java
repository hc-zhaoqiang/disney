package com.resort.goods.customer;

import com.resort.goods.domain.customer.Customer;
import com.resort.goods.domain.customer.gateway.CustomerGateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author issavior
 */
@Component
public class CustomerGatewayImpl implements CustomerGateway {
//    @Autowired
    private CustomerMapper customerMapper;

    @Override
    public Customer getByById(String customerId){
      CustomerDO customerDO = customerMapper.getById(customerId);
      //Convert to Customer
      return null;
    }
}

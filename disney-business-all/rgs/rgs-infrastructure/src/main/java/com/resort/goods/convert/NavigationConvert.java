package com.resort.goods.convert;

import com.disney.domain.sdk.anno.DomainConvert;
import com.disney.domain.sdk.common.Lazy;
import com.resort.goods.domain.gateway.navigation.NavigationSubSubGateway;
import com.resort.goods.domain.gateway.navigation.NavigationSubSubSubGateway;
import com.resort.goods.domain.model.navigation.Navigation;
import com.resort.goods.domain.model.navigation.NavigationSubSub;
import com.resort.goods.pojo.RgsGoodsSubConfig;
import com.resort.goods.pojo.RgsGoodsSubSubConfig;
import com.resort.goods.pojo.RgsGoodsSubSubSubConfig;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;
import com.resort.goods.domain.model.navigation.NavigationSubSubSub;

/**
 * @author issavior
 */
@DomainConvert
public class NavigationConvert {

    @Resource
    private NavigationSubSubGateway navigationSubSubGateway;
    @Resource
    private NavigationSubSubSubGateway navigationSubSubSubGateway;

    public Navigation fromRgsGoodsSub(RgsGoodsSubConfig ticketSub) {

        return Navigation.builder()
                .navigationSubSubs(Lazy.of(() -> navigationSubSubGateway.getNavigationsBySubId(ticketSub.getId())))
                .build();
    }

    public List<Navigation> fromRgsGoodsSubs(List<RgsGoodsSubConfig> ticketSubs) {


        return ticketSubs.stream()
                .map(ticketSub -> Navigation.builder()
                        .navigationSubSubs(Lazy.of(() -> navigationSubSubGateway.getNavigationsBySubId(ticketSub.getId())))
                        .build())
                .collect(Collectors.toList());
    }

    public List<NavigationSubSubSub> fromRgsGoodsSubSubSubs(List<RgsGoodsSubSubSubConfig> ticketSubSubSubs) {
        return ticketSubSubSubs.stream()
                .map(ticketSubSubSub -> NavigationSubSubSub.builder()
                        .build())
                .collect(Collectors.toList());
    }

    public List<NavigationSubSub> fromRgsGoodsSubSubs(List<RgsGoodsSubSubConfig> ticketSubSubs) {
        return ticketSubSubs.stream()
                .map(ticketSubSub -> NavigationSubSub.builder()
                        .navigationSubSubSubs(Lazy.of(() -> navigationSubSubSubGateway.getNavigationsBySubSubId(ticketSubSub.getId())))
                        .build())
                .collect(Collectors.toList());
    }
}

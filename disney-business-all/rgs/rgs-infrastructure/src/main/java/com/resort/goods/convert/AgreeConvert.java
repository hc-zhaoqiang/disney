package com.resort.goods.convert;

import com.disney.domain.sdk.anno.DomainConvert;
import com.disney.domain.sdk.common.Lazy;

import com.resort.goods.domain.gateway.agree.AgreeSubSubGateway;
import com.resort.goods.domain.gateway.agree.AgreeSubSubSubGateway;
import com.resort.goods.domain.model.agree.AgreeSubSub;
import com.resort.goods.domain.model.agree.AgreeSubSubSub;
import com.resort.goods.pojo.RgsGoodsSubConfig;
import com.resort.goods.pojo.RgsGoodsSubSubConfig;
import com.resort.goods.pojo.RgsGoodsSubSubSubConfig;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;
import com.resort.goods.domain.model.agree.Agree;

/**
 * @author issavior
 */
@DomainConvert
public class AgreeConvert {

    @Resource
    private AgreeSubSubGateway agreeSubSubGateway;

    @Resource
    private AgreeSubSubSubGateway agreeSubSubSubGateway;
    

    public Agree fromRgsGoodsSub(RgsGoodsSubConfig ticketSub) {

        return Agree.builder()
                .agreeSubSubs(Lazy.of(() -> agreeSubSubGateway.getAgreesBySubId(ticketSub.getId())))
                .build();
    }

    public List<Agree> fromRgsGoodsSubs(List<RgsGoodsSubConfig> ticketSubs) {


        return ticketSubs.stream()
                .map(ticketSub -> Agree.builder()
                        .agreeSubSubs(Lazy.of(() -> agreeSubSubGateway.getAgreesBySubId(ticketSub.getId())))
                        .build())
                .collect(Collectors.toList());
    }

    public List<AgreeSubSubSub> fromRgsGoodsSubSubSubs(List<RgsGoodsSubSubSubConfig> ticketSubSubSubs) {
        return ticketSubSubSubs.stream()
                .map(ticketSubSubSub -> AgreeSubSubSub.builder()
                        .build())
                .collect(Collectors.toList());
    }

    public List<AgreeSubSub> fromRgsGoodsSubSubs(List<RgsGoodsSubSubConfig> ticketSubSubs) {
        return ticketSubSubs.stream()
                .map(ticketSubSub -> AgreeSubSub.builder()
                        .agreeSubSubSubs(Lazy.of(() -> agreeSubSubSubGateway.getAgreesBySubSubId(ticketSubSub.getId())))
                        .build())
                .collect(Collectors.toList());
    }
}

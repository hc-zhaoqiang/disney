package com.resort.goods.gateway.navigation;

import com.disney.domain.sdk.anno.DomainGateway;
import com.resort.goods.convert.NavigationConvert;
import com.resort.goods.domain.gateway.navigation.NavigationSubSubSubGateway;
import com.resort.goods.domain.model.navigation.NavigationSubSubSub;
import com.resort.goods.mapper.RgsGoodsSubSubSubConfigMapper;
import com.resort.goods.pojo.RgsGoodsSubSubSubConfigExample;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author issavior
 */
@DomainGateway
public class NavigationSubSubSubGatewayImpl implements NavigationSubSubSubGateway {

    @Resource
    private RgsGoodsSubSubSubConfigMapper rgsGoodsSubSubSubConfigMapper;
    @Resource
    private NavigationConvert navigationConvert;

    @Override
    public List<NavigationSubSubSub> getNavigationsBySubSubId() {
        return null;
    }

    @Override
    public List<NavigationSubSubSub> getNavigationsBySubSubId(Long id) {
        RgsGoodsSubSubSubConfigExample example = new RgsGoodsSubSubSubConfigExample();
        example.createCriteria().andGoodsSubSubIdEqualTo(id);
        return navigationConvert.fromRgsGoodsSubSubSubs(rgsGoodsSubSubSubConfigMapper.selectByExample(example));
    }
}

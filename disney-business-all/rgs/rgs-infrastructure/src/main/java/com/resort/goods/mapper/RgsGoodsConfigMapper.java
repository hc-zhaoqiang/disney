package com.resort.goods.mapper;

import com.resort.goods.pojo.RgsGoodsConfig;
import com.resort.goods.pojo.RgsGoodsConfigExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RgsGoodsConfigMapper {
    long countByExample(RgsGoodsConfigExample example);

    int deleteByExample(RgsGoodsConfigExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RgsGoodsConfig record);

    int insertSelective(RgsGoodsConfig record);

    List<RgsGoodsConfig> selectByExample(RgsGoodsConfigExample example);

    RgsGoodsConfig selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RgsGoodsConfig record, @Param("example") RgsGoodsConfigExample example);

    int updateByExample(@Param("record") RgsGoodsConfig record, @Param("example") RgsGoodsConfigExample example);

    int updateByPrimaryKeySelective(RgsGoodsConfig record);

    int updateByPrimaryKey(RgsGoodsConfig record);
}
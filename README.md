![输入图片说明](file/images/%E4%B8%8A%E6%B5%B7%E8%BF%AA%E5%A3%AB%E5%B0%BC.jpeg)


## 1. 背景
上海迪士尼度假区已运营近10年，度假区交易体系依赖于各家平台（携程、去哪儿、同程、途牛、芒果网、悠哉网等），随着互联网的高速发展以及度假区业务的不断迭代更新，目前的技术架构已经很难支持度假区的需求和业务。



故度假区经过慎重的考虑，确定上海迪士尼度假区的交易系统从0构建，打造度假区自己的交易自闭环，更好的为用户服务。



## 2. 技术架构
![输入图片说明](file/images/Disney%E6%8A%80%E6%9C%AF%E6%9E%B6%E6%9E%84.png)

## 3. 业务架构

### 3.1 架构图
![输入图片说明](file/images/Disney%E4%B8%9A%E5%8A%A1%E6%9E%B6%E6%9E%84.png)

### 3.2 说明
- disney-auth-all	-----	认证授权中心
- disney-business-all	-----	业务中心
	- rcs-parent	-----	购物车服务
	- rds-parent	-----	数据分析服务
	- rgs-parent	-----	商品服务
	- ros-parent	-----	订单服务
	- rps-parent	-----	支付服务
	- rts-parent	-----	交易服务
	- rus-parent	-----	用户服务
- disney-config-all	-----	配置中心
- disney-fusing-all	-----	熔断降级中心
- disney-gateway-all	-----	服务网关中心
- disney-job-all	----- 分布式调度中心
- disney-mq-all ---- 消息队列中心
- disney-register-all	-----	注册中心
- disney-rpc-all	-----	远程服务调用中心
- disney-sdk-all	-----	开发工具中心
- disney-trace-all	-----	分布式链路追踪中心
- disney-transition-all	-	分布式事务中心

## 4. 技术能力
### 4.1 自研中间件
1. [注册中心：Disney-Register的设计与实现](https://www.baidu.com)
2. [配置中心：Disney-Config的设计与实现](https://www.baidu.com)
3. [RPC：Disney-Rpc的设计与实现](https://www.baidu.com)
4. [熔断降级：Disney-fusing的设计与实现](https://www.baidu.com)
5. [分布式事务：Disney-Transaction的设计与实现](https://www.baidu.com)
6. [分布式调度：Disney-Job的设计与实现](https://www.baidu.com)
7. [分布式链路追踪：Disney-Trace的设计与实现](https://www.baidu.com)
8. [服务授权中心：Disney-Auth的设计与实现](https://www.baidu.com)
9. [服务网关：Disney-Gateway的设计与实现](https://www.baidu.com)

### 4.2 定制化中间件
1. [改造mybatis逆向工程](https://www.baidu.com)
2. [改造Redis多级缓存](https://www.baidu.com)
3. [改造RocketMQ消息队列](https://www.baidu.com)
4. [整合Nacos、Sentinel](https://www.baidu.com)

## 5. 领域模型
[【上海迪士尼度假区】技术解决方案 - 领域模型](https://www.baidu.com)
## 6. 数据模型
[【上海迪士尼度假区】技术解决方案 - 数据模型](https://www.baidu.com)

## 7. 交易链路
![输入图片说明](file/images/Disney%E6%8A%80%E6%9C%AF%E6%96%B9%E6%A1%88.png)


## 8. 状态机

[【上海迪士尼度假区】技术解决方案 - 状态机](https://www.baidu.com)

## 8. 接口文档
[【上海迪士尼度假区】技术解决方案 - 接口文档](https://www.baidu.com)





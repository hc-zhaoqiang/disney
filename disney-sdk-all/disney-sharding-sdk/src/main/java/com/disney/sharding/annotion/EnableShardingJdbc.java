package com.disney.sharding.annotion;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 引入sharding-jdbc功能注解
 *
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(ShardingAutoConfig.class)
public @interface EnableShardingJdbc {
}
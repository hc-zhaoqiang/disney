package com.disney.datasource.start.annotion;

import com.disney.datasource.start.DynamicDataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 动态数据源的引入类
 *
 * @author issavior
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(DynamicDataSourceAutoConfiguration.class)
public @interface EnableDynamicDataSource {
}
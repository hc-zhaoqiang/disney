package com.disney.redis.start.lock;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.api.RedissonReactiveClient;

/**
 * 分布式锁的实现类
 *
 * @author issavior
 */
@Slf4j
public class DistributedLockService {

    private static RedissonClient staticRedissonClient;
    private static RedissonReactiveClient staticRedissonReactiveClient;

    public DistributedLockService(RedissonClient redissonClient, RedissonReactiveClient redissonReactiveClient) {
        DistributedLockService.staticRedissonClient = redissonClient;
        DistributedLockService.staticRedissonReactiveClient = redissonReactiveClient;
    }

    public static RedissonClient getRedissonCli() {
        return staticRedissonClient;
    }

    public static RedissonReactiveClient getRedisReactCli() {
        return staticRedissonReactiveClient;
    }

    /**
     * 加锁
     *
     * @param lockKey 锁的key
     */
    public void lock(String lockKey) {
        RLock lock = getRedissonCli().getLock(lockKey);
        lock.lock();
    }

    /**
     * 解锁
     *
     * @param lockKey 锁的key
     */
    public void unlock(String lockKey) {
        RLock lock = getRedissonCli().getLock(lockKey);
        lock.unlock();
    }

}

package com.disney.domain.sdk.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * 领域工厂 - 领域划分
 * @author issavior
 */
@AllArgsConstructor
@Getter
public enum DomainAbsBizType {

    GOODS("disney.biz.goods", "商品域"),
    CMS("disney.biz.cms", "商品配置域"),
    TRADE("disney.biz.goods", "交易域"),
    PAY("disney.biz.payment", "支付域"),

    UNKNOWN("disney.biz.unknown", "未知");

    private final String code;
    private final String desc;

    public static DomainAbsBizType of(String code) {
        return Arrays.stream(DomainAbsBizType.values())
                .filter(e -> Objects.equals(e.getCode(), code))
                .findFirst()
                .orElse(UNKNOWN);
    }
}

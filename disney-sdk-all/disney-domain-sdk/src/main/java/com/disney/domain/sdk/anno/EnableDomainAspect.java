package com.disney.domain.sdk.anno;

import com.disney.domain.sdk.factory.FactoryImport;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标识切面处理所有的异常类路径
 *
 * @author issavior
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({FactoryImport.class})
public @interface EnableDomainAspect {

    @AliasFor("basePackages")
    String[] value() default {};

    @AliasFor("value")
    String[] basePackages() default {};

    boolean enable() default true;
}

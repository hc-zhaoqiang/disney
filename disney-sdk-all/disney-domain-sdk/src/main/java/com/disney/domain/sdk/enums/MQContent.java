package com.disney.domain.sdk.enums;

import lombok.Data;
import lombok.Getter;

/**
 * @author issavior
 */
@Data
public class MQContent {


    @Getter
    public enum Group {

        UNKNOWN("UNKNOWN", "未知");

        private final String value;
        private final String desc;


        Group(String value, String desc) {
            this.value = value;
            this.desc = desc;
        }
    }

    @Getter
    public enum Topic {

        UNKNOWN("UNKNOWN:", "未知");

        private final String value;
        private final String desc;


        Topic(String value, String desc) {
            this.value = value;
            this.desc = desc;
        }
    }

    @Getter
    public enum Tag {

        UNKNOWN("UNKNOWN", "未知");

        private final String value;
        private final String desc;


        Tag(String value, String desc) {
            this.value = value;
            this.desc = desc;
        }
    }

}

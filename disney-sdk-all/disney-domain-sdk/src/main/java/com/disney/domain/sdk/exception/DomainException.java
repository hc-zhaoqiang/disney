package com.disney.domain.sdk.exception;

import lombok.Data;

/**
 * @author issavior
 */
@Data
public class DomainException extends RuntimeException{

    public DomainException(String message) {
        super(message);
    }
}

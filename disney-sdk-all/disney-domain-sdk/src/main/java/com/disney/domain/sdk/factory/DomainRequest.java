package com.disney.domain.sdk.factory;

import com.disney.domain.sdk.enums.DomainBizType;
import com.disney.domain.sdk.enums.DomainAbsBizType;
import lombok.Builder;
import lombok.Data;

/**
 * @author issavior
 */
@Data
@Builder(builderMethodName = "to",buildMethodName = "end")
public class DomainRequest {

    private DomainBizType code;

    private DomainAbsBizType scenario;
}

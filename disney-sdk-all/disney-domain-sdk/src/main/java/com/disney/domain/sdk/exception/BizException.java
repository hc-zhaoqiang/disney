package com.disney.domain.sdk.exception;

/**
 * @author issavior
 */
public class BizException extends DomainException {
    public BizException(String message) {
        super(message);
    }

    public BizException() {
        super("业务异常");
    }

}

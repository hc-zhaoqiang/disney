package com.disney.domain.sdk.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author issavior
 */

@Getter
public enum DomainBizType {

    TICKET("disney.biz.sub.ticket", "门票"),
    ANNUAL_PASS("disney.biz.sub.annual.pass", "年卡"),
    SERVICE("disney.biz.sub.service", "服务"),
    HOTEL("disney.biz.sub.hotel", "酒店"),
    // 一键小写变大写 [CMD + SHIFT + U]
    NAVIGATION("disney.biz.sub.navigation", "活动"),
    AGREE("disney.biz.sub.agree", "餐饮"),


    WECHAT_PAY("disney.pay.wechat", "微信支付"),
    ALIPAY_PAY("disney.pay.alipay", "支付宝支付"),

    UNKNOWN("disney.biz.sub.unknown", "其他");

    private final String code;
    private final String msg;

    DomainBizType(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static DomainBizType of(String code) {
        return Arrays.stream(DomainBizType.values())
                .filter(e -> Objects.equals(e.getCode(), code))
                .findFirst()
                .orElse(null);
    }

}

package com.disney.domain.sdk.anno;


import com.disney.domain.sdk.enums.DomainAbsBizType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author issavior
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AbsBiz {

    DomainAbsBizType value();

}

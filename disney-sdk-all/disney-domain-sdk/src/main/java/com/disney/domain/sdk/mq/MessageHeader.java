package com.disney.domain.sdk.mq;

import java.util.UUID;

/**
 * @author issavior
 */
public class MessageHeader {

    public static final UUID ID_VALUE_NONE = new UUID(0L, 0L);
    public static final String ID = "id";
    public static final String KEY = "key";
    public static final String TIMESTAMP = "timestamp";
    public static final String CONTENT_TYPE = "contentType";
    public static final String REPLY_CHANNEL = "replyChannel";
    public static final String ERROR_CHANNEL = "errorChannel";

}

package com.disney.domain.sdk.dto;

/**
 * Query request from Client.
 *
 *
 */
public abstract class Query extends Command {

    private static final long serialVersionUID = 1L;

}

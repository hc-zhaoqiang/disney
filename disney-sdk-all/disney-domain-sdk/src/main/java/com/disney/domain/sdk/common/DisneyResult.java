package com.disney.domain.sdk.common;

import lombok.*;

import java.io.Serializable;
import java.util.Optional;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DisneyResult<T> implements Serializable {

    @Getter
    @Setter
    private boolean success;

    @Getter
    @Setter
    private boolean fail;

    @Getter
    @Setter
    private int code;

    @Getter
    @Setter
    private String msg;

    @Getter
    @Setter
    private T data;

    public static <T> DisneyResult<T> ok() {
        return restResult(null, CommonConstants.SUCCESS, null);
    }

    public static <T> DisneyResult<T> ok(T data) {
        return restResult(data, CommonConstants.SUCCESS, null);
    }

    public static <T> DisneyResult<T> ok(T data, String msg) {
        return restResult(data, CommonConstants.SUCCESS, msg);
    }

    public static <T> DisneyResult<T> failed() {
        return restResult(null, CommonConstants.FAIL, null);
    }

    public static <T> DisneyResult<T> failed(String msg) {
        return restResult(null, CommonConstants.FAIL, msg);
    }

    public static <T> DisneyResult<T> failed(T data) {
        return restResult(data, CommonConstants.FAIL, null);
    }

    public static <T> DisneyResult<T> failed(T data, String msg) {
        return restResult(data, CommonConstants.FAIL, msg);
    }


    private static <T> DisneyResult<T> restResult(T data, int code, String msg) {
        DisneyResult<T> apiResult = new DisneyResult<>();
        apiResult.setSuccess(Optional.of(code).filter(c -> c.equals(CommonConstants.SUCCESS)).isPresent());
        apiResult.setSuccess(Optional.of(code).filter(c -> c.equals(CommonConstants.FAIL)).isPresent());
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }

}

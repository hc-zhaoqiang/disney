package com.disney.domain.sdk.factory;

/**
 * @author issavior
 */
public interface IDomainBiz {

    /**
     * 是否开启
     *
     * @return
     */
    default boolean enable() {
        return true;
    }

    /**
     * 排序
     *
     * @return
     */
    default int byOrder() {
        return 0;
    }

    /**
     * 设置id，根据规则过滤
     *
     * @return
     */
    default String byId() {
        return null;
    }
}

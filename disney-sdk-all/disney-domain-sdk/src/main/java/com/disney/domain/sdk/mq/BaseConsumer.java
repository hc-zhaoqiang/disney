package com.disney.domain.sdk.mq;


import org.apache.rocketmq.spring.core.RocketMQListener;

/**
 * @author issavior
 */
public interface BaseConsumer<T> extends RocketMQListener<DomainMessage<T>> {



}

package com.disney.domain.sdk.dto;

/**
 * Command request from Client.
 *
 *
 */
public abstract class Command extends DTO {

    private static final long serialVersionUID = 1L;

}

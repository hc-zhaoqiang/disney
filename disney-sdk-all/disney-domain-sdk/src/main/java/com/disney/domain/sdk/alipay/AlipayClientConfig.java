package com.disney.domain.sdk.alipay;

import com.alipay.api.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;

/**
 * @author issavior
 */
@Configuration
@PropertySource("classpath:application.properties")
public class AlipayClientConfig {

    @Resource
    private Environment environment;

    @Bean
    public AlipayClient alipayClient() throws AlipayApiException {
        AlipayConfig alipayConfig = new AlipayConfig();
        alipayConfig.setServerUrl(environment.getProperty("alipay.gateway-url"));
        alipayConfig.setAppId(environment.getProperty("alipay.app.id"));
        alipayConfig.setPrivateKey(environment.getProperty("alipay.merchant-private-key"));
        alipayConfig.setFormat(AlipayConstants.FORMAT_JSON);
        alipayConfig.setCharset(AlipayConstants.CHARSET_UTF8);
        alipayConfig.setAlipayPublicKey(environment.getProperty("alipay.alipay-public-key"));
        alipayConfig.setSignType(AlipayConstants.SIGN_TYPE_RSA2);

        return new DefaultAlipayClient(alipayConfig);

    }
}

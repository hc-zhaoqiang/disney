package com.disney.domain.sdk.catchlog;

/**
 * @author issavior
 */
public interface ResponseHandlerI {
    public Object handle(Class returnType, String errCode, String errMsg);
}

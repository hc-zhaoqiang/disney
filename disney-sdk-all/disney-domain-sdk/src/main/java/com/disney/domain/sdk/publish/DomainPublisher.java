package com.disney.domain.sdk.publish;

import com.disney.domain.sdk.anno.DomainEvenPublish;
import com.disney.domain.sdk.even.DomainEven;
import com.disney.domain.sdk.even.MQEven;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

/**
 * @author issavior
 */
@DomainEvenPublish
public class DomainPublisher implements ApplicationEventPublisherAware {

    private static ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        DomainPublisher.applicationEventPublisher = applicationEventPublisher;
    }

    public static void publish(DomainEven domainEven) {
        applicationEventPublisher.publishEvent(domainEven);
    }

    public static void publish(MQEven mqEven) {
        applicationEventPublisher.publishEvent(mqEven);
    }



}

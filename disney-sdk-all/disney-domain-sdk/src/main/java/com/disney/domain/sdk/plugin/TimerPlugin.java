package com.disney.domain.sdk.plugin;

import lombok.extern.slf4j.Slf4j;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * @author issavior
 */
@Mojo(name = "TimerPlugin")
@Slf4j
public class TimerPlugin extends AbstractMojo {

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        log.info("===================== maven timer插件 生效");
    }
}
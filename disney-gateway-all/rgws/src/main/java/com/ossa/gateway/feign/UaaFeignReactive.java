package com.ossa.gateway.feign;

import com.ossa.common.api.constant.OssaServiceName;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import reactivefeign.spring.config.ReactiveFeignClient;
import reactor.core.publisher.Mono;

import java.security.Principal;
import java.util.Map;

/**
 * @author issavior
 */
@FeignClient(value = OssaServiceName.UAA_SERVER)
public interface UaaFeignReactive {

    @RequestMapping(value = "/uaa/oauth/check_token")
    @ResponseBody
    public Map<String, ?> checkToken(@RequestParam("token") String value);

    @RequestMapping(value = "/uaa/oauth/token", method = RequestMethod.POST)
    public ResponseEntity<OAuth2AccessToken> postAccessToken(Principal principal, @RequestParam
    Map<String, String> parameters);

}


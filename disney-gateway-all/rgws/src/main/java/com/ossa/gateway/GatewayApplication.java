package com.ossa.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import reactivefeign.spring.config.EnableReactiveFeignClients;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 网关API
 * @author issavior
 */
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = "com.ossa")
@EnableReactiveFeignClients(basePackages = "com.ossa.gateway.feign")
@EnableFeignClients(basePackages = "com.ossa.gateway.feign")
public class GatewayApplication {

    public static void main(String[] args) {
        System.setProperty("csp.sentinel.app.type", "1");
        SpringApplication.run(GatewayApplication.class, args);
    }
}
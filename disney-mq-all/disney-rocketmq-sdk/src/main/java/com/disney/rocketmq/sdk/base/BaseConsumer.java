package com.disney.rocketmq.sdk.base;


import org.apache.rocketmq.spring.core.RocketMQListener;

/**
 * @author issavior
 */
public interface BaseConsumer<T> extends RocketMQListener<DomainMessage<T>> {



}

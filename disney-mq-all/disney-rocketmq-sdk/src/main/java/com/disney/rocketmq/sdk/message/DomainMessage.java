package com.disney.rocketmq.sdk.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author issavior
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DomainMessage<T> {

    private String key;

    private T message;

    private Long timeout = 3000L;

    private Integer delayLevel;





}

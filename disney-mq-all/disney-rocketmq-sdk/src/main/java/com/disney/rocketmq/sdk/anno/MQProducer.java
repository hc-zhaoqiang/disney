package com.disney.rocketmq.sdk.anno;


import com.disney.domain.sdk.enums.MQContent;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @author issavior
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface MQProducer {

    MQContent.Topic topic();
    MQContent.Tag tag();
}

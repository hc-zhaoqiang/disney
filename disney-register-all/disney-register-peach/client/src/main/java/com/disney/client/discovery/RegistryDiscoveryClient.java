package com.disney.client.discovery;

import cn.lzj.nacos.api.pojo.Instance;
import com.disney.client.api.DiscoveryClient;
import com.disney.client.api.NamingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RegistryDiscoveryClient implements DiscoveryClient {

    @Autowired
    NamingService namingService;

    @Override
    public List<Instance> getInstances(String serviceName) {
        return namingService.selectInstances(serviceName);
    }
}

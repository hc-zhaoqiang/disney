package com.disney.client.api;

import cn.lzj.nacos.api.pojo.Instance;

import java.util.List;

public interface DiscoveryClient {

    List<Instance> getInstances(String serviceId);
}

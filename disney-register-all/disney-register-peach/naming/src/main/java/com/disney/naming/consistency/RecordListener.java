package com.disney.naming.consistency;

import com.disney.naming.core.Instances;

public interface RecordListener {

    void onChange(String key, Instances value);

    void onDelete(String key);
}

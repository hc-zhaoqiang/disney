package com.disney.naming.consistency;

import com.disney.naming.core.Instances;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class SyncTask {

    private Map<String, Instances> dataMap=new HashMap<>();

    private int retryCount;

    private long lastExecuteTime;

    private String targetServer;



}

package com.disney.naming.misc;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

@Data
public class Message {

    private String data;
}
